import { PulsarTenantInK8s, PulsarTenantInPulsar } from '@/classes/PulsarTenant';
import logger from '@/log/logger.default';
import { ResourceMeta } from '@dot-i/k8s-operator';
import getFullName from '../shared/misc/service.shared.get_full_name';
import pulsarRestClient from '../shared/service.shared.get_pulsar_rest_client';
import waitForTenantReady from './conditions/service.tenant.await_ready';
import updateIsNecessary from './conditions/service.tenant.update_is_necessary';
import { getPatchStatus, StatusPatcher } from '../shared/k8s/service.shared.k8s.get_status_patcher';
import tenantPresentInDifferentNamespace from './validations/service.tenant.k8s_tenant_present_in_different_namespace';
import getErrorPatch, { ErrorTypeTenant, isErrorPatch } from '../errors/service.errors.tenant.get_error_patch';
import logError from '../errors/service.errors.try_to_log_error_message';
import errorIsClustersDoNotExist from '../errors/service.errors.tenant.is_cluster_does_not_exist';

export const getService = (client= pulsarRestClient) =>
  async (
    meta: ResourceMeta, 
    tenantK8s: PulsarTenantInK8s,
    tenantPulsar: PulsarTenantInPulsar,
    patchStatus: StatusPatcher<PulsarTenantInK8s>
  ) => {
    try {

      if (tenantK8s.status.state === 'creating' || tenantK8s.status.state === 'error'){
        logger.info('tenant is in creating state in k8s. Checking if in use in other namespaces');
        if (await tenantPresentInDifferentNamespace(tenantK8s)){
          const errPatch = getErrorPatch(ErrorTypeTenant.TenantAlreadyExistsInK8s);
          logger.error(errPatch.statusMessage);

          if (isErrorPatch(ErrorTypeTenant.TenantAlreadyExistsInK8s, tenantK8s.status)){
            logger.debug('Already in error state. no need to patch');
          } else {
            logger.debug('patching error');
            await patchStatus(errPatch);
          }

          
          return;
        } else {
          logger.info('tenant not yet present in other namespace. creation is allowed...');
        }
      }
      
      await patchStatus({
        state: 'updating'
      });
      
      const { adminRoles, allowedClusters } = tenantK8s.spec.config;
      
      
      
      await client.post(
        `/tenants/${meta.name}`,
        {
          adminRoles,
          allowedClusters
        }
      );
      await waitForTenantReady(meta, tenantK8s);
      await patchStatus({
        state: 'active'
      });
      logger.info(`${getFullName(meta)}: Successfuly updated tenant`);
    } catch (err) {
      if (errorIsClustersDoNotExist(err)){
        await patchStatus(getErrorPatch(ErrorTypeTenant.ClustersDoNotExist));
        return;
      }

      logError(err, `${getFullName(meta)}: Could not update tenant`);
      await patchStatus(getErrorPatch(ErrorTypeTenant.InternalError));
    }
  };

export default getService();