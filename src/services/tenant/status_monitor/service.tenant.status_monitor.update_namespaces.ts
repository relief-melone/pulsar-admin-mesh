import { PulsarTenantInK8s, PulsarTenantInPulsar } from '@/classes/PulsarTenant';
import { StatusPatchGetter } from '@/classes/StatusMonitorNew';
import logger from '@/log/logger.default';
import getNamespacesForTenant from '@/services/namespaces/pulsar/service.namespaces.get_for_tenant';
import getFullName from '@/services/shared/misc/service.shared.get_full_name';
import getMeta from '@/services/shared/service.shared.get_resource_meta_from_kube_object_meta';

export const getService = ():StatusPatchGetter<PulsarTenantInPulsar, PulsarTenantInK8s> => 
  async (_pulsarState, itemK8s) => {
    const status = itemK8s.status;
    const meta = getMeta(itemK8s);
    const patch:Partial<PulsarTenantInK8s['status']> = {};
    if ( status.state === 'error')
      return {};
    
    try {
      const namespaces = await getNamespacesForTenant(meta.name);

      if (namespaces.length !== status.namespaces.count){
        logger.debug(`${getFullName(meta)}: Updating status.namespaces`);
        patch.namespaces = { count: namespaces.length };
      } else {
        logger.debug(`${getFullName(meta)}: No update for status.namespaces necessary`);
      }
      
    } catch (err){
      logger.error(`${getFullName(meta)}: error updating status for namespaces`);
    } finally {
      return patch;
    }
  };


export default getService();