import { PulsarTenantInK8s, PulsarTenantInPulsar } from '@/classes/PulsarTenant';
import { StatusPatchGetter } from '@/classes/StatusMonitorNew';
import { ErrorTypeTenant } from '@/services/errors/service.errors.tenant.get_error_patch';
import logError from '@/services/errors/service.errors.try_to_log_error_message';
import getMeta from '@/services/shared/service.shared.get_resource_meta_from_kube_object_meta';
import getFullName from '../../shared/misc/service.shared.get_full_name';
import GetTenant from '../misc/service.tenant.rest.get_one';
import TenantPresentInAnotherNamespace from '../validations/service.tenant.k8s_tenant_present_in_different_namespace';

export const getService = (
  tenantPresentInAnotherNamespace = TenantPresentInAnotherNamespace,
  getTenant = GetTenant
):StatusPatchGetter<PulsarTenantInPulsar, PulsarTenantInK8s> => 
  async (_pulsarState, itemInK8s) => {
    const patch:Partial<PulsarTenantInK8s['status']> = {};
    const status = itemInK8s.status;
    const meta = getMeta(itemInK8s);
    try {
      const tenant = await getTenant(meta);
      if (!!tenant)
        switch (status.state){
          case 'active':
            break;
          case 'creating':
          case 'updating':
          case 'error':
            if (
              !(await tenantPresentInAnotherNamespace({
                metadata: meta,
                status: status
              })) 
              && status.statusMessage !== ErrorTypeTenant.ClustersDoNotExist
            ){
              patch.state = 'active';
            }
            break;
        }
    } catch (err:any){
      logError(err, `${getFullName(meta)}: error updating state`);
    } finally {
      return patch;
    }
  };


export default getService();