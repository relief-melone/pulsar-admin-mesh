import { PulsarTenantInK8s } from '@/classes/PulsarTenant';
import ConfigTenant, { ConfigPulsarTenant } from '@/configs/config.pulsar.tenant';
import logger from '@/log/logger.default';
import resourceNotIncludedInWhitelist from '@/services/shared/validations/service.shared.resource_not_included_in_whitelist';


export const getService = (config = ConfigTenant) =>
  (tenant?: string):boolean => {
    if (!tenant){
      logger.warn('tenant not handled as no name for it provided');
      return false;
    }

    if (resourceNotIncludedInWhitelist(tenant, config)){
      logger.debug(`tenant ${tenant} not handled as not whitelisted`);
      return false;
    }

    logger.debug(`handeling tenant ${tenant}`);
    return true;
  };

export default getService();