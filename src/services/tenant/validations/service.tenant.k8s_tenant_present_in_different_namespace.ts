import { PulsarTenantInK8s } from '@/classes/PulsarTenant';
import ConfigK8s from '@/configs/config.k8s';
import configPulsarTenant from '@/configs/config.pulsar.tenant';
import logger from '@/log/logger.default';
import getFullName from '@/services/shared/misc/service.shared.get_full_name';
import { getNamespace } from '@/services/shared/service.shared.get_namespace';
import getMeta from '@/services/shared/service.shared.get_resource_meta_from_kube_object_meta';
import getNamespaceNames from '@/services/shared/k8s/service.shared.k8s.get_names_of_namepsaces';
import { ResourceMeta } from '@dot-i/k8s-operator';
import isCustomResourceNotFound from '@/services/errors/service.errors.is_resource_not_found';
import logErr from '@/services/errors/service.errors.try_to_log_error_message';

export interface MinInputType {
  metadata?: ResourceMeta
  status: PulsarTenantInK8s['status']
}

export const getService = ( configK8s = ConfigK8s, configTenant = configPulsarTenant) => 
  async (tenant: PulsarTenantInK8s | MinInputType):Promise<boolean> => {
    const meta = getMeta(tenant);
    logger.debug(`${getFullName(meta)}: looking up if tenant already exists in different namespace...`);

    if (tenant.status.state === 'active'){
      logger.debug(`${getFullName(meta)}: tenant already active. no need for check...`);
      return false;
    }
    
    try {
      const { apiCustom, apiCore } = configK8s;
      const { group, plural, version } = configTenant.main;
      const namespaces = await getNamespaceNames();
      const name = tenant.metadata?.name;
      if (!name){
        const msg = 'could not get name from tenant';
        logger.error(msg); 
        throw new Error(msg);
      }

      const lookupOperations = namespaces.map(async ns => {
        if (ns === getNamespace(tenant.metadata))
          return;

        try {
          await apiCustom.getNamespacedCustomObject(
            group, version, ns, plural, name
          );
          return true;
        } catch (err) {
          if (isCustomResourceNotFound(err))
            return false;
          else {
            logErr(err, `unkown error looking up tenant ${name}`);
            throw err;
          }
        }
      });
      const fullfilled = await Promise.all(lookupOperations);
      return (await Promise.all(lookupOperations)).some(v => v);
      
      

      // const tenantList = await apiCustom.listNamespacedCustomObject(
      //   group, version, getNamespace(meta), plural
      // );
    } catch (err:any) {
      throw err;
    }
    
  };

export default getService();