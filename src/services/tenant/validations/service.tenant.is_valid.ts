import { PulsarTenantInK8s } from '@/classes/PulsarTenant';
import logger from '@/log/logger.default';
import { ResourceMeta } from '@dot-i/k8s-operator';
import getFullName from '../../shared/misc/service.shared.get_full_name';

export const tenantIsValid = (meta: ResourceMeta, tenant: Partial<PulsarTenantInK8s>):boolean => {
  const isValid = !!(tenant.spec?.config.allowedClusters?.length
    && tenant.spec.config.allowedClusters.length > 0
    && tenant.metadata?.name);

  if (!isValid)
    logger.error(`${getFullName(meta)}: Tenant is not valid`);

  return isValid;
};
  


