import { PulsarTenantInK8s, PulsarTenantInternal } from '@/classes/PulsarTenant';
import configPulsarTenant from '@/configs/config.pulsar.tenant';
import logger from '@/log/logger.default';
import { ResourceMeta } from '@dot-i/k8s-operator';
import wait from '@root/test/helpers/wait';
import { AxiosError } from 'axios';
import getFullName from '../../shared/misc/service.shared.get_full_name';
import restClientPulsar from '../../shared/service.shared.get_pulsar_rest_client';

export const getService = (client = restClientPulsar, config = configPulsarTenant) =>
  async (
    tenant: PulsarTenantInternal,
    attempt = 1
  ):Promise<void> => {
    try {
      const result = await client.get(
        `/tenants/${tenant.name}`
      );
      const readiness = config.probing.readiness;
      if (!!result && attempt < readiness.maxAttempts){
        logger.info(`${getFullName(tenant.k8s?.meta)}: Tenant still present. Attempt ${attempt}/${readiness.maxAttempts}. Retriying`);
        await wait((attempt+1)*readiness.intervalIncrementorMs);
        return await getService(client,config)(tenant, attempt+1);
      } else if (!!result && attempt >= readiness.maxAttempts){
        logger.error(`${getFullName(tenant.k8s?.meta)}: Tenant did not get deleted within specified attempt limit. Giving up`);
        return;
      } else {
        logger.error(`${getFullName(tenant.k8s?.meta)}: Tenant could not be deleted`);
      }
      
    } catch (err:any) {
      if (err instanceof AxiosError && err.response?.status === 404){
        logger.info(`${getFullName(tenant.k8s?.meta)}: Tenant successfully removed from pulsar cluster`);
        return;
      } else 
        logger.error(`${getFullName(tenant.k8s?.meta)}: Unkown error while trying to delete pulsar tenant`);
    }
  };

export default getService();