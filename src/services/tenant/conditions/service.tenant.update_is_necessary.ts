import { PulsarTenantInK8s } from '@/classes/PulsarTenant';
import logger from '@/log/logger.default';
import logError from '@/services/errors/service.errors.try_to_log_error_message';
import getPrevious from '@/services/shared/k8s/service.shared.k8s.get_previous_config';
import getFullName from '@/services/shared/misc/service.shared.get_full_name';
import { ResourceMeta } from '@dot-i/k8s-operator';
import clone from 'clone-deep';
import deepEqual from 'fast-deep-equal';

export const getService = () => 
  (metaTenant: ResourceMeta, tenant: PulsarTenantInK8s):boolean => {
    const beforeChange = getPrevious(tenant);
    try {

      switch (tenant.status.state){
        case 'creating':
          return true;
      }

      const config = clone(beforeChange?.spec.config);
      const configNew = clone(tenant.spec.config);

      config?.adminRoles.sort();
      config?.allowedClusters.sort();
      
      configNew.adminRoles.sort();
      configNew.allowedClusters.sort();
      
      return !deepEqual(config, configNew); 

    } catch (err:any) {
      const msg = `${getFullName(metaTenant)}: could not check if update is necessary`;
      logError(err, msg);
      throw new Error(msg);
    }
  };

export default getService();
