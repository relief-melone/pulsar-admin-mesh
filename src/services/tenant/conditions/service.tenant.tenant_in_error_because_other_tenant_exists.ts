import { ErrorTypeTenant } from '@/services/errors/service.errors.tenant.get_error_patch';
import { ResourceEvent } from '@dot-i/k8s-operator';
import getTenantFromResourceEvent from '../misc/service.tenant.get_from_resource_event';

export default (evt: ResourceEvent):boolean => {
  const tenant = getTenantFromResourceEvent(evt);
  return tenant.status.state === 'error'
  && tenant.status.statusMessage === ErrorTypeTenant.TenantAlreadyExistsInK8s;
};
  