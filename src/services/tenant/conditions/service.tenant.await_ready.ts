import { PulsarTenantInK8s } from '@/classes/PulsarTenant';
import configPulsarTenant from '@/configs/config.pulsar.tenant';
import logger from '@/log/logger.default';
import { ResourceMeta } from '@dot-i/k8s-operator';
import wait from '@root/test/helpers/wait';
import { AxiosError } from 'axios';
import getFullName from '@/services/shared/misc/service.shared.get_full_name';
import restClientPulsar from '@/services/shared/service.shared.get_pulsar_rest_client';

export const getService = (client = restClientPulsar, config = configPulsarTenant) =>
  async (
    meta: ResourceMeta,
    tenant: PulsarTenantInK8s,
    attempt = 1
  ) => {
    try {
      const result = await client.get(
        `/tenants/${meta.name}`
      );
      logger.info(`${getFullName(meta)}: Tenant is now ready`);
      return result.data;
    } catch (err:any) {
      const readiness = config.probing.readiness;
      if (err instanceof AxiosError)
        if (err.status === 404 && attempt < readiness.maxAttempts) {
          logger.info(`${getFullName(meta)}: Tenant not found Attempt ${attempt}/${readiness.maxAttempts}. Retriying`);
          await wait((attempt+1)*readiness.intervalIncrementorMs);
          return await getService(client,config)(meta, tenant, attempt+1);
        } else if (err.status === 404 && attempt >= readiness.maxAttempts){
          logger.error(`${getFullName(meta)}: Tenant did not turn ready. Giving up`);
          return null;
        } else {
          logger.error(`${getFullName(meta)}: Tenant could not be created`);
          logger.error(`${getFullName(meta)}: ${err.status}-${err.message}`);
        }
      else 
        logger.error(`${getFullName(meta)}: Unkown error waiting for tenant to become ready`);
    }
  };

export default getService();