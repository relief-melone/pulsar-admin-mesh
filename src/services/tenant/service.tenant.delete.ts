
import { PulsarTenantInK8s } from '@/classes/PulsarTenant';
import logger from '@/log/logger.default';
import { ResourceEvent } from '@dot-i/k8s-operator';
import logError from '../errors/service.errors.try_to_log_error_message';
import getFullName from '../shared/misc/service.shared.get_full_name';
import pulsarRestClient from '../shared/service.shared.get_pulsar_rest_client';
import { StatusPatcher } from '../shared/k8s/service.shared.k8s.get_status_patcher';
import resourceStillExists from '../shared/k8s/service.shared.k8s.resource_exists';
import getInternalTenant from './misc/service.tenant.get_internal_from_pulsar';
import deleteTenant from './misc/service.tenant.rest.delete_one';
import getPulsarTenant from './misc/service.tenant.rest.get_one';

export const getService = (client = pulsarRestClient) =>
  async (
    e: ResourceEvent,
    patchStatus: StatusPatcher<PulsarTenantInK8s>
  ):Promise<void> => {
    const meta = e.meta;    
    logger.info(`${getFullName(e.meta)}: Deleting tenant`);

    try {
      const resourceExists = await resourceStillExists(meta, 'pulsartenants');
      if (!resourceExists){
        logger.info(`${getFullName(meta)}: tenant has already been in k8s deleted. Nothing to do...`);
        return;
      }

      await patchStatus({
        state: 'terminating'
      });
      
      const tenantPulsar = await getPulsarTenant(meta);
      if (!tenantPulsar){
        logger.error(`${getFullName(meta)}: tenant could not be found in pulsar`);
        return;
      }

      const tenant = getInternalTenant(meta, tenantPulsar);
      await deleteTenant(tenant);

      logger.info(`${getFullName(meta)}: tenant deletion process complete`);
      
    } catch (err: any) {
      if (err.response?.status === 404){
        logger.warn(`${getFullName(meta)}: tenant was already deleted`);
        return;
      }
      // FIXME: why is this throwing here
      logError(err, `${getFullName(meta)}: could not delete tenant in pulsar`);
    }
  };

export default getService();