import { PulsarTenantInPulsar, PulsarTenantInternal } from '@/classes/PulsarTenant';
import { getNamespace } from '@/services/shared/service.shared.get_namespace';
import { ResourceMeta } from '@dot-i/k8s-operator';

export default (tenantMeta: ResourceMeta, tenantPulsar: PulsarTenantInPulsar ):PulsarTenantInternal => 
  new PulsarTenantInternal({
    name: tenantMeta.name,
    k8s: { 
      namespace: getNamespace(tenantMeta),
      meta: tenantMeta
    },
    config: tenantPulsar,
  });