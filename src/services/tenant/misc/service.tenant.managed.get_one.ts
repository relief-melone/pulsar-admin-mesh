import { PulsarTenantInK8s } from '@/classes/PulsarTenant';
import ConfigK8s from '@/configs/config.k8s';
import ConfigTenant from '@/configs/config.pulsar.tenant';
import { ResourceMeta } from '@dot-i/k8s-operator';
import getFullName from '@/services/shared/misc/service.shared.get_full_name';
import { getNamespace } from '@/services/shared/service.shared.get_namespace';
import logErr from '@/services/errors/service.errors.try_to_log_error_message';

export const getService = (configK8s = ConfigK8s, configTenant = ConfigTenant) => 
  async (metaWithNamespace: ResourceMeta, tenantName: string): Promise<PulsarTenantInK8s | null> => {
    const namespace = getNamespace(metaWithNamespace);
    const { apiCustom } = configK8s;

    try {
      const tenant = await apiCustom.getNamespacedCustomObject(
        configTenant.main.group,
        configTenant.main.version,
        namespace,
        configTenant.main.plural,
        tenantName
      );
      return tenant.body as PulsarTenantInK8s;
    } catch (err: any){
      logErr(err, `${getFullName(metaWithNamespace)}: could not get tenant ${tenantName}`);
      return null;
    }
  };

export default getService();
