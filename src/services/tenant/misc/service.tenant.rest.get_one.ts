import { PulsarTenantInPulsar } from '@/classes/PulsarTenant';
import { ResourceMeta } from '@dot-i/k8s-operator';
import getFullName from '@/services/shared/misc/service.shared.get_full_name';
import PulsarRestClient from '@/services/shared/service.shared.get_pulsar_rest_client';
import logError from '@/services/errors/service.errors.try_to_log_error_message';

export const getService = ( client = PulsarRestClient) =>
  async (metaTenant: ResourceMeta):Promise<PulsarTenantInPulsar| null> => {
    try {
      const current = await client.get<PulsarTenantInPulsar>(
        `tenants/${metaTenant.name}`
      );
      return current.data;
    } catch (err:any) {
      if (err.response?.status === 404)
        return null;
      else {
        logError(err,`${getFullName(metaTenant)}: error getting current tenant from pulsar api. Message was ${err.message}` );
        throw err;
      }
    }
    
  };

export default getService();