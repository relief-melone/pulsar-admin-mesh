import { Pulsartenant } from '@/classes/crd.pulsar_tenant';
import { PulsarTenantInK8s } from '@/classes/PulsarTenant';
import { ResourceEvent } from '@dot-i/k8s-operator';
import { KubernetesObject } from '@kubernetes/client-node';
import getFullName from '../../shared/misc/service.shared.get_full_name';
import { tenantIsValid } from '../validations/service.tenant.is_valid';

export default (ev: ResourceEvent):PulsarTenantInK8s => {
  const tenant = ev.object as PulsarTenantInK8s;
  if (!tenantIsValid(ev.meta, tenant))
    throw new Error('Tenant is not valid');

  return tenant;
};
  