import get_pulsar_rest_client from '@/services/shared/service.shared.get_pulsar_rest_client';

export const getService = ( client = get_pulsar_rest_client) =>
  async () => {
    const tenants = await client.get(
      '/tenants'
    );
    return tenants;
  };

export default getService();
