import { PulsarTenantInternal } from '@/classes/PulsarTenant';
import configPulsarNamespace from '@/configs/config.pulsar.namespace';
import logger from '@/log/logger.default';
import deleteK8sNamespaces from '@/services/namespaces/k8s/service.namespace.k8s.delete_namespaces_for_tenant';
import getFullName from '@/services/shared/misc/service.shared.get_full_name';
import PulsarRestClient from '@/services/shared/service.shared.get_pulsar_rest_client';
import awaitTenantDeletion from '../conditions/service.tenant.await_deleted';


export const getService = (client = PulsarRestClient, configNs = configPulsarNamespace) => 
  async (tenant: PulsarTenantInternal):Promise<void> => {

    logger.info(`${getFullName(tenant.k8s?.meta)}: deleting tenant in pulsar...`);
      
    // TODO: orphaned namespace functionality should be implemented before this config is treated
    if (configNs.deleteNamespacesWithTenant || true)
      await deleteK8sNamespaces(tenant);

    // TODO: delete non managed namespaces
    // await deleteRemainingNamespaces(tenant)

    await client.delete(`tenants/${tenant.name}`);
    await awaitTenantDeletion(tenant);

    logger.info(`${getFullName(tenant.k8s?.meta)}: tenant and child resources have been successfully deleted from pulsar`);
    
  };

export default getService();