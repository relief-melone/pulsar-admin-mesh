import logger from '@/log/logger.default';
import { ResourceEvent } from '@dot-i/k8s-operator';
import getFullName from '../shared/misc/service.shared.get_full_name';
import createTenant from './service.tenant.create';
import getCurrentPulsarTenant from './misc/service.tenant.rest.get_one';
import getTenantFromResourceEvent from './misc/service.tenant.get_from_resource_event';
import updateTenant from './service.tenant.update';
import { StatusPatcher } from '../shared/k8s/service.shared.k8s.get_status_patcher';
import { PulsarTenantInK8s } from '@/classes/PulsarTenant';
import logError from '../errors/service.errors.try_to_log_error_message';
import updateIsNecessary from './conditions/service.tenant.update_is_necessary';
import setLastApplied from '../shared/k8s/service.shared.k8s.set_last_applied_config';
import configPulsarTenant from '@/configs/config.pulsar.tenant';
import operatorHandlesTenant from './validations/service.tenant.validations.handles_tenant';

export const getService = ( configTenant = configPulsarTenant ) =>
  async (e:ResourceEvent, sp: StatusPatcher<PulsarTenantInK8s>) => {
    const meta = e.meta;
    const updatedTenantK8s = getTenantFromResourceEvent(e);
    const tenantPulsar = await getCurrentPulsarTenant(meta);

    try {
      if (tenantPulsar){
        if (updateIsNecessary(meta, updatedTenantK8s)){
          logger.debug(`${getFullName(meta)}: tenant exists and an update is necessary. patching...`);
          await updateTenant(meta, updatedTenantK8s, tenantPulsar, sp);
          await setLastApplied(updatedTenantK8s, configTenant);
        } else {
          logger.debug(`${getFullName(meta)}: tenant exists but does not require updating`);
        }
      } else {
        logger.debug(`${getFullName(e.meta)}: tenant does not yet exist. creating new tenant...`);
        await createTenant(meta, updatedTenantK8s, sp);
        await setLastApplied(updatedTenantK8s, configTenant);

      }
    } catch (err) {
      logError(err), `${getFullName(meta)}: Error in CreateOrUpdate tenant`;
    }
    
  };

export default getService();