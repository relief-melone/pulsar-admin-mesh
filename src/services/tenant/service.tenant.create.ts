import { PulsarTenantInK8s } from '@/classes/PulsarTenant';
import logger from '@/log/logger.default';
import Operator, { ResourceEvent, ResourceMeta } from '@dot-i/k8s-operator';
import getErrorPatch, { ErrorTypeTenant } from '../errors/service.errors.tenant.get_error_patch';
import getFullName from '../shared/misc/service.shared.get_full_name';
import PulsarRestClient from '../shared/service.shared.get_pulsar_rest_client';
import { StatusPatcher } from '../shared/k8s/service.shared.k8s.get_status_patcher';
import waitForTenantReady from './conditions/service.tenant.await_ready';
import tenantPresentInDifferentNamespace from './validations/service.tenant.k8s_tenant_present_in_different_namespace';
import configPulsarTenant from '@/configs/config.pulsar.tenant';
import logError from '../errors/service.errors.try_to_log_error_message';
import errorIsClustersDoNotExist from '../errors/service.errors.tenant.is_cluster_does_not_exist';

export const getService = (client = PulsarRestClient, configTenant = configPulsarTenant) =>
  async (
    meta: ResourceMeta, 
    tenant: PulsarTenantInK8s,
    patchStatus: StatusPatcher<PulsarTenantInK8s>
  ):Promise<void> => {
    logger.info(`${getFullName(meta)}: creating new tenant...`);

    if (await tenantPresentInDifferentNamespace(tenant)){
      const errPatch = getErrorPatch(ErrorTypeTenant.TenantAlreadyExistsInK8s);
      logger.error(errPatch.statusMessage);
      await patchStatus(errPatch);
      return;
    }

    const { adminRoles, allowedClusters } = tenant.spec.config;

    try {
      await patchStatus({
        state: 'creating'
      });
      const result = await client.put(
        `/tenants/${meta.name}`,
        {
          adminRoles,
          allowedClusters
        }
      );
      await waitForTenantReady(meta, tenant);
      await patchStatus({
        state: 'active'
      });
      logger.info(`${getFullName(meta)}: Successfully created new tenant`);
    } catch (err) {
      if (errorIsClustersDoNotExist(err)){
        await patchStatus(getErrorPatch(ErrorTypeTenant.ClustersDoNotExist));
        return;
      }
      
      logError(err, `${getFullName(meta)}: Could not create new tenant`);
      await patchStatus(getErrorPatch(ErrorTypeTenant.InternalError));
    }
  };

export default getService();