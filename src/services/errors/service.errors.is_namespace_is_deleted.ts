export default (err:any):boolean => 
  err.name === 'AxiosError' &&
  err.response?.status === 412 &&
  err.response?.data?.reason &&
  err.response.data.reason.toLowerCase().includes('namespace is deleted');