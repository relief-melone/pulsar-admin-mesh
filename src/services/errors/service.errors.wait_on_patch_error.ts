export default (attemptNr: number, baseValue= 500, maxWaitTime=60000):number => {
  const calculated = Math.pow(2, attemptNr)*baseValue;
  return calculated > maxWaitTime
    ? maxWaitTime
    : calculated;
};