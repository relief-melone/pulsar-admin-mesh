import isPrimitive from '@/util/util.is_primitive';
import logger from '@/log/logger.default';
import { AxiosError } from 'axios';
import { stringify } from 'yaml';

export default (err: any, leadingMessages?: string, additionalMessages: string[] = []):void => {
  if (leadingMessages)
    logger.error(leadingMessages);

  if (err instanceof AxiosError){
    logger.error('AXIOS ERROR');
    logger.error(`\thost: ${err.request.host}`);
    logger.error(`\tmethod: ${err.request.method}`);
    logger.error(`\tpath: ${err.request.path}`);
    logger.error(`\tstatus: ${err.response?.status}`);
    logger.error(`\tstatusText: ${err.response?.statusText}`);
    logger.error(`\tdata: 
\t\t${stringify(err.response?.data || {}, undefined, 2)}`);
  }
  
  if (isPrimitive(err))
    logger.error(`\t${err}`);
  else {
    if (err.statusCode)
      logger.error(`\tstatusCode: ${err.statusCode}`);
    if (err.message)
      logger.error(`\terr.message: ${err.message}`);
    if (err.response?.message)
      logger.error(`\terr.response.message: ${err.message}`);
  }

  for (const msg of additionalMessages)
    logger.error(`\t${msg}`);


};