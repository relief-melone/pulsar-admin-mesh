import { StatusPatch, State } from '@/classes/CustomResourceShared';

export enum ErrorTypeNamespace {
  TenantDoesNotExist = 'tenant does not exist',
  UpdateFailed = 'namespace does not exist',
  InvalidConfiguration = 'invalid configuration',
  InternalError = 'internal error'
}

// TODO: include error in parameters to return better status messages
export const getErrorPatch = (
  type: ErrorTypeNamespace, 
  messageOverride?: string
): StatusPatch => {
  const state:State = 'error';
  
  return {
    state,
    statusMessage: messageOverride || type
  }; 
};


export default getErrorPatch;
