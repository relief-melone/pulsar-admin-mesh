export default (err:any):boolean => 
  err.name === 'AxiosError' &&
  err.response?.status === 404 &&
  err.response?.data?.reason?.toLowerCase().includes('tenant not found');
