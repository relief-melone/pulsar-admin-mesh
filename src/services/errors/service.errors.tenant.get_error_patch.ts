import { StatusPatch, State } from '@/classes/CustomResourceShared';
import deepEquals from 'fast-deep-equal';

export enum ErrorTypeTenant {
  TenantAlreadyExistsInK8s = 'tenant already exists in another namespace',
  ClustersDoNotExist = 'Clusters do not exist',
  InternalError = 'internalError'
}

export const getErrorPatch = (type: ErrorTypeTenant, messageOverride?:string):StatusPatch => {
  const state:State = 'error';

  return {
    state,
    statusMessage: messageOverride || type
  };

};


export const isErrorPatch = (type: ErrorTypeTenant, patch: Partial<StatusPatch>):boolean => {
  const compareWith = getErrorPatch(type);
  const { state, statusMessage } = patch;
  return deepEquals(compareWith, { state, statusMessage });
};

export default getErrorPatch;