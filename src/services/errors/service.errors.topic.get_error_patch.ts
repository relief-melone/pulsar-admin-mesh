import { StatusPatch, State } from '@/classes/CustomResourceShared';

export enum ErrorTypeTopic {
  NamespaceDoesNotExist,
  UpdateFailed,
  InvalidConfiguration,
  InternalError
}

export const getErrorPatch = (
  type: ErrorTypeTopic,
  messageOverride?: string,
): StatusPatch => {
  const state:State = 'error';
  let statusMessage: string;

  switch (type){
    case ErrorTypeTopic.NamespaceDoesNotExist:
      statusMessage = 'namespace does not exist';
      break;
    case ErrorTypeTopic.UpdateFailed:
      statusMessage = 'topic update failed';
      break;
    case ErrorTypeTopic.InternalError:
      statusMessage = 'internal error';
      break;
    case ErrorTypeTopic.InvalidConfiguration:
      statusMessage = 'invalid configuration';
  }
  
  return {
    state,
    statusMessage: messageOverride || statusMessage
  }; 
};


export default getErrorPatch;
