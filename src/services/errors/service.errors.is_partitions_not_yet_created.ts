export default (err:any):boolean =>
  err.response?.status === 404 &&
  err.response?.statusText === 'Topic partitions were not yet created';