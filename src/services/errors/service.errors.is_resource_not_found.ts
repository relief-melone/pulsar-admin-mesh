export default (err: any):boolean => 
  err.name === 'HttpError'
  && err.statusCode === 404;