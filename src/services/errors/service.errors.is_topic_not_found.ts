export default (err: any):boolean => 
  err.name === 'AxiosError' &&
  err.response?.status === 404 &&
  typeof err.response?.data?.reason === 'string' &&
  (
    err.response.data.reason.toLowerCase().includes('topic not found') ||
    err.response.data.reason.toLowerCase().includes('topic not exist') ||
    err.response.data.reason.toLowerCase().includes('topic does not exist') ||
    !!(err.response.data.reason.toLowerCase() as string).match(/topic.* not found/)
  );