import { AxiosError } from 'axios';

export const errorPresentsReason = (err:any):boolean =>
  err instanceof AxiosError && !!err.response?.data.reason;

export const getReason = (err:any):string =>
  errorPresentsReason(err)
    ? err.response.data.reason
    : '';