import { AxiosError } from 'axios';

export default (err:any):boolean => 
  err instanceof AxiosError &&
  err.response?.status === 412 &&
  err.response.statusText.toLowerCase().includes('clusters do not exist');