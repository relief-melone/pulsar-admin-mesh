export default (msg: string, headlineLength = 80, fillerSymbol = '='): string => {
  const fillerLength = Math.floor((headlineLength - msg.length)/2 - 1) ;
  const filler = new Array(fillerLength).fill(fillerSymbol).join('');
  return `${filler} ${msg.toUpperCase()} ${filler}`;
};