import { PulsarTopicInternal } from '@/classes/PulsarTopic';

export const getTopicTypePrefix = (persistent: boolean) => persistent ? 'persistent' : 'non-persistent';
export const getTopicTypePrefixForInternalTopic = (topic:PulsarTopicInternal):string => getTopicTypePrefix(topic.persistent);