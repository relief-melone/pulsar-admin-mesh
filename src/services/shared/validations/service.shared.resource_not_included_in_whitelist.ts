import { CustomResourceDebugConfig } from '@/classes/Internal';

export interface MinConfig {
  debug: CustomResourceDebugConfig
}

export const resourceIncludedInWhitelist = (resourceName: string, config: MinConfig): boolean => 
  !config.debug.whitelist
  || (config.debug.whitelist.includes(resourceName));

export const resourceNotIncludedInWhitelist = (resourceName: string, config: MinConfig): boolean => 
  !resourceIncludedInWhitelist(resourceName, config);

export default resourceNotIncludedInWhitelist;