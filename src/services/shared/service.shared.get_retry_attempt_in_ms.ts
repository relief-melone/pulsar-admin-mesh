import { ProbingSettings } from '@/classes/Internal';

export default (retryAttempt: number, probing: ProbingSettings):number =>
  retryAttempt * probing.intervalIncrementorMs;