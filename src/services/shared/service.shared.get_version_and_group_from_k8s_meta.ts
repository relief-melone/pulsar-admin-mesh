import { ResourceMeta } from '@dot-i/k8s-operator';

export default (meta: ResourceMeta) => ({
  version: meta.apiVersion.split('/')[1],
  group: meta.apiVersion.split('/')[0],
});