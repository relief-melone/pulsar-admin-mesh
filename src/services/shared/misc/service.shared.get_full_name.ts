import logger from '@/log/logger.default';
import { ResourceMeta } from '@dot-i/k8s-operator';
import { KubernetesObject } from '@kubernetes/client-node';

export default (obj?: ResourceMeta | null | KubernetesObject):string => {
  if (!obj){
    logger.error('cannot log full name as no object was provided. returning name as [N/A]');
    return '[N/A]';
  }
  
  const isKubeObject = Object.keys(obj).includes('metadata');
  if (isKubeObject){
    const kObject = obj as KubernetesObject;
    return `[${kObject.apiVersion}/${kObject.kind} ns=${kObject.metadata?.namespace || 'n/a'} name=${kObject.metadata?.name || 'n/a'}]`;
  } else {
    const meta = obj as ResourceMeta;
    return `[${meta.apiVersion}/${meta.kind} ns=${meta.namespace || 'n/a'} name=${meta.name}]`;
  }
};

