import { ConfigCustomResource, CustomResourceInfo } from '@/classes/Internal';

export default (
  known: string, 
  configs:ConfigCustomResource[], 
  knownType: keyof CustomResourceInfo = 'kind'
):CustomResourceInfo => 
  configs.filter(c => c.main[knownType] === known )[0]?.main;
