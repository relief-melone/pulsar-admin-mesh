import { ConfigValueUpdateType } from '@/classes/Internal';
import deepEqual from 'fast-deep-equal';

export default <T>(now: T|null, desired: T|null):ConfigValueUpdateType => {
  if (now === null && desired !== null)
    return ConfigValueUpdateType.ADDED;

  else if (now !== null && desired === null)
    return ConfigValueUpdateType.REMOVED;

  else if (
    typeof desired === 'object' 
    && desired !== null
    && Object.keys(desired).length === 0
    && typeof now === 'object'
    && now !== null
    && Object.keys(now).length !== 0
  ){
    return ConfigValueUpdateType.REMOVED;
  } 

  else if (
    typeof desired === 'object' 
    && desired !== null
    && Object.keys(desired).length === 0
    && typeof now === 'object'
    && now !== null
    && Object.keys(now).length === 0
  ){
    return ConfigValueUpdateType.UNCHANGED;
  }
  
  else {
    return deepEqual(now, desired)
      ? ConfigValueUpdateType.UNCHANGED
      : ConfigValueUpdateType.CHANGED;
  }
    
};