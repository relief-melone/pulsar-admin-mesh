import { CustomResourceInfo } from '@/classes/Internal';

export default (input: CustomResourceInfo):string => 
  `${input.group}/${input.version}`;