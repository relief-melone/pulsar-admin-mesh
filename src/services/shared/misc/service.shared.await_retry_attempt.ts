import { ProbingSettings } from '@/classes/Internal';
import logger from '@/log/logger.default';
import wait from '@root/test/helpers/wait';

import getRetryAttemptTime from '../service.shared.get_retry_attempt_in_ms';

export default async (
  retryAttempt: number, 
  probing: ProbingSettings,
  logMessage?: string,
  log = logger.info
):Promise<void> => {
  const retryInMs = getRetryAttemptTime(retryAttempt, probing);
  if (logMessage){
    let message = `${logMessage}. \t Attempt ${retryAttempt}/${probing.maxAttempts} \t`;
    if (retryAttempt< probing.maxAttempts)
      message += `Retrying in ${retryInMs} ms...`;
    log(message);
  }
  await wait(retryInMs);
};