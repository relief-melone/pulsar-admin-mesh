import configPulsarCluster from '@/configs/config.pulsar.cluster';
import axios, { AxiosInstance } from 'axios';
import logErr from '../errors/service.errors.try_to_log_error_message';
import logger from '@/log/logger.default';
import ConfigDebug from '@/configs/config.debug';

let succesfullReqCount = 0;
let failureReqCount = 0;
let successfullResCount = 0;
let failureResCount = 0;

export const getService = (config = configPulsarCluster, configDebug = ConfigDebug):AxiosInstance => {
  logger.info('creating rest client');
  const client = axios.create({
    baseURL: `${ config.service.apiEndpoint }`,
    headers: {
      'content-type': 'application/json'
    }
  });

  
  
  if (configDebug.httpTracing.enabled){    
    
    let requestsOnLastCheck = 0;
    let requestRate = 0;
    const intervalMs = 5000;
    setInterval(() => {
      const reqDiff = succesfullReqCount + failureReqCount - requestsOnLastCheck;
      requestsOnLastCheck = succesfullReqCount + failureReqCount;
      requestRate = Math.round(reqDiff/(intervalMs/1000)*100)/100;
    }, intervalMs);  

    client.interceptors.request.use(
      req => {
        logger.debug(`REQUEST SUCCESS
        url: ${req.baseURL}/${req.url}
        method: ${req.method}
        requestRate:  ${requestRate} /s
        `);
        succesfullReqCount++;
        return req;
      },
      err => {
        failureReqCount++;
        return err;
      }
    );
  
    client.interceptors.response.use(
      res => {
        logger.debug(`RESPONSE SUCCESS
        url:          ${res.config.baseURL}/${res.config.url}
        method:       ${res.config.method}
        status:       ${res.status}
        text:         ${res.statusText}
        successRate:  ${succesfullReqCount}/${succesfullReqCount + failureReqCount}
        requestRate:  ${requestRate} /s
        `);
  
        successfullResCount++;
        return res;
      },      
      res => {
        logger.debug(`RESPONSE ERROR
        url:          ${res.config.baseURL}/${res.config.url}
        method:       ${res.config.method}
        status:       ${res.status}
        text:         ${res.statusText}
        successRate:  ${successfullResCount}/${successfullResCount + failureResCount}
        requestRate:  ${requestRate} /s
        `);
        failureResCount++;
        logErr(res);
        return res;
      }
    );
  }
  
  

  return client;

};
  

export default getService();