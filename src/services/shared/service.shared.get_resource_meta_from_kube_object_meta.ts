import { ConfigCustomResource } from '@/classes/Internal';
import { ResourceMeta } from '@dot-i/k8s-operator';
import { KubernetesObject } from '@kubernetes/client-node';
import getFullName from './misc/service.shared.get_full_name';
import { getNamespace } from './service.shared.get_namespace';

export interface InputMin  {
  metadata: ResourceMeta | KubernetesObject['metadata']
}

export default (
  input: KubernetesObject | InputMin,
):ResourceMeta => {
  const undeterminedMeta = input.metadata;
  if (!undeterminedMeta)
    throw new Error('cannot get resource meta from kubeobject. no metadata found');
  

  const annotations = (undeterminedMeta as Record<string,any>).annotations;
  const lastApplied = annotations?.['kubectl.kubernetes.io/last-applied-configuration']
    ? JSON.parse(annotations['kubectl.kubernetes.io/last-applied-configuration']) 
    : null;

  const isResourceMeta = !lastApplied && !!undeterminedMeta;

  const metadata = isResourceMeta 
    ? input.metadata as KubernetesObject['metadata']
    : input.metadata as ResourceMeta;

  const apiVersion = isResourceMeta
    ? (metadata as ResourceMeta).apiVersion || (input as Record<string,any>).apiVersion
    : lastApplied.apiVersion || (metadata as Record<string,any>).apiVersion || (input as Record<string,any>).apiVersion;

  const kind = isResourceMeta
    ? (metadata as ResourceMeta).kind || (input as Record<string,any>).kind
    : lastApplied.kind || (input as Record<string,any>).kind;

  const resourceVersion = isResourceMeta
    ? (metadata as ResourceMeta).resourceVersion
    : lastApplied.resourceVersion || metadata?.resourceVersion;

  const namespace = isResourceMeta
    ? getNamespace(metadata)
    : getNamespace(lastApplied.metadata);
  
  const id = isResourceMeta
    ? (metadata as ResourceMeta).id || (metadata as Record<string,any>).uid
    : lastApplied.uid || (metadata as Record<string,any>).uid;

  const name = isResourceMeta
    ? (metadata as ResourceMeta).name
    : lastApplied.metadata.name || metadata?.name;

  if (!apiVersion)
    throw new Error('cannot get resource meta from kube object. no apiVersion could be determined');
  if (!kind)
    throw new Error('cannot get resource meta from kube object. no kind could be determined');
  if (!resourceVersion)
    throw new Error('cannot get resource meta from kube object. no resourceVersion could be determined');
  if (!name)
    throw new Error('cannot get resource meta from kube object. no name could be determined');
  if (!id)
    throw new Error('cannot get resource meta from kube object. no id could be determined');


  return {
    apiVersion,
    resourceVersion,
    id,
    kind,
    name,
    namespace
  };

    
};