import logger from '@/log/logger.default';
import Operator from '@dot-i/k8s-operator';

const shutdown = async (operators: Operator[], evt: NodeJS.Signals) => {
  logger.info(`Received ${evt} Shutting down operators...`);
  for (const operator of operators) {
    await operator.stop();
  }
  logger.info('Operators shut down');
  logger.info('Exiting Application now');
  process.exit(0);
};

export const setShutdown = (operators:Operator[]) => {
  
  // process.on('SIGABRT', () => shutdown(operators));
  process.on('SIGINT', (e) => shutdown(operators, e));
  process.on('SIGTERM', (e) => shutdown(operators, e));
  // process.on('SIGKILL', () => shutdown(operators));
};