import { ResourceMeta } from '@dot-i/k8s-operator';
import { V1CustomResourceDefinition } from '@kubernetes/client-node';

export default (input: V1CustomResourceDefinition):ResourceMeta => {
  const apiVersion = input.apiVersion;
  const id = input.metadata?.uid;
  const kind = input.kind;
  const name = input.metadata?.name;
  const resourceVersion = input.metadata?.resourceVersion;
  const namespace = input.metadata?.namespace;

  if (!apiVersion)
    throw new Error('cannot get resource meta from custom resource definition. no apiVersion could be determined');
  if (!kind)
    throw new Error('cannot get resource meta from custom resource definition. no kind could be determined');
  if (!resourceVersion)
    throw new Error('cannot get resource meta from custom resource definition. no resourceVersion could be determined');
  if (!id)
    throw new Error('cannot get resource meta from custom resource definition. no name could be determined');
  if (!name)
    throw new Error('cannot get resource meta from custom resource definition. no id could be determined');
  return {
    apiVersion,
    id,
    kind,
    name,
    resourceVersion,
    namespace
  };
};