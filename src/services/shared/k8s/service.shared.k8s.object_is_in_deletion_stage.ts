import { ResourceEvent, ResourceEventType } from '@dot-i/k8s-operator';

export default (e: ResourceEvent):boolean => 
  !!e.object.metadata?.deletionTimestamp &&
  ( e.type === ResourceEventType.Added || e.type === ResourceEventType.Modified);
