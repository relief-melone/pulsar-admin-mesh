import ConfigK8s from '@/configs/config.k8s';
import logErr from '@/services/errors/service.errors.try_to_log_error_message';

export const getService = ( configK8s = ConfigK8s) =>
  async ():Promise<Array<string>> => {
    try {
      const { apiCore } = configK8s;
      const namespaceList = await apiCore.listNamespace();
      return namespaceList.body.items
        .map( i => i.metadata?.name)
        .filter(i => i !== undefined) as Array<string>;

    } catch (err) {
      logErr(err, 'could not get namespace list from cluster');
      throw err;
    }
  };

export default getService();