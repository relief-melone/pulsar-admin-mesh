import ConfigK8s from '@/configs/config.k8s';
import logger from '@/log/logger.default';
import logError from '@/services/errors/service.errors.try_to_log_error_message';
import { ResourceMeta } from '@dot-i/k8s-operator';
import { KubernetesObject } from '@kubernetes/client-node';
import getFullName from '../misc/service.shared.get_full_name';
import getResource from './service.shared.k8s.get_resource';

export const getService = () =>
  async <T extends KubernetesObject> (meta: ResourceMeta, plural:string):Promise<T | null> => {
    
    const group = meta.apiVersion.split('/')[0];
    const version = meta.apiVersion.split('/')[1];
    const namespace = meta.namespace;
    
    if (!namespace){
      const msg = `${getFullName}: Cannot check if resource still exists. No namespace present`;
      logger.error(msg);
      throw new Error(msg);
    }

    return await getResource<T>(group, version, namespace, plural, meta.name);
    


  };

export default getService();