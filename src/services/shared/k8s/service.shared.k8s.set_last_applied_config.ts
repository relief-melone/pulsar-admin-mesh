import { ConfigCustomResource, CustomResourceInfo } from '@/classes/Internal';
import  ConfigK8s  from '@/configs/config.k8s';
import logErr from '@/services/errors/service.errors.try_to_log_error_message';
import { KubernetesObject, PatchUtils } from '@kubernetes/client-node';
import getFullName from '../misc/service.shared.get_full_name';
import { getNamespace } from '../service.shared.get_namespace';
import getResourceMeta from '../service.shared.get_resource_meta_from_kube_object_meta';
import getGroupAndVersion from '../service.shared.get_version_and_group_from_k8s_meta';

export const getService = ( configK8s = ConfigK8s ) => 
  async <T extends KubernetesObject & {[k:string]:any}>(item: T, config: ConfigCustomResource):Promise<void> => {
    const meta = getResourceMeta(item);
    const { apiCustom, apiCore } = configK8s;
    const { group, kind, plural, version } = config.main;
  

    const lastApplied:Partial<KubernetesObject & {[k:string]:any}> = {
      apiVersion: item.apiVersion,
      kind: item.kind,
      metadata: {  
        name: item.metadata?.name,
        namespace: item.metadata?.namespace
      }
    };

    if (item.spec){
      lastApplied.spec = item.spec;
    }

    try {
      await apiCustom.patchNamespacedCustomObject(
        group,version, getNamespace(meta), plural, meta.name, {
          metadata: { annotations: {
            'pulsar.admin.io/last-applied-configuration': JSON.stringify(lastApplied),
            'pulsar.admin.io/last-applied-configuration-timestamp': new Date().toISOString()
          } }
        }, undefined, undefined, undefined, {
          headers: {
            'content-type': PatchUtils.PATCH_FORMAT_JSON_MERGE_PATCH
          }
        }
      );
    } catch (err:any) {
      logErr(err, `${getFullName(getResourceMeta(item))}: could not apply annotation for last configured configuration`);
    }
  };

export default getService();