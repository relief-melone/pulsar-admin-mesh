import logger from '@/log/logger.default';
import { KubernetesObject } from '@kubernetes/client-node';

export default (resource: KubernetesObject):string => {
  const name = resource.metadata?.name;
  if (!name){
    const msg = 'Could not get Name from kubernetes object as .metadata.name was not defined';
    logger.error(msg);
    throw new Error(msg);
  } else {
    return name; 
  }
  

};