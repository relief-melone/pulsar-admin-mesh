import logger from '@/log/logger.default';
import { KubernetesObject } from '@kubernetes/client-node';
import getFullName from '../misc/service.shared.get_full_name';
import getMeta from '../service.shared.get_resource_meta_from_kube_object_meta';

export default <T extends KubernetesObject>(input:T):T|null => {
  const lastAppliedAnnotation = 
    input.metadata?.annotations?.['pulsar.admin.io/last-applied-configuration'] ||
    input.metadata?.annotations?.['kubectl.kubernetes.io/last-applied-configuration'];
    
  
  if (!lastAppliedAnnotation)
    logger.warn(`${getFullName(getMeta(input))}: could not get previously applied configuration`);

  return lastAppliedAnnotation
    ? (JSON.parse(lastAppliedAnnotation) as T)
    : null;

};