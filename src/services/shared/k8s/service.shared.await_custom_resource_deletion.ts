import { ProbingSettings } from '@/classes/Internal';
import ConfigK8s from '@/configs/config.k8s';
import logger from '@/log/logger.default';
import { ResourceMeta } from '@dot-i/k8s-operator';
import wait from '@root/test/helpers/wait';
import isCustomResourceNotFound from '../../errors/service.errors.is_resource_not_found';
import awaitRetryAttempt from '../misc/service.shared.await_retry_attempt';
import getFullName from '../misc/service.shared.get_full_name';
import { getNamespace } from '../service.shared.get_namespace';
import getRetryAttemtTime from '../service.shared.get_retry_attempt_in_ms';
import getVersionAndGroup from '../service.shared.get_version_and_group_from_k8s_meta';

export const getService = (configK8s = ConfigK8s) =>
  async (
    meta: ResourceMeta, 
    plural: string, 
    probing: ProbingSettings,
    attempt = 0
  ):Promise<void> => {
    const { group, version } = getVersionAndGroup(meta);
    const { apiCustom } = configK8s;

    try {
      const resource = await apiCustom.getNamespacedCustomObject(
        group,
        version,
        getNamespace(meta),
        plural,
        meta.name
      );
      if (resource){
        if (attempt < probing.maxAttempts){
          await awaitRetryAttempt(
            ++attempt, 
            probing,
            `${getFullName(meta)}: Resource still present.`
          );
          return await getService(configK8s)(meta, plural, probing, attempt);
        } else {
          const msg = `${getFullName(meta)}: Timed out waiting for resources deletion`;
          logger.error(msg);
          throw new Error(msg);
        }
      }
        
    } catch (err:any) {
      if (isCustomResourceNotFound(err)){
        logger.debug(`${getFullName(meta)}: custom resource successfully deleted`);
        return;
      } else {
        const msg = `${getFullName(meta)}: unknown error waiting for custom resource deletion`;
        logger.error(msg);        
      }
    }
  };

export default getService();
