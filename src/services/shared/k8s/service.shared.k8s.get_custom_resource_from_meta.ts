import { ConfigCustomResource } from '@/classes/Internal';
import ConfigK8s from '@/configs/config.k8s';
import configPulsarNamespace from '@/configs/config.pulsar.namespace';
import configPulsarTenant from '@/configs/config.pulsar.tenant';
import configPulsarTopic from '@/configs/config.pulsar.topic';
import isCustomResourceNotFound from '@/services/errors/service.errors.is_resource_not_found';
import { ResourceMeta } from '@dot-i/k8s-operator';
import { KubernetesObject } from '@kubernetes/client-node';
import getCustomResourceInfo from '../misc/service.shared.get_custom_resource_info_from_configs';
import { getNamespace } from '../service.shared.get_namespace';

export const getService = (
  configK8s = ConfigK8s,
  configsCRDs: ConfigCustomResource[] = [
    configPulsarTenant,
    configPulsarNamespace,
    configPulsarTopic
  ]
) => 
  async <T extends KubernetesObject & { spec: Record<string,any>}>(meta: ResourceMeta):Promise<T|null> => {
    const { apiCustom } = configK8s;
    const { group, plural, version } = getCustomResourceInfo(meta.kind, configsCRDs);
    try {
      const currentResource = await apiCustom.getNamespacedCustomObject(
        group, version, getNamespace(meta), plural, meta.name
      );
      return currentResource.body as T;
    } catch (err: any) {
      if (isCustomResourceNotFound(err))
        return null;
      else 
        throw err;
    }
    
  };

export default getService();