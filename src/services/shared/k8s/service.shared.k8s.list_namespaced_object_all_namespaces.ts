import ConfigK8s from '@/configs/config.k8s';
import logError from '@/services/errors/service.errors.try_to_log_error_message';


export const getService = (
  apiCustom = ConfigK8s.apiCustom
) => 
  async <T>(group: string, version: string, plural: string):Promise<T[]> => {
    try {
      const call = await apiCustom.listNamespacedCustomObject(
        group, version, '', plural
      );
      return (call.body as Record<string, any>).items as T[];
    } catch (err) {
      logError(err);
      throw err;
    }
    
  };

export default getService();