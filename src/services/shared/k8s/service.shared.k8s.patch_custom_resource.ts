import { CustomResourceInfo } from '@/classes/Internal';
import { CustomResourceMin } from '@/classes/K8sAutoImporter';
import ConfigK8s from '@/configs/config.k8s';
import logger from '@/log/logger.default';
import { KubernetesObject, PatchUtils } from '@kubernetes/client-node';
import { getNamespace } from '../service.shared.get_namespace';
import getMeta from '../service.shared.get_resource_meta_from_kube_object_meta';

export const getService = (apiCustom: typeof ConfigK8s.apiCustom) =>
  async <STATE extends CustomResourceMin>(
    statusPatch: Partial<STATE['status']>,
    metadata: CustomResourceMin['metadata'],
    resInfo: CustomResourceInfo
  ) => {
    try {
      const { group, version, plural } = resInfo;      
      if (!metadata?.name)
        throw new Error('could not get metadata.name');
      if (!metadata.namespace)
        throw new Error('could not get metadata.namespace');

      await apiCustom.patchNamespacedCustomObjectStatus(
        group, version, metadata.namespace, plural, metadata.name,[{
          op: 'replace',
          path: '/status',
          value: statusPatch
        }],
        undefined, undefined, undefined,
        { headers: { 'content-type': PatchUtils.PATCH_FORMAT_JSON_PATCH } }
      );
      
    } catch (err:any) {
      const msg = 'error patching state';
      logger.warn(msg);
      
      if (err.message)
        logger.warn(err.message);

      return new Error(msg);
    }
  };

export default getService(ConfigK8s.apiCustom);