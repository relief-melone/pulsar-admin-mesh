import K8sResourceGenerationHandledTracker from '@/classes/K8sResourceGenerationHandledTracker';
import { KubernetesObject } from '@kubernetes/client-node';

const generationHandler = new K8sResourceGenerationHandledTracker();

export const resourceGenerationWasRecentlyHandled = (input: KubernetesObject['metadata'] ):boolean =>
  generationHandler.generationWasRecentlyHandled(input?.uid, input?.generation);

export const addHandledResourceGeneration = (input:KubernetesObject['metadata']):void => 
  generationHandler.addHandledGeneration(input?.uid, input?.generation);