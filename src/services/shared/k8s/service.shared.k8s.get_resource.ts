import ConfigK8s from '@/configs/config.k8s';
import isCustomResourceNotFound from '@/services/errors/service.errors.is_resource_not_found';
import logError from '@/services/errors/service.errors.try_to_log_error_message';
import { KubernetesObject } from '@kubernetes/client-node';

export const getService = (configK8s = ConfigK8s) =>
  async <T extends KubernetesObject>(
    group: string,
    version: string,
    namespace: string,
    plural: string,
    name: string
  ):Promise<T| null> => {

    try {
      const res = await configK8s.apiCustom.getNamespacedCustomObject(
        group, version, namespace, plural, name
      );
      return res.body as T;

    } catch (err: any) {
      if (isCustomResourceNotFound(err))
        return null;

      logError(err, `could not get custom resource ${group}/${version}.${plural}/${name} in namespace ${namespace}`);
      throw err;
    }
  };

export default getService();