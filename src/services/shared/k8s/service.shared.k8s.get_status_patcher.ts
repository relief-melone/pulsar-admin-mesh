import logger from '@/log/logger.default';
import Operator, { ResourceMeta } from '@dot-i/k8s-operator';
import { KubernetesObject, Response } from '@kubernetes/client-node';
import { AxiosError } from 'axios';
import getFullName from '../misc/service.shared.get_full_name';
import clone from 'clone-deep';
import logError from '@/services/errors/service.errors.try_to_log_error_message';
import getWaitTime from '@/services/errors/service.errors.wait_on_patch_error';
import wait from '@root/test/helpers/wait';
import ConfigK8s from '@/configs/config.k8s';
import configPulsarTenant from '@/configs/config.pulsar.tenant';
import configPulsarNamespace from '@/configs/config.pulsar.namespace';
import configPulsarTopic from '@/configs/config.pulsar.topic';
import { ConfigCustomResource } from '@/classes/Internal';
import getCustomResourceFromMeta from './service.shared.k8s.get_custom_resource_from_meta';
import { MonitoredItemMin } from '@/classes/StatusMonitorNew';

export type KubernetesObjectMin = KubernetesObject & { status: Record<string,any> };
export type StatusPatcher<T extends KubernetesObjectMin>  = (status:Partial<T['status']>) => Promise<void>;
export type RawStatusPatcher<T extends KubernetesObjectMin>  = (status:Partial<T['status']>, meta: ResourceMeta) => Promise<void>;

export async function rawPatchStatus(
  this: Operator, 
  status: Record<string,any>, 
  meta: ResourceMeta
):Promise<ResourceMeta| null> {
  try {
    const newMeta = await this.patchResourceStatus(
      Object.assign(clone(meta), { resourceVersion: undefined }),
      status
    );
  
    if (newMeta)
      Object.assign(meta, newMeta);
  
    return newMeta;
  } catch (err:any) {
    if (err instanceof AxiosError && err.status === 404){
      logger.info(`${getFullName(meta)}: Could not patch resource as it was already deleted`);
      return null;
    }
    logError(err);
    throw err;
  }
}

export function getRawPatcher<ITEM extends MonitoredItemMin, >(this: Operator):RawStatusPatcher<ITEM> {
  return async function (status: Partial<ITEM['status']>, meta: ResourceMeta) {
    // FIXME: update is necessary should not 
    logger.info(`${getFullName(meta)}: Patching status to ${status.state?.toUpperCase()}`);
    try {
      
      if (status.state === 'error'){
        const currentResource = await getCustomResourceFromMeta<ITEM>(meta);
        if (currentResource){
          status.failedRetries = (currentResource.status.failedRetries || 0) + 1;
          status.lastSpecOnError = JSON.stringify(currentResource.spec);
        }
      } 

      if (status.state === 'active'){
        Object.assign(status, {
          failedRetries: 0,
          lastSpecOnError: null,
          statusMessage: 'OK'
        });
      }
        

      await rawPatchStatus.bind(this)(status, meta);

    } catch (err:any) {
      if (err instanceof AxiosError && err.status === 404){
        logger.info(`${getFullName(meta)}: Could not patch resource as it was already deleted`);
      }
      logError(err);
      throw err;
    }
  };
}

export function getPatchStatus<T extends KubernetesObjectMin & { spec: Record<string,any>}>(this:Operator, meta: ResourceMeta):StatusPatcher<T>{
  return async (status:Record<string,any>):Promise<void> => {
    // FIXME: update is necessary should not 
    logger.info(`${getFullName(meta)}: Patching status to ${status.state?.toUpperCase()}`);
    try {
      
      if (status.state === 'error'){
        const currentResource = await getCustomResourceFromMeta<T>(meta);
        if (currentResource){
          status.failedRetries = (currentResource.status.failedRetries || 0) + 1;
          status.lastSpecOnError = JSON.stringify(currentResource.spec);
        }
      } 

      if (status.state === 'active'){
        Object.assign(status, {
          failedRetries: 0,
          lastSpecOnError: null,
          statusMessage: 'OK'
        });
      }
        

      await rawPatchStatus.bind(this)(status, meta);

    } catch (err:any) {
      if (err instanceof AxiosError && err.status === 404){
        logger.info(`${getFullName(meta)}: Could not patch resource as it was already deleted`);
      }
      logError(err);
      throw err;
    }

  };
}
