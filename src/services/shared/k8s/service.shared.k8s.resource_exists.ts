import { ResourceMeta } from '@dot-i/k8s-operator';
import isCustomResourceNotFound from '@/services/errors/service.errors.is_resource_not_found';
import getResource from './service.shared.k8s.get_resource.from_meta';

export const getService = () =>
  async (meta: ResourceMeta, plural):Promise<boolean> => {
    try {
      const resource = await getResource(meta, plural);
      return !!resource;
    } catch (err) {
      if (isCustomResourceNotFound(err))
        return false;
      else
        throw err;
    }
    

  };

export default getService();