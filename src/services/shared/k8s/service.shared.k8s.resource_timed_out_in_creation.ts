import { DeepPartial } from '@/classes/Helpers';
import { CustomResourceMin } from '@/classes/K8sAutoImporter';
import { PulsarNamespaceInK8s } from '@/classes/PulsarNamespace';
import { PulsarTenantInK8s } from '@/classes/PulsarTenant';
import { PulsarTopicInK8s } from '@/classes/PulsarTopic';
import { ConfigMinStatusMonitor, MonitoredItemMin } from '@/classes/StatusMonitorNew';
import ConfigPulsarNamespace from '@/configs/config.pulsar.namespace';
import ConfigPulsarTenant from '@/configs/config.pulsar.tenant';
import ConfigPulsarTopic from '@/configs/config.pulsar.topic';
import logger from '@/log/logger.default';
import { KubernetesObject } from '@kubernetes/client-node';

export const hasResourceTimedOut = <T extends KubernetesObject & Record<string,any>>(
  item:T, 
  timeoutInSeconds = 120
):boolean => {
  if (item.status?.state !== 'creating')
    return false;
    
  if (!item.metadata?.creationTimestamp){
    logger.debug('cannot determine if resource timed out because creationTimestamp not present');
    return false;
  }

  const createdAt = new Date(item.metadata.creationTimestamp);
  const now = Date.now();
  return (now - Number(createdAt)) > timeoutInSeconds*1000;
};

export interface MinConfig extends DeepPartial<ConfigMinStatusMonitor & CustomResourceMin> {
  main: { kind: string }
  statusMonitor: { creationTimeoutInSeconds: number }
}

export default (item: MonitoredItemMin, config: MinConfig):boolean => {
  const timeout = config.statusMonitor.creationTimeoutInSeconds;
  return hasResourceTimedOut(item, timeout);
};