import { ResourceMeta } from '@dot-i/k8s-operator';

export const getNamespace = (meta?: Partial<ResourceMeta>):string => {
  if (!meta?.namespace)
    throw new Error('namespace should have been present but was not');

  return meta.namespace;
};