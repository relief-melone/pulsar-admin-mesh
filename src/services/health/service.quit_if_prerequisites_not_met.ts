import ConfigTenant from '@/configs/config.pulsar.tenant';
import ConfigNamespace from '@/configs/config.pulsar.namespace';
import ConfigTopic from '@/configs/config.pulsar.topic';
import ConfigK8s from '@/configs/config.k8s';
import serviceSharedK8sGet_resource_name from '../shared/k8s/service.shared.k8s.get_resource_name';
import { V1CustomResourceDefinitionList } from '@kubernetes/client-node';
import { CustomResourceInfo } from '@/classes/Internal';
import logger from '@/log/logger.default';
import getApiVersion from '../shared/misc/service.shared.get_api_version_from_cri';

export const resourceExistsInList = (
  list: V1CustomResourceDefinitionList, 
  cri: CustomResourceInfo
) => {
  return list.items.some(i => 
    i.spec.group === cri.group &&
    i.spec.names.plural === cri.plural &&
    i.spec.versions.some(v => v.name === cri.version)
  );  
};

export const getService = (
  exit = process.exit, 
  configK8s = ConfigK8s,
  configTenant = ConfigTenant,
  configNamespace = ConfigNamespace,
  configTopic = ConfigTopic,
) => 
  async () => {
    const { apiExtensions } = configK8s;
    let crdList: V1CustomResourceDefinitionList;

    try {
      const response = await apiExtensions.listCustomResourceDefinition();
      crdList = response.body;
    } catch (err) {
      logger.error('error checking prerequisites. Could not retrieve crd list from kubernetes cluster. exiting...');
      return exit(1);
    }
    
    logger.info('checking for prerequisites: checking if custom resources are installed...');
    
    [
      configTenant,
      configNamespace,
      configTopic
    ]
      .forEach(config => {
        const resourceExists = resourceExistsInList(crdList, config.main);
        if (!resourceExists){
          logger.error(`error checking prerequisites. CRD for ${getApiVersion(config.main)} ${config.main.kind}`);
          return exit(1);
        }
          
      });

    logger.info('checking for prerequisites: all prerequisites met');
    

  };

export default getService();