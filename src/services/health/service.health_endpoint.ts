import logger from '@/log/logger.default';
import http from 'http';

export const getService = (proc = process) => 
  ():Promise<void> => new Promise((res,rej) =>{
    const server = http.createServer((req, res) => {
      if (req.url === '/health'){
        res.writeHead(200);      
        res.end(`
      status:     ${res.statusCode}
      processId:  ${proc.pid},
      memory:     ${Math.round(proc.memoryUsage().heapUsed / 1024 / 1024)} MB
      url:        ${req.url}
`);
      } else {
        res.writeHead(404);
        res.end('404 - not found\n');
      
      }
    });
    server.listen(8080, undefined, undefined, () => {
      logger.info('health endpoint started');
      res();
    });
  });

export default getService();