import { PulsarConfig, PulsarNamespaceInternal, PulsarPermissions } from '@/classes/PulsarNamespace';
import ConfigPulsarTenant from '@/configs/config.pulsar.tenant';
import namespaceNotPresentAnymore from '@/services/errors/service.errors.is_namespace_does_not_exist';
import logErr from '@/services/errors/service.errors.try_to_log_error_message';
import PulsarRestClient from '@/services/shared/service.shared.get_pulsar_rest_client';
import { ResourceMeta } from '@dot-i/k8s-operator';

export const getService = ( client = PulsarRestClient, configTenant= ConfigPulsarTenant ) => 
  async (
    tenant: string, 
    pulsarNamespace: string,
    k8sNamespace: string
  ):Promise<PulsarNamespaceInternal|null> => {
    try {
      const config = (await client.get<PulsarConfig>(
        `namespaces/${tenant}/${pulsarNamespace}`
      )).data;
      const permissions = (await client.get<PulsarPermissions>(
        `namespaces/${tenant}/${pulsarNamespace}/permissions`
      )).data;
      const { kind, group, version } = configTenant.main;
      return {
        config,
        permissions,
        k8s: {
          // Missing id and resourceVersion might pose a problem
          // but may not exist at this point
          meta: {
            apiVersion: `${group}/${version}`,
            kind,
            name: pulsarNamespace,
            namespace: k8sNamespace,
          } as ResourceMeta,
          namespace: k8sNamespace
        },
        name: pulsarNamespace,
        tenant
      };
    } catch (err) {
      if (namespaceNotPresentAnymore(err))
        return null;
      logErr(err);
      throw err;
    }
  };

export default getService();