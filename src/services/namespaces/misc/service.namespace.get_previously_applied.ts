import { PulsarConfig, PulsarNamespaceInK8s, PulsarPermissions } from '@/classes/PulsarNamespace';
import isNamespaceNotFound from '@/services/errors/service.errors.is_namespace_does_not_exist';
import logError from '@/services/errors/service.errors.try_to_log_error_message';
import getResourceFromMeta from '@/services/shared/k8s/service.shared.k8s.get_custom_resource_from_meta';
import getFullName from '@/services/shared/misc/service.shared.get_full_name';
import serviceSharedGet_pulsar_rest_client from '@/services/shared/service.shared.get_pulsar_rest_client';
import getMeta from '@/services/shared/service.shared.get_resource_meta_from_kube_object_meta';

export const getService = (client = serviceSharedGet_pulsar_rest_client) => 
  async (ns: PulsarNamespaceInK8s): Promise<PulsarNamespaceInK8s | null> => {
    const { tenant, name: namespace } = ns.spec;

    const lastApplied = await getResourceFromMeta<PulsarNamespaceInK8s>(getMeta(ns));
    if (!lastApplied)
      return null;

    try {
      const config = (await client.get<PulsarConfig>(
        `namespaces/${tenant}/${namespace}`
      )).data;
      const permissions = (await client.get<PulsarPermissions>(
        `namespaces/${tenant}/${namespace}/permissions`
      )).data;

      lastApplied.spec.config = config;
      lastApplied.spec.permissions = permissions;
      return lastApplied;
    } catch (err:any) {
      if (isNamespaceNotFound(err))
        return null;
      logError(err), `${getFullName(getMeta(ns))}: could not get previously applied config for namespace`;
      return null;
    }
  };

export default getService();