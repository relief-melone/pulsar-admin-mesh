import { PulsarConfig, PulsarNamespaceInternal, PulsarPermissions } from '@/classes/PulsarNamespace';
import { getNamespace } from '@/services/shared/service.shared.get_namespace';
import { ResourceMeta } from '@dot-i/k8s-operator';

export default (
  metaNamespace: ResourceMeta, 
  pulsarConfig: PulsarConfig,
  pulsarPermissions: PulsarPermissions,
  tenant: string
):PulsarNamespaceInternal => ({
  name: metaNamespace.name,
  config: pulsarConfig,
  permissions: pulsarPermissions,
  k8s: {
    namespace: getNamespace(metaNamespace),
    meta: metaNamespace
  },
  tenant
});