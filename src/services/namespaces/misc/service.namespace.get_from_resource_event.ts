import { PulsarNamespaceInK8s } from '@/classes/PulsarNamespace';
import { ResourceEvent } from '@dot-i/k8s-operator';
import { namespaceIsValid } from '../validations/service.namespace.is_valid';

export default (ev: ResourceEvent):PulsarNamespaceInK8s => {
  const ns = ev.object as PulsarNamespaceInK8s;
  if ( !namespaceIsValid(ev.meta, ns)){
    throw new Error('Namespace is not valid');
  }

  return ns;
};