import { PulsarConfig } from '@/classes/PulsarNamespace';
import deepEqual from 'fast-deep-equal';
import clone from 'clone-deep';
import isDefaultValue from './service.namespace.is_default_value';
import uniqueArray from '@/util/util.array.get_unique';
import ConfigPulsarNamespace from '@/configs/config.pulsar.namespace';
import logger from '@/log/logger.default';


const getIgnoredIfNotSetConfigOptions = (config = ConfigPulsarNamespace) => [
  'auth_policies',
  'replication_clusters',
  ...config.debug.ignoredIfNotSetConfigOpts
];

const getIgnoredPulsarSettings = (config = ConfigPulsarNamespace) =>[
  'bundles',
  ...config.debug.ignoredPulsarSettings
];

const specialComparisons = {
  persistence: (configPulsar = {}, configK8s = {}):boolean => 
    uniqueArray([...Object.keys(configPulsar),...Object.keys(configK8s)])
      .every(key => 
        (configPulsar[key] === configK8s[key]) ||
        (configPulsar[key] === 0 && configK8s[key] === undefined) ||
        (configPulsar[key] === undefined && configK8s[key] === 0)
      )
};

export const configsMatchOrSpecialComparisonHasBeenSet =
  (key: string, configPulsar = {}, configK8s = {}):boolean => {
    const result = deepEqual(configK8s[key], configPulsar[key]) || specialComparisons[key];
    if (!result)
      logger.verbose(`pulsarNamespace: configsMismatch and no special comparison set for ${key}`);

    return result;
    
  };

export const specialComparisonMatches = (key: string, configPulsar = {}, configK8s = {}):boolean => {
  const result = specialComparisons[key] && specialComparisons[key](configPulsar[key], configK8s[key]);

  if (!result)
    logger.verbose(`pulsarNamespace: special comparison mismatches for ${key}`);

  return result;
};

export default async (
  cPulsar: PulsarConfig = {},
  configK8s: PulsarConfig = {}, 
): Promise<boolean> => {
  const configPulsar = clone(cPulsar);
  
  const ignoredIfNotSetConfigOptions = getIgnoredIfNotSetConfigOptions();
  const ignoredPulsarSettings = getIgnoredPulsarSettings();

  for (const key of Object.keys(configK8s)){
    if (ignoredIfNotSetConfigOptions.includes(key))
      continue;

    if (
      configsMatchOrSpecialComparisonHasBeenSet(key, configPulsar, configK8s) ||
      specialComparisonMatches(key, configPulsar, configK8s)
    )
      delete configPulsar[key];
    else {
      logger.info(`pulsarNamespaces: config mismatch for ${key}`);
      return false;
    }
      
  }

  for (const key of Object.keys(configPulsar)){
    if (
      ignoredIfNotSetConfigOptions.includes(key) ||
      ignoredPulsarSettings.includes(key) 
    )
      continue;
    
    if (!await isDefaultValue(key, configPulsar[key])){
      logger.info(`pulsarNamespaces: config mismatch for ${key} and value is not default value`);
      return false;
    }
  }

  return true;
};