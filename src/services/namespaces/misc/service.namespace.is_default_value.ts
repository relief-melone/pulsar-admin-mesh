import { PulsarConfig } from '@/classes/PulsarNamespace';
import deepEqual from 'fast-deep-equal';
import clone from 'clone-deep';

const defaultValues = {
  'auth_policies': {
    'namespace_auth': {},
    'destination_auth': {},
    'subscription_auth_roles': {}
  },
  'replication_clusters': [
    'pulsar'
  ],
  'bundles': {
    'boundaries': [
      '0x00000000',
      '0x40000000',
      '0x80000000',
      '0xc0000000',
      '0xffffffff'
    ],
    'numBundles': 4
  },
  'backlog_quota_map': {},
  'clusterDispatchRate': {},
  'topicDispatchRate': {},
  'subscriptionDispatchRate': {},
  'replicatorDispatchRate': {},
  'clusterSubscribeRate': {},
  'publishMaxMessageRate': {},
  'latency_stats_sample_rate': {},
  'deleted': false,
  'encryption_required': false,
  'subscription_auth_mode': 'None',
  'offload_threshold': -1,
  'schema_compatibility_strategy': 'UNDEFINED',
  'is_allow_auto_update_schema': true,
  'schema_validation_enforced': false,
  'subscription_types_enabled': [],
  'properties': {},
  'offload_policies': {
    'offloadersDirectory': './offloaders',
    'managedLedgerOffloadMaxThreads': 2,
    'managedLedgerOffloadPrefetchRounds': 1,
    'managedLedgerOffloadThresholdInBytes': -1,
    'managedLedgerOffloadedReadPriority': 'TIERED_STORAGE_FIRST',
    's3ManagedLedgerOffloadMaxBlockSizeInBytes': 67108864,
    's3ManagedLedgerOffloadReadBufferSizeInBytes': 1048576,
    's3ManagedLedgerOffloadRoleSessionName': 'pulsar-s3-offload',
    'gcsManagedLedgerOffloadMaxBlockSizeInBytes': 67108864,
    'gcsManagedLedgerOffloadReadBufferSizeInBytes': 1048576,
    's3Driver': false,
    'gcsDriver': false,
    'fileSystemDriver': false
  }
};

export const ignoredDefaultValues = [
  'bundles'
];

export const getDefaultValue = (key: string):any | null =>
  defaultValues[key] !== undefined 
    ?  clone(defaultValues[key])
    : null;

export default async (key: string, value: any ):Promise<boolean> => 
  ignoredDefaultValues.includes(key)
    ? true
    : deepEqual(value, defaultValues[key]); 

