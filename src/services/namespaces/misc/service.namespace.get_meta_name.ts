import objectHash from 'object-hash';

export default (tenant: string, namespace: string): string =>
  `${namespace.toLowerCase()}-${objectHash({ tenant, namespace }).slice(0,5)}`;