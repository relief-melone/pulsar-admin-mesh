import { PulsarNamespaceInK8s, PulsarConfig, PulsarNamespaceInternal, PulsarPermissions } from '@/classes/PulsarNamespace';
import logger from '@/log/logger.default';
import { ResourceMeta } from '@dot-i/k8s-operator';
import getFullName from '../../shared/misc/service.shared.get_full_name';
import PulsarRestClient from '../../shared/service.shared.get_pulsar_rest_client';
import getInternalNamespace from './service.namespace.get_internal_from_pulsar';


export const getService = ( client = PulsarRestClient ) =>
  async (metaNs: ResourceMeta, ns: PulsarNamespaceInK8s):Promise<PulsarNamespaceInternal | null> => {
    try {
      const config = (await client.get<PulsarConfig>(
        `namespaces/${ns.spec.tenant}/${ns.spec.name}`
      )).data;
      const permissions= (await client.get<PulsarPermissions>(
        `namespaces/${ns.spec.tenant}/${ns.spec.name}/permissions`
      )).data;
      
      return getInternalNamespace(metaNs, config, permissions, ns.spec.tenant);
    } catch (err: any) {
      if (err.response?.status === 404)
        return null;
      else {
        logger.error(`${getFullName(metaNs)}: error getting namespace ${metaNs.name} from pulsar api. Error message: ${err.message}`);
        throw err;
      }
    }
  };

export default getService();