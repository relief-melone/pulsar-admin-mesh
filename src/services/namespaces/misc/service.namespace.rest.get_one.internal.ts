import { PulsarNamespaceInK8s, PulsarConfig, PulsarNamespaceInternal, PulsarPermissions } from '@/classes/PulsarNamespace';
import ConfigPulsarNamespace from '@/configs/config.pulsar.namespace';
import ConfigTenant from '@/configs/config.pulsar.tenant';
import logger from '@/log/logger.default';
import logError from '@/services/errors/service.errors.try_to_log_error_message';
import { ResourceMeta } from '@dot-i/k8s-operator';
import getFullName from '../../shared/misc/service.shared.get_full_name';
import PulsarRestClient from '../../shared/service.shared.get_pulsar_rest_client';
import getInternalNamespace from './service.namespace.get_internal_from_pulsar';
import getMetaName from './service.namespace.get_meta_name';


export const getService = ( client = PulsarRestClient, congigNamespace = ConfigPulsarNamespace ) =>
  async (
    tenant: string, 
    nsNameInPulsar: string,
    nsNameMeta: string,
    k8sNamespace: string,
  ):Promise<PulsarNamespaceInternal | null> => {
    try {
      const config = (await client.get<PulsarConfig>(
        `namespaces/${tenant}/${nsNameInPulsar}`
      )).data;
      const permissions = (await client.get<PulsarPermissions>(
        `namespaces/${tenant}/${nsNameInPulsar}/permissions`
      )).data;
      const { kind, group, version } = congigNamespace.main;
      return {
        config,
        permissions,
        k8s: {
          // Missing id and resourceVersion might pose a problem
          // but may not exist at this point
          meta: {
            apiVersion: `${group}/${version}`,
            kind,
            name: nsNameMeta,
            namespace: k8sNamespace,
          } as ResourceMeta,
          namespace: k8sNamespace
        },
        name: nsNameInPulsar,
        tenant
      };
    } catch (err: any) {
      if (err.response?.status === 404)
        return null;
      else {
        logError(err, `could not get namespace ${tenant}/${nsNameInPulsar} from pulsar`);
        throw err;
      }
    }
  };

export default getService();