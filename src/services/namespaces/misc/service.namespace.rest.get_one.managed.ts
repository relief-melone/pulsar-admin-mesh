import ConfigK8s from '@/configs/config.k8s';
import ConfigPulsarNamespace from '@/configs/config.pulsar.namespace';
import logError from '@/services/errors/service.errors.try_to_log_error_message';
import { PulsarNamespaceInK8s } from '@/classes/PulsarNamespace';

export const getService = (configK8s = ConfigK8s, configPulsarNamepsace = ConfigPulsarNamespace) => 
  async (k8sNamespace: string, tenantName: string, namespaceName: string): Promise<PulsarNamespaceInK8s | null> => {
    
    const { apiCustom } = configK8s;
    const { group, version, plural, kind } = configPulsarNamepsace.main;

    try {
      const filteredNamespaces = ((await apiCustom.listNamespacedCustomObject(
        group, version, k8sNamespace, plural
      ) as Record<string,any>)
        .body
        .items as PulsarNamespaceInK8s[])
        .filter( i => 
          i.spec.name === namespaceName &&
          i.spec.tenant === tenantName
        );
      
      if (filteredNamespaces.length === 0){
        return null;
      }
      if (filteredNamespaces.length !== 1){
        const msg = `error getting namespace from k8s cluster. There namespace should be unique for tenant but ${filteredNamespaces.length} were found`;
        throw new Error(msg);
      }

      return filteredNamespaces[0] as PulsarNamespaceInK8s;
    } catch (err: any){
      logError(`[${group}/${version}/${kind}/${tenantName}]: could not get pulsar namespace from k8s cluster`);
      return null;
    }
  };

export default getService();
