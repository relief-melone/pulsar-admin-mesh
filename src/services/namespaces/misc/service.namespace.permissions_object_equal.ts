import { PulsarNamespaceInK8s } from '@/classes/PulsarNamespace';
import deepEqual from 'fast-deep-equal';
import permissionsAreValid from '../validations/service.namespace.permissions_are_valid';

export default (
  permissionsBefore?: PulsarNamespaceInK8s['spec']['permissions'],
  permissionsAfter?: PulsarNamespaceInK8s['spec']['permissions'],
):boolean => {
  if ( permissionsBefore === permissionsAfter )
    return true;

  if ( 
    (deepEqual(permissionsBefore, {}) && permissionsAfter === undefined) ||
    (permissionsBefore === undefined && deepEqual(permissionsAfter, {}))
  )
    return true;

  if (
    (permissionsBefore === undefined && permissionsAfter !== undefined ) ||
    (permissionsBefore !== undefined && permissionsAfter === undefined)
  )
    return false;

  const keys1 = Object.keys(permissionsBefore as object).sort();
  const keys2 = Object.keys(permissionsAfter as object).sort();

  if (!deepEqual(keys1, keys2))
    return false;

  for (const [role1, actions1] of Object.entries(permissionsBefore as Record<string,string[]>)){
    if (
      !deepEqual(actions1.sort(), (permissionsAfter as Record<string,string[]>)[role1].sort())
    )
      return false;
  }

  return true;
};