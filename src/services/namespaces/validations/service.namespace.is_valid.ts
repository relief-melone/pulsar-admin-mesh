import { PulsarNamespaceInK8s } from '@/classes/PulsarNamespace';
import { PulsarTenantInK8s } from '@/classes/PulsarTenant';
import logger from '@/log/logger.default';
import { ResourceMeta } from '@dot-i/k8s-operator';
import getFullName from '../../shared/misc/service.shared.get_full_name';

export const namespaceIsValid = (meta: ResourceMeta, namespace: Partial<PulsarNamespaceInK8s>):boolean => {
  const isValid = !!(namespace.spec?.tenant);

  if (!isValid)
    logger.error(`${getFullName(meta)}: Namespace is not valid`);

  return isValid;
};