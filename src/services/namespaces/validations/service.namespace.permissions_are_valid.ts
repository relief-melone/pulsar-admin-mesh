const allowed = ['produce','consume','functions','sources','sinks','packages'];

export default (input:any):boolean => {
  if (!Array.isArray(input))
    return false;
  
  return !input
    .map(i => allowed.includes(i))
    .some(i => !i);
};