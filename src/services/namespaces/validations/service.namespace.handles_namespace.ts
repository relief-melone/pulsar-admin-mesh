import ConfigNamespace from '@/configs/config.pulsar.namespace';
import ConfigTenant from '@/configs/config.pulsar.tenant';
import logger from '@/log/logger.default';
import resourceNotIncludedInWhitelist from '@/services/shared/validations/service.shared.resource_not_included_in_whitelist';

export const getService = (configTenant = ConfigTenant, configNamespace = ConfigNamespace) =>
  (tenant?: string, namespace?: string): boolean => {
    if (!tenant || !namespace){
      logger.warn('namespace not handled as either namespace or tenant is not defined');
      return false;
    }

    if (resourceNotIncludedInWhitelist(tenant, configTenant)){
      logger.debug(`tenant ${tenant} is not whitelisted. Will not handle namespace ${namespace}`);
      return false;
    }

    if (resourceNotIncludedInWhitelist(namespace, configNamespace)){
      logger.debug(`namespace ${namespace} is not whitelisted. Will not be handled`);
      return false;
    }

    logger.debug(`handeling namespace ${namespace}`);
    return true;
  };

export default getService();