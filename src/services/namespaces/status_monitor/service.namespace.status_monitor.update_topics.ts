import { PulsarNamespaceInK8s } from '@/classes/PulsarNamespace';
import { NamespaceConfig, StatusPatchGetter } from '@/classes/StatusMonitorNew';
import logger from '@/log/logger.default';
import getMeta from '@/services/shared/service.shared.get_resource_meta_from_kube_object_meta';
import getFullName from '../../shared/misc/service.shared.get_full_name';
import collectTopics from '../../topics/pulsar/service.topics.collect_for_namespace';

export const getService = ():StatusPatchGetter<NamespaceConfig, PulsarNamespaceInK8s> =>
  async (_pulsarState, currentK8s) => {
    const meta = getMeta(currentK8s);

    try {
      const topics = await collectTopics(meta, currentK8s);
      if (topics instanceof Error)
        return {};
      
      return {
        topics: {
          count: topics.length
        }
      };
    } catch (err: any) {
      logger.warn(`${getFullName(meta)}: Could not update status for topics`);
      return {};
    }
    
    
  };

export default getService();