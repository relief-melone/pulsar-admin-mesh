import logger from '@/log/logger.default';
import logError from '@/services/errors/service.errors.try_to_log_error_message';
import getFullName from '../../shared/misc/service.shared.get_full_name';
import getNamespace from '@/services/namespaces/misc/service.namespace.rest.get_one';
import { NamespaceConfig, StatusPatchGetter } from '@/classes/StatusMonitorNew';
import { PulsarNamespaceInK8s } from '@/classes/PulsarNamespace';
import getMeta from '@/services/shared/service.shared.get_resource_meta_from_kube_object_meta';
import deepEqual from 'fast-deep-equal';
import PulsarRestClient from '@/services/shared/service.shared.get_pulsar_rest_client';
import permissionsAreEqual from '@/services/namespaces/misc/service.namespace.permissions_object_equal';
import configsMatch from '../misc/service.namespace.pulsar_and_k8s_configs_match';

export const getService = ( client = PulsarRestClient ):StatusPatchGetter<NamespaceConfig, PulsarNamespaceInK8s> => 
  async (pulsarState, currentK8s) => {
    const meta = getMeta(currentK8s);
    const patch: Partial<PulsarNamespaceInK8s['status']> = {};

    const permissionsAreInSync = async () => {
      const { tenant, name: namespace } = currentK8s.spec;

      const permissions = (await client.get<PulsarNamespaceInK8s['spec']['permissions']>(
        `namespaces/${tenant}/${namespace}/permissions`
      )).data;

      return permissionsAreEqual(currentK8s.spec.permissions, permissions);
    };

    try {
      if (!!currentK8s.status.state)
        switch (currentK8s.status.state){
          case 'active':
            break;
          case 'creating':
          case 'updating':
          case 'error':
            if (
              await configsMatch(pulsarState, currentK8s.spec.config) &&
              await permissionsAreInSync()
            ) 
              patch.state = 'active';
            else 
              logger.info(`${getFullName(meta)}: config not in sync with spec. leaving state in ${currentK8s.status.state}`);
            
            break;
            
        }
    } catch (err:any){
      logError(err, `${getFullName(meta)}: error updating state`);
    } finally {
      return patch;
    }
  };


export default getService();