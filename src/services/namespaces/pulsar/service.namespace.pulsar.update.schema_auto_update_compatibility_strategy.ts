import { ConfigValueUpdateType } from '@/classes/Internal';
import { PulsarNamespaceConfigUpdateFunc } from '@/classes/PulsarNamespace';
import getErrorPatch, { ErrorTypeNamespace } from '@/services/errors/service.errors.namespace.get_error_patch';
import logError from '@/services/errors/service.errors.try_to_log_error_message';
import getUpdateType from '@/services/shared/misc/service.shared.get_config_update_type';
import PulsarRestClient from '@/services/shared/service.shared.get_pulsar_rest_client';
import { isInvalidConfiguration } from '../error/service.namespace.error.update';
import logResponse from './service.namespace.pulsar.log_update_response';

export const getService = (client = PulsarRestClient):PulsarNamespaceConfigUpdateFunc =>
  async (nsNow, nsDes) => {
    const { tenant, name: namespace } = nsDes.spec;
    const valNow = nsNow.spec.config?.schema_auto_update_compatibility_strategy || null;
    const valDes = nsDes.spec.config?.schema_auto_update_compatibility_strategy || null;

    const url = `namespaces/${tenant}/${namespace}/schemaAutoUpdateCompatibilityStrategy`;
    const updateType = getUpdateType(valNow, valDes);

    let response: any;

    try {
      switch (updateType){
        case ConfigValueUpdateType.ADDED:
        case ConfigValueUpdateType.CHANGED:
          response = await client.put(url, valDes);
          break;
        case ConfigValueUpdateType.REMOVED:
          response = await client.put(url, '');
          break;
        case ConfigValueUpdateType.UNCHANGED:
          break;
      }
      logResponse(response);
      return [ null, updateType ];
    } catch (err:any) {
      if (isInvalidConfiguration(err))
        return [
          getErrorPatch(ErrorTypeNamespace.InvalidConfiguration, err.response.data.reason),
          updateType
        ];

      logError(err, `${url}: error updating namespace config for schema_auto_update_compatibility_strategy`);
      
      return [
        getErrorPatch(ErrorTypeNamespace.InternalError, 'error updating namespace config for schema_auto_update_compatibility_strategy'),
        updateType
      ];
    }

  };

export default getService();