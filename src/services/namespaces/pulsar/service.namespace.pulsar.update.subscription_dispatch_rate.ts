import { ConfigValueUpdateType } from '@/classes/Internal';
import { PulsarNamespaceConfigUpdateFunc } from '@/classes/PulsarNamespace';
import getErrorPatch, { ErrorTypeNamespace } from '@/services/errors/service.errors.namespace.get_error_patch';
import logError from '@/services/errors/service.errors.try_to_log_error_message';
import getUpdateType from '@/services/shared/misc/service.shared.get_config_update_type';
import PulsarRestClient from '@/services/shared/service.shared.get_pulsar_rest_client';
import { isInvalidConfiguration } from '../error/service.namespace.error.update';
import { getDefaultValue } from '../misc/service.namespace.is_default_value';
import logResponse from './service.namespace.pulsar.log_update_response';

export const getService = ( client = PulsarRestClient):PulsarNamespaceConfigUpdateFunc =>
  async (nsNow, nsDesired) => {
    const { tenant, name: namespace } = nsDesired.spec;
    const valNow = nsNow.spec.config?.subscriptionDispatchRate || getDefaultValue('subscriptionDispatchRate');
    const valDesired = nsDesired.spec.config?.subscriptionDispatchRate || getDefaultValue('subscriptionDispatchRate');

    const url = `namespaces/${tenant}/${namespace}/dispatchRate`;
    let response: any;

    const updateType = getUpdateType(valNow, valDesired);
    try {
      switch (updateType){
        case ConfigValueUpdateType.ADDED:
        case ConfigValueUpdateType.CHANGED:
          response = await client.post(url, valDesired);
          break;
        case ConfigValueUpdateType.REMOVED:
          response = await client.delete(url);
          break;
        case ConfigValueUpdateType.UNCHANGED:
          break;
      }
      logResponse(response);
      return [null, updateType];
    } catch (err: any) {
      if (isInvalidConfiguration(err))
        return [getErrorPatch(ErrorTypeNamespace.InternalError, err.response.data.reason), updateType];
      logError(err, `${url}: error updating config for subscriptionDispatchRate`);
      return [getErrorPatch(ErrorTypeNamespace.InternalError, 'error updating config for subscriptionDispatchRate'), updateType];
    }
  };

export default getService();