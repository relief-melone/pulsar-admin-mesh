import { ConfigValueUpdateType } from '@/classes/Internal';
import { PulsarNamespaceConfigUpdateFunc } from '@/classes/PulsarNamespace';
import getErrorPatch, { ErrorTypeNamespace } from '@/services/errors/service.errors.namespace.get_error_patch';
import logError from '@/services/errors/service.errors.try_to_log_error_message';
import getUpdateType from '@/services/shared/misc/service.shared.get_config_update_type';
import PulsarRestClient from '@/services/shared/service.shared.get_pulsar_rest_client';
import { isInvalidConfiguration } from '../error/service.namespace.error.update';
import logResponse from './service.namespace.pulsar.log_update_response';

export const getService = ( client = PulsarRestClient):PulsarNamespaceConfigUpdateFunc => 
  async (nsNow, nsDesired) => {
    const { tenant, name: namespace } = nsDesired.spec;
    const valNow = typeof nsNow.spec.config?.max_topics_per_namespace === 'number'
      ? nsNow.spec.config.max_topics_per_namespace
      : null;
    const valDesired = typeof nsDesired.spec.config?.max_topics_per_namespace === 'number'
      ? nsDesired.spec.config.max_topics_per_namespace
      : null;

    const url = `namespaces/${tenant}/${namespace}/maxTopicsPerNamespace`;
    let response: any;
    const changeType = getUpdateType(valNow, valDesired);

    try {
      switch (changeType){
        case ConfigValueUpdateType.ADDED:
        case ConfigValueUpdateType.CHANGED:
          response = await client.post(url, valDesired);
          break;
        case ConfigValueUpdateType.REMOVED:
          response = await client.delete(url);
          break;
        case ConfigValueUpdateType.UNCHANGED:
          break;
      }
      logResponse(response);
      return [null, changeType];
      
    } catch (err:any) {
      if (isInvalidConfiguration(err))
        return [
          getErrorPatch(err, err.response.data.reason),
          changeType
        ];
      logError(err, `${url}: error updating namspace config for max_topics_per_namespace`);
      return [
        getErrorPatch(ErrorTypeNamespace.InternalError, 'error updating namspace config for max_topics_per_namespace'),
        changeType
      ];
    }

  };

export default getService();