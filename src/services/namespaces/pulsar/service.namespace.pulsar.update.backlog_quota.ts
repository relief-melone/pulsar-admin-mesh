import { ConfigValueUpdateType } from '@/classes/Internal';
import { PulsarNamespaceConfigUpdateFunc, PulsarNamespaceInK8s } from '@/classes/PulsarNamespace';
import getErrorPatch, { ErrorTypeNamespace } from '@/services/errors/service.errors.namespace.get_error_patch';
import logError from '@/services/errors/service.errors.try_to_log_error_message';
import getConfigUpdateType from '@/services/shared/misc/service.shared.get_config_update_type';
import PulsarRestClient from '@/services/shared/service.shared.get_pulsar_rest_client';
import { isInvalidConfiguration } from '../error/service.namespace.error.update';
import { getDefaultValue } from '../misc/service.namespace.is_default_value';
import logResponse from './service.namespace.pulsar.log_update_response';

export const getService = ( client = PulsarRestClient):PulsarNamespaceConfigUpdateFunc => 
  async ( nsNow, nsDesired ) => {
    const { tenant, name: namespace } = nsDesired.spec;
    const valNow = nsNow.spec.config?.backlog_quota_map || getDefaultValue('backlog_quota_map');
    const valDesired = nsDesired.spec.config?.backlog_quota_map || getDefaultValue('backlog_quota_map');

    const url = `namespaces/${tenant}/${namespace}/backlogQuota`;
    const changeType = getConfigUpdateType(valNow, valDesired);

    if (
      (typeof valDesired?.limit === 'number' && typeof valDesired.limitSize === 'number') &&
      valDesired.limit !== valDesired.limitSize
    )
      return [{
        state: 'error',
        statusMessage: 'invalid configuration: backlog_quota. limit and limitSize are both set but differ'
      }, changeType];
    

    let response: any | undefined;

    try {
      switch (changeType){
        case ConfigValueUpdateType.ADDED:
        case ConfigValueUpdateType.CHANGED:
          response = await client.post(
            url, valDesired, {
              params: {
                // backlogQuotaType: TODO: shoudl be configurable in the future
              }
            }
          );
          break;
        case ConfigValueUpdateType.REMOVED:
          response = await client.delete(url);
          break;
        case ConfigValueUpdateType.UNCHANGED:
          break;
      }
      logResponse(response);
      return [null, changeType];
    } catch (err:any) {
      if (isInvalidConfiguration(err)){
        return [
          getErrorPatch(ErrorTypeNamespace.InvalidConfiguration, err.response.data.reason),
          changeType
        ];
      }
      logError(err, `${url}: error updating config for backlog_quota_map`); 
      return [
        getErrorPatch(ErrorTypeNamespace.InternalError, 'error updating config for backlog_quota_map'),
        changeType];
    }
  };

export default getService();