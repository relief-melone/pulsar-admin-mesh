import logger from '@/log/logger.default';

export default (response?: Record<string,any>) => {
  if (!response)
    return;
  logger.debug(`pulsar namespace update on url ${response?.config?.url || 'N/A'} `);
  logger.debug(`\tmethod:\t${response?.config?.method.toUpperCase() || 'N/A'}`);
  logger.debug(`\tstatus:\t${response?.status || 'N/A'}`);  
  logger.debug(`\tdata:\t${response?.config?.data || 'N/A'}`);
};