import isTenantNotFound from '../../errors/service.errors.is_tenant_not_found';
import logErr from '../../errors/service.errors.try_to_log_error_message';
import pulsarRestClient from '../../shared/service.shared.get_pulsar_rest_client';

export const getService = (client = pulsarRestClient) => 
  async (tenant: string): Promise<Array<string>> => {
    try {
      const namespaces = await client.get(
        `namespaces/${tenant}`
      );
      return namespaces.data;
    } catch (err: any) {
      if (isTenantNotFound(err)){
        return [];
      }

      logErr(err);
      throw err;

    }
    
  };

export default getService();