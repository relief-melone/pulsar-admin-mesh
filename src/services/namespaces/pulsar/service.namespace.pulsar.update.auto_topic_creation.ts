import { ConfigValueUpdateType } from '@/classes/Internal';
import { PulsarNamespaceConfigUpdateFunc, PulsarNamespaceInK8s } from '@/classes/PulsarNamespace';
import getErrorPatch, { ErrorTypeNamespace } from '@/services/errors/service.errors.namespace.get_error_patch';
import logError from '@/services/errors/service.errors.try_to_log_error_message';
import getConfigUpdateType from '@/services/shared/misc/service.shared.get_config_update_type';
import PulsarRestClient from '@/services/shared/service.shared.get_pulsar_rest_client';
import { isInvalidConfiguration } from '../error/service.namespace.error.update';
import logResponse from './service.namespace.pulsar.log_update_response';

export const getService = ( client = PulsarRestClient):PulsarNamespaceConfigUpdateFunc => 
  async (nsNow, nsDesired) => {
    const { tenant, name: namespace } = nsDesired.spec;
    const valNow = nsNow.spec.config?.autoTopicCreationOverride || null;
    const valDesired = nsDesired.spec.config?.autoTopicCreationOverride || null;

    const url = `namespaces/${tenant}/${namespace}/autoTopicCreation`;
    let response:any;
    const changeType = getConfigUpdateType(valNow, valDesired);
    try {
      switch (changeType){
        case ConfigValueUpdateType.ADDED:
        case ConfigValueUpdateType.CHANGED:
          response = await client.post(url, valDesired);
          break;
        case ConfigValueUpdateType.REMOVED:
          response = await client.delete(url);
          break;
        case ConfigValueUpdateType.UNCHANGED:
          break;
      }
      logResponse(response);
      return [null, changeType];
    } catch (err:any) {
      if (isInvalidConfiguration(err)){
        return [
          getErrorPatch(ErrorTypeNamespace.InvalidConfiguration, err.response.data.reason),
          changeType
        ];
      }
      logError(err, `${url}: error updating config for autoTopicCreationOverride`); 
      return [
        getErrorPatch(ErrorTypeNamespace.InternalError, 'error updating config for autoTopicCreationOverride'),
        changeType
      ];
      
    }
  };

export default getService();

