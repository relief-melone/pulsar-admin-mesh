import { ConfigValueUpdateType } from '@/classes/Internal';
import { PulsarNamespaceConfigUpdateFunc } from '@/classes/PulsarNamespace';
import { PulsarTenantInPulsar } from '@/classes/PulsarTenant';
import getErrorPatch, { ErrorTypeNamespace } from '@/services/errors/service.errors.namespace.get_error_patch';
import logError from '@/services/errors/service.errors.try_to_log_error_message';
import getUpdateType from '@/services/shared/misc/service.shared.get_config_update_type';
import PulsarRestClient from '@/services/shared/service.shared.get_pulsar_rest_client';
import { isInvalidConfiguration } from '../error/service.namespace.error.update';
import logResponse from './service.namespace.pulsar.log_update_response';

export const getService = ( client = PulsarRestClient):PulsarNamespaceConfigUpdateFunc => 
  async (nsNow, nsDes) => {
    const { tenant, name: namespace } = nsDes.spec;

    const url = `namespaces/${tenant}/${namespace}/replication`;
    let updateType = ConfigValueUpdateType.UNCHANGED;
    try {

      const valDefault = (await client.get<PulsarTenantInPulsar>(`tenants/${tenant}`)).data.allowedClusters;
      const valNow = nsNow.spec.config?.replication_clusters || valDefault;
      const valDes = nsDes.spec.config?.replication_clusters || valDefault;
      updateType = getUpdateType(valNow, valDes);


      let response:any;
      switch (updateType){
        case ConfigValueUpdateType.ADDED:
        case ConfigValueUpdateType.CHANGED:
          response = await client.post(url, valDes);
          break;
        case ConfigValueUpdateType.REMOVED:
          response = await client.post(url, valDefault);
          break;
        case ConfigValueUpdateType.UNCHANGED:
          break;
      }
      logResponse(response);
      return [
        null,
        updateType
      ];

    } catch (err: any) {
      if (isInvalidConfiguration(err))
        return [
          getErrorPatch(ErrorTypeNamespace.InvalidConfiguration, err.response.data.reason), 
          updateType
        ];
      logError(err, `${url}: error updating namespace config for replication_clusters`);
      return [
        getErrorPatch(ErrorTypeNamespace.InternalError, 'error updating namespace config for replication_clusters'),
        updateType
      ];
    }
  };

export default getService();