import isNamespaceNotPresent from '@/services/errors/service.errors.is_namespace_does_not_exist';
import logError from '@/services/errors/service.errors.try_to_log_error_message';
import PulsarRestClient from '@/services/shared/service.shared.get_pulsar_rest_client';

export const getService = ( client = PulsarRestClient) =>
  async (tenant: string, namespace: string):Promise<void|Error> => {
    const url = `namespaces/${tenant}/${namespace}/autoTopicCreation`;
    try {
      await client.post(url, {
        allowTopicAutoCreation: false
      });
    } catch (err:any) {
      if (isNamespaceNotPresent(err))
        return;
      logError(err);
      return err;
    }
  };

export default getService();