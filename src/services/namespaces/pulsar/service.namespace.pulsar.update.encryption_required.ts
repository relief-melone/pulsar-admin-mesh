import { ConfigValueUpdateType } from '@/classes/Internal';
import { PulsarNamespaceConfigUpdateFunc } from '@/classes/PulsarNamespace';
import getErrorPatch, { ErrorTypeNamespace } from '@/services/errors/service.errors.namespace.get_error_patch';
import logErr from '@/services/errors/service.errors.try_to_log_error_message';
import getUpdateType from '@/services/shared/misc/service.shared.get_config_update_type';
import PulsarRestClient from '@/services/shared/service.shared.get_pulsar_rest_client';
import { isInvalidConfiguration } from '../error/service.namespace.error.update';
import { getDefaultValue } from '../misc/service.namespace.is_default_value';
import logResponse from './service.namespace.pulsar.log_update_response';

export const getService = ( client = PulsarRestClient):PulsarNamespaceConfigUpdateFunc => 
  async (nsNow, nsDesired) => {
    const { tenant, name: namespace } = nsDesired.spec;
    const valNow = typeof nsNow.spec.config?.encryption_required === 'boolean' 
      ?  nsNow.spec.config?.encryption_required
      : getDefaultValue('encryption_required');
    const valDesired = typeof nsDesired.spec.config?.encryption_required === 'boolean'
      ? nsDesired.spec.config?.encryption_required
      : getDefaultValue('encryption_required');

    const url = `namespaces/${tenant}/${namespace}/encryptionRequired`;
    let response: any;
    const changeType = getUpdateType(valNow, valDesired);

    try {
      switch (changeType){
        case ConfigValueUpdateType.ADDED:
        case ConfigValueUpdateType.CHANGED:
          response = await client.post(url, valDesired);
          break;
        case ConfigValueUpdateType.REMOVED:
          response = await client.post(url, false);
          break;
        case ConfigValueUpdateType.UNCHANGED:
          break;
      }
      logResponse(response);
      return [null, changeType];

    } catch (err:any) {
      if (isInvalidConfiguration(err))
        return [
          getErrorPatch(ErrorTypeNamespace.InvalidConfiguration, err.response.data.reason),
          changeType
        ];
      logErr(err, `${url}: error updating namespace config for encryption_required`);
      return [
        getErrorPatch(ErrorTypeNamespace.InternalError, 'error updating namespace config for encryption_required'),
        changeType
      ];
    }
  };

export default getService();
