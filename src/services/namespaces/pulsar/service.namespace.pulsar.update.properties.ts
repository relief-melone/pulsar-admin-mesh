import { PulsarNamespaceConfigUpdateFunc } from '@/classes/PulsarNamespace';
import getErrorPatch, { ErrorTypeNamespace } from '@/services/errors/service.errors.namespace.get_error_patch';
import logError from '@/services/errors/service.errors.try_to_log_error_message';
import PulsarRestClient from '@/services/shared/service.shared.get_pulsar_rest_client';
import { isInvalidConfiguration } from '../error/service.namespace.error.update';
import { ConfigValueUpdateType } from '@/classes/Internal';
import deepEqual from 'fast-deep-equal';
import getUpdateType from '@/services/shared/misc/service.shared.get_config_update_type';
import logResponse from './service.namespace.pulsar.log_update_response';


export const getService = ( client = PulsarRestClient ):PulsarNamespaceConfigUpdateFunc =>
  async (nsNow, nsDes) => {
    const { tenant, name: namespace } = nsDes.spec;    
    const valDes = nsDes.spec.config?.properties || {};
    
    const url = `namespaces/${tenant}/${namespace}/properties`;
    let updateType = ConfigValueUpdateType.UNCHANGED;

    let response:any;

    try {
      const valNow = (await client.get<Record<string,string>>(url)).data;
      updateType = getUpdateType(valNow, valDes);
      switch (updateType){
        case ConfigValueUpdateType.ADDED:
        case ConfigValueUpdateType.CHANGED:
          // TODO: impprove: dont remove all keys first but just update the ones necessary if changed
          await client.delete(url);
          response = await client.put(url, valDes);
          break;
        case ConfigValueUpdateType.REMOVED:
          response = await client.delete(url);
          break;
        case ConfigValueUpdateType.UNCHANGED:
          break;
      }
      logResponse(response);
      return [null, updateType];
    

    } catch (err:any) {
      if (isInvalidConfiguration(err))
        return [
          getErrorPatch( ErrorTypeNamespace.InvalidConfiguration, err.response.data.reason ), 
          updateType
        ];
      
      logError(err,`${url}: error updating namespace config for properties`);
      return [
        getErrorPatch(ErrorTypeNamespace.InternalError),
        updateType
      ];
    }
  };

export default getService();