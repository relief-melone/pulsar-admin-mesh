import { PulsarNamespaceConfigUpdateFunc } from '@/classes/PulsarNamespace';
import PulsarRestClient from '@/services/shared/service.shared.get_pulsar_rest_client';
import deepEqual from 'fast-deep-equal';
import deepmerge from 'deepmerge';
import clone from 'clone-deep';
import logError from '@/services/errors/service.errors.try_to_log_error_message';
import getErrorPatch, { ErrorTypeNamespace } from '@/services/errors/service.errors.namespace.get_error_patch';
import logger from '@/log/logger.default';
import { ConfigValueUpdateType } from '@/classes/Internal';
import permissionsAreValid from '../validations/service.namespace.permissions_are_valid';

export const getService = ( client = PulsarRestClient ):PulsarNamespaceConfigUpdateFunc => 
  async (nsNow, nsDesired) => {
    const { tenant, name: namespace } = nsDesired.spec;
    const baseUrl = `namespaces/${tenant}/${namespace}/permissions`;
    const desiredPermissions = nsDesired.spec.permissions || {};
    const operations:Array<Promise<any>> = [];
    let changeOp = ConfigValueUpdateType.UNCHANGED;

    try {
      const currentPermissions = (await client.get<Record<string,Array<string>>>(baseUrl)).data;
      
      for (const [role, desiredActions] of Object.entries(desiredPermissions)){

        if (!permissionsAreValid(desiredActions))
          return [ 
            getErrorPatch(ErrorTypeNamespace.InvalidConfiguration, 'permissions should be '),
            changeOp
          ];

        const currentActions = currentPermissions[role] as string[] | undefined;
        (desiredActions as string[]).sort();
        currentActions?.sort();

        if (!currentActions || !deepEqual(currentActions, desiredActions)){
          operations.push( client.post(`${baseUrl}/${role}`, desiredActions) );
          changeOp = ConfigValueUpdateType.CHANGED;
        }
        
        delete currentPermissions[role];
      }

      for (const [roleToDelete, _] of Object.entries(currentPermissions)){
        operations.push(
          client.delete(`${baseUrl}/${roleToDelete}`)
        );
        changeOp = ConfigValueUpdateType.CHANGED;
      }

      await Promise.all(operations);
      return [null, changeOp];
    } catch (err) {
      logError(err, `${baseUrl}: error updating permissions for namespace`);
      
      return [
        getErrorPatch(ErrorTypeNamespace.InternalError, 'error updating permissions for namespace'),
        changeOp
      ];
    }
  };

export default getService();