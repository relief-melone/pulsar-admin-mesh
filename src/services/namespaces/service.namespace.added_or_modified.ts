import OperatorPulsarNamespace from '@/classes/Operator.PulsarNamespace';
import logger from '@/log/logger.default';
import { ResourceEvent } from '@dot-i/k8s-operator';
import getFullName from '../shared/misc/service.shared.get_full_name';
import getResource from '../shared/k8s/service.shared.k8s.get_resource.from_meta';
import { getPatchStatus, StatusPatcher } from '../shared/k8s/service.shared.k8s.get_status_patcher';
import createOrPatch from './service.operator.namespace.create_or_patch';
import deleteNamespace from './service.operator.namespace.delete';
import getNamespaceFromResourceEvent from './misc/service.namespace.get_from_resource_event';
import getTenant from '@/services/tenant/misc/service.tenant.managed.get_one';
import { PulsarNamespaceInK8s } from '@/classes/PulsarNamespace';
import handleTenantNotPresent from './error/service.namespace.error.tenant_not_present';
import handleTenantInErrorState from './error/service.namespace.error.tenant_in_error_state';
import isInDeletionStage from '../shared/k8s/service.shared.k8s.object_is_in_deletion_stage';
import logErr from '../errors/service.errors.try_to_log_error_message';
import updateIsNecessary from './conditions/service.namespace.update_is_necessary';

export const addedOrModified = async function(
  this:OperatorPulsarNamespace, 
  e: ResourceEvent,
){
  const namespace = getNamespaceFromResourceEvent(e);
  const patchStatus = getPatchStatus.bind(this)(e.meta) as StatusPatcher<PulsarNamespaceInK8s>;

  if (!isInDeletionStage(e)){
    
    if (!await updateIsNecessary(e.meta, getNamespaceFromResourceEvent(e)))
      return;

    // CHECK IF NAMESPACE HAS A CORRESPODING TENANT AND TENANT IS IN HEALTHY STATE
    const tenant = await getTenant(e.meta, namespace.spec.tenant);    
    if (!tenant)
      return await handleTenantNotPresent(patchStatus);
    
    if (tenant.status.state === 'error')
      return await handleTenantInErrorState(patchStatus);
    
  
    const k8sResource = await getResource(e.meta, this.config.main.plural);
    if (!k8sResource){
      logger.info(`${getFullName(e.meta)}: Namespace has already been deleted from cluster. nothing to do...`);
      return;
    }
  }
  
  let finalizerFinishedRunning = false;
  try {
    await this.handleResourceFinalizer(
      e,
      'delete-child-resources',
      async evt => {
        try {
          // this.smStore.terminate(evt);
          const ns = getNamespaceFromResourceEvent(evt);
          await deleteNamespace(evt.meta, ns, patchStatus);
          finalizerFinishedRunning = true;
        } catch (err: any) {
          logger.error(`${getFullName(e.meta)}: error deleting child resources for namespace`);
        }
      }
    );
    if (!finalizerFinishedRunning){
      await createOrPatch(e, patchStatus);
    }

  } catch (err: any){
    logErr(err, `${getFullName(e.meta)}: An error occured executing addedOrModified for Operator pulsar namespace`);
  }
};