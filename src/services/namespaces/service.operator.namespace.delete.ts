import { PulsarNamespaceInK8s } from '@/classes/PulsarNamespace';
import logger from '@/log/logger.default';
import { ResourceMeta } from '@dot-i/k8s-operator';
import getFullName from '../shared/misc/service.shared.get_full_name';
import PulsarRestClient from '../shared/service.shared.get_pulsar_rest_client';
import { StatusPatcher } from '@/services/shared/k8s/service.shared.k8s.get_status_patcher';
import collectTopicsInPulsar from '../topics/pulsar/service.topics.collect_for_namespace';
import deleteTopic from '../topics/pulsar/service.topics.delete_one';
import deleteManagedTopicsForNamespace from '../topics/k8s/service.topic.k8s.delete_topics_for_namespace';
import getInternalNamespace from './misc/service.namespace.get_internal';
import { getNamespace } from '../shared/service.shared.get_namespace';
import logError from '../errors/service.errors.try_to_log_error_message';
import disableAutoTopicCreation from './pulsar/service.namespace.pulsar.delete.prevent_auto_topic_creation';
import deleteRemainingTopicsOnPulsar from '../topics/pulsar/service.topic.delete_for_namespace';

export const getService = (client = PulsarRestClient) => 
  async (
    nsMeta: ResourceMeta, 
    ns: PulsarNamespaceInK8s,
    patchStatus: StatusPatcher<PulsarNamespaceInK8s>
  ):Promise<void> => {
    await patchStatus({
      state: 'terminating'
    });
    try {
      const pulsarTenant = ns.spec.tenant;
      const pulsarNamespace = ns.spec.name;

      logger.info(`${getFullName(nsMeta)}: deleting namespace`);
      
      logger.info(`${getFullName(nsMeta)}: turning off autoTopicCreation to prevent recreation of topics by producers/consumers`);      
      await disableAutoTopicCreation(pulsarTenant, pulsarNamespace);
      logger.info(`${getFullName(nsMeta)}: autoTopicCreation successfully disabled`);
      
      const nsInternal = await getInternalNamespace(
        ns.spec.tenant,
        ns.spec.name,
        getNamespace(nsMeta)
      );
      if (!nsInternal){
        logger.warn(`${getFullName(nsMeta)}: no namespace could be found. aborting...`);
        return;
      }
        
      await deleteManagedTopicsForNamespace(nsInternal);
      await deleteRemainingTopicsOnPulsar(nsMeta, ns);

      await client.delete(
        `namespaces/${ns.spec.tenant}/${pulsarNamespace}`
      );

      logger.info(`${getFullName(nsMeta)}: namespace successfully deleted`);
    } catch (err:any){
      const msg =`${getFullName(nsMeta)}: could not delete namespace`;
      logError(err, msg);      
      await patchStatus({
        state: 'error'
      });
    }
    
  };


export default getService();