import { PulsarNamespaceInK8s } from '@/classes/PulsarNamespace';
import logError from '@/services/errors/service.errors.try_to_log_error_message';
import getPrevious from '@/services/namespaces/misc/service.namespace.get_previously_applied';
import { ResourceMeta } from '@dot-i/k8s-operator';
import getFullName from '../../shared/misc/service.shared.get_full_name';
import permissionsAreEqual from '../misc/service.namespace.permissions_object_equal';
import k8sAndPulsarConfigsMatch from '../misc/service.namespace.pulsar_and_k8s_configs_match';
import logger from '@/log/logger.default';

export const getService = () =>
  async (
    metaNs: ResourceMeta, 
    ns: PulsarNamespaceInK8s
  ):Promise<boolean> => {
    
    const beforeChange = await getPrevious(ns);
    try {
      switch (ns.status.state){
        case 'creating':
          return true;
        case 'terminating':
          return false;
      }

      if (
        (
          beforeChange?.status.state === 'error' && 
          (ns.status.state === 'updating' || ns.status.state === 'error')
        ) 
        && ns.status.lastSpecOnError === JSON.stringify(ns.spec)){
        logger.warn(`${getFullName(metaNs)}: apply failed before with current spec. will not update`);
        return false;
      }
      const configsMatch = await k8sAndPulsarConfigsMatch(beforeChange?.spec.config, ns.spec.config);
      const permissionsMatch = permissionsAreEqual(beforeChange?.spec.permissions, ns.spec.permissions);
      return ( !configsMatch || !permissionsMatch);

    } catch (err) {
      const msg = `${getFullName(metaNs)}: could not check if update is necessary`;
      logError(err, msg);
      throw new Error(msg);
    }
  };

export default getService();