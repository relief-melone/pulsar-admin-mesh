import { PulsarNamespaceInK8s } from '@/classes/PulsarNamespace';
import { ResourceMeta } from '@dot-i/k8s-operator';
import ConfigTenant from '@/configs/config.pulsar.tenant';
import ConfigK8s from '@/configs/config.k8s';
import { getNamespace } from '../../shared/service.shared.get_namespace';
import { PulsarTenantInK8s } from '@/classes/PulsarTenant';
import logger from '@/log/logger.default';
import getFullName from '../../shared/misc/service.shared.get_full_name';
import configPulsarNamespace from '@/configs/config.pulsar.namespace';
import awaitRetryAttempt from '@/services/shared/misc/service.shared.await_retry_attempt';

export const getService = (configK8s = ConfigK8s, configTenant = ConfigTenant, configNamespace = configPulsarNamespace) => 
  async (
    metaNamespace: ResourceMeta, 
    pulsarNamespace: PulsarNamespaceInK8s,
    retryAttempt = 0
  ):Promise<boolean> => {
    const { apiCustom } = configK8s;
    const kns = getNamespace(metaNamespace);
    try {
      const tenantInK8s = (await apiCustom.getNamespacedCustomObject(
        configTenant.main.group,
        configTenant.main.version,
        kns,
        configTenant.main.plural,
        pulsarNamespace.spec.tenant
      )).body as PulsarTenantInK8s;
  
      if (tenantInK8s){
        switch (tenantInK8s.status.state){
          case 'active':
            logger.debug(`${getFullName(metaNamespace)}: Corresponding tenant [${pulsarNamespace.spec.tenant}] is already active.`);
            return true;

          case 'creating':
          case 'updating':
            if (retryAttempt < configNamespace.probing.readiness.maxAttempts){
              await awaitRetryAttempt(
                retryAttempt,
                configNamespace.probing.readiness,
                `${getFullName(metaNamespace)}: Corrensponding tenant still in state ${tenantInK8s.status.state}`
              );
              return await getService(configK8s, configTenant, configNamespace)(metaNamespace, pulsarNamespace, retryAttempt+1);
            } else {
              logger.error(`${getFullName(metaNamespace)}: Corresponding tenant still in state ${tenantInK8s.status.state} after max retry attempts. Giving up`);
              return false;
            }

          case 'terminating':
          case 'error':

            logger.error(`${getFullName(metaNamespace)}: Corresponding tenant is in state ${tenantInK8s.status.state}.`);
            return false;
        }
      } else {
        logger.error(`${getFullName(metaNamespace)}: No correspoding tenant with name ${pulsarNamespace.spec.tenant} exists in k8s cluster.`);
        return false;
      }
    } catch (err:any) {
      if (err.statusCode === 404){
        logger.error(`${getFullName(metaNamespace)}: No correspoding tenant with name ${pulsarNamespace.spec.tenant} exists in k8s cluster.`);
      } else {
        logger.error(`${getFullName(metaNamespace)}: An unkown error occured getting the tenant ${pulsarNamespace.spec.tenant} from k8s cluster.`);
      }
      return false;
    }
    
  };

export default getService();