import { PulsarNamespaceInK8s, PulsarNamespaceInternal } from '@/classes/PulsarNamespace';
import { PulsarTenantInternal } from '@/classes/PulsarTenant';
import  ConfigK8s from '@/configs/config.k8s';
import configPulsarNamespace from '@/configs/config.pulsar.namespace';
import getMetaFromCustomResourceDefinition from '@/services/shared/service.shared.get_resource_meta_from_custom_resource_definition';
import getMetaFromKubeObject from '@/services/shared/service.shared.get_resource_meta_from_kube_object_meta';
import { ResourceMeta } from '@dot-i/k8s-operator';
import { V1CustomResourceDefinition, V1CustomResourceDefinitionList } from '@kubernetes/client-node';
import getNamespaceInternal from '../misc/service.namespace.get_internal_from_pulsar';
import getNsPulsar from '../misc/service.namespace.rest.get_one';
import getPulsarNamespace from '../misc/service.namespace.rest.get_one.internal';


export const getService = (configK8s = ConfigK8s, configNs = configPulsarNamespace) =>
  async (tenant: PulsarTenantInternal):Promise<PulsarNamespaceInternal[]> => {
    const k8s = tenant.k8s;

    if (!k8s)
      throw new Error('cannot get k8s namespace for tenant as no k8s info was present in internal tenant');

    const { apiCustom } = configK8s;
    const pnsInNamespace = await apiCustom.listNamespacedCustomObject(
      configNs.main.group,
      configNs.main.version,
      k8s.namespace,
      configNs.main.plural
    );
    
        
    const items = (pnsInNamespace.body as V1CustomResourceDefinitionList).items as any as V1CustomResourceDefinition[];
    const calls = items
      .map(async pns => {
        // FIXME: possibly broken if pns does not contain spec
        const namespaceNameInPulsar = (pns.spec as any).name;
        const namespaceNameMeta = pns.metadata?.name;
        if (!namespaceNameInPulsar || !namespaceNameMeta)
          return null;
        
        const nsInPulsar = await getPulsarNamespace(
          tenant.name, 
          namespaceNameInPulsar, 
          namespaceNameMeta,
          k8s.namespace
        );
        return nsInPulsar || null;
      });


    const namespaces = (await Promise.all(calls)).filter(ns => ns !== null);

    return namespaces as PulsarNamespaceInternal[];
  };

export default getService();
