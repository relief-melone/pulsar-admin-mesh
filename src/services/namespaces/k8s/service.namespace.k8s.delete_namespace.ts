import { PulsarNamespaceInternal } from '@/classes/PulsarNamespace';
import ConfigK8s from '@/configs/config.k8s';
import configPulsarNamespace from '@/configs/config.pulsar.namespace';
import logger from '@/log/logger.default';
import logError from '@/services/errors/service.errors.try_to_log_error_message';
import awaitResourceDeletion from '@/services/shared/k8s/service.shared.await_custom_resource_deletion';

export const getService = ( configNs = configPulsarNamespace, configK8s = ConfigK8s) => 
  async (namespace: PulsarNamespaceInternal):Promise<void> => {
    const { apiCustom } = configK8s;
    try {
      const { group, plural, version } = configNs.main;
      if (!namespace.k8s)
        throw new Error('cannot delete namespace from k8s as k8s info is not present in internal namespace');
      if (!namespace.k8s.meta?.name)
        throw new Error('cannot delete namespace from k8s as k8s meta.name is not present in internal namespace');

      await apiCustom.deleteNamespacedCustomObject(
        group,
        version,
        namespace.k8s.namespace,
        plural,
        namespace.k8s.meta.name
      );

      await awaitResourceDeletion(
        namespace.k8s.meta, 
        configNs.main.plural, 
        configNs.probing.deletion
      );

      return;
    } catch (err:any) {
      logError(err);
      throw err;
    }
    
  };

export default getService();