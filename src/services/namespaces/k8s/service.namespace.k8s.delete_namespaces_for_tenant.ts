import { PulsarTenantInternal } from '@/classes/PulsarTenant';
import logger from '@/log/logger.default';
import getFullName from '@/services/shared/misc/service.shared.get_full_name';
import deletePulsarNamespace from './service.namespace.k8s.delete_namespace';
import getPulsarNamespaces from './service.namespace.k8s.get_for_tenant';

export const getService = () =>
  async (tenant: PulsarTenantInternal):Promise<void> => {
    const k8s = tenant.k8s;

    if (!k8s){
      throw new Error('cannot delete k8s namespace as not k8s info present in internal tenant');
    }
    logger.info(`${getFullName(k8s.meta)}: deleting all namespaces for tenant`);
    const k8sPns = await getPulsarNamespaces(tenant);
    
    const deletionOperations = k8sPns
      .map(pns => deletePulsarNamespace(pns));

    await Promise.all(deletionOperations);

    logger.info(`${getFullName(k8s.meta)}: all namespaces for tenant have been deleted`);
  };

export default getService();