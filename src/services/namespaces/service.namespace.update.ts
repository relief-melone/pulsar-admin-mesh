import { PulsarNamespaceConfigUpdateFunc, PulsarNamespaceInK8s } from '@/classes/PulsarNamespace';
import logger from '@/log/logger.default';
import { ResourceMeta } from '@dot-i/k8s-operator';
import wait from '@root/test/helpers/wait';
import getFullName from '../shared/misc/service.shared.get_full_name';
import PulsarRestClient from '../shared/service.shared.get_pulsar_rest_client';
import { StatusPatcher } from '../shared/k8s/service.shared.k8s.get_status_patcher';
import autoSubscriptionCreation from './pulsar/service.namespace.pulsar.update.auto_subscription_creation';
import getErrorPatch from '../errors/service.errors.namespace.get_error_patch';
import { ErrorTypeNamespace } from '../errors/service.errors.namespace.get_error_patch';
import autoTopicCreation from './pulsar/service.namespace.pulsar.update.auto_topic_creation';
import { StatusPatch } from '@/classes/CustomResourceShared';
import backLogQuota from './pulsar/service.namespace.pulsar.update.backlog_quota';
import compactionThreshold from './pulsar/service.namespace.pulsar.update.compaction_threshold';
import deduplication from './pulsar/service.namespace.pulsar.update.deduplication';
import deduplicationSnapshotInterval from './pulsar/service.namespace.pulsar.update.deduplication_snapshot_interval';
import delayedDelivery from './pulsar/service.namespace.pulsar.update.delayed_delivery';
import subscriptionDispatchRate from './pulsar/service.namespace.pulsar.update.subscription_dispatch_rate';
import encryptionRequired from './pulsar/service.namespace.pulsar.update.encryption_required';
import entryFilters from './pulsar/service.namespace.pulsar.update.entry_filters';
import inactiveTopicPolicies from './pulsar/service.namespace.pulsar.update.inactive_topic_policies';
import isAllowAutoUpdateSchema from './pulsar/service.namespace.pulsar.update.is_allow_auto_update_schema';
import maxConsumersPerSubscription from './pulsar/service.namespace.pulsar.update.max_consumers_per_subscription';
import getBefore from './misc/service.namespace.get_previously_applied';
import maxConsumersPerTopic from './pulsar/service.namespace.pulsar.update.max_consumers_per_topic';
import maxProducersPerTopic from './pulsar/service.namespace.pulsar.update.max_producers_per_topic';
import maxSubsriptionsPerTopic from './pulsar/service.namespace.pulsar.update.max_subscriptions_per_topic';
import maxTopicsPerNamespace from './pulsar/service.namespace.pulsar.update.max_topics_per_namespace';
import maxUnackedMessagesPerConsumer from './pulsar/service.namespace.pulsar.update.max_unacked_messages_per_consumer';
import maxUnackedMessagesPerSubscription from './pulsar/service.namespace.pulsar.update.max_unacked_messages_per_subscription';
import messageTTLInSeconds from './pulsar/service.namespace.pulsar.update.message_ttl_in_seconds';
import offloadDeletionLag from './pulsar/service.namespace.pulsar.update.offload_deletion_lag_ms';
import offloadPolicies from './pulsar/service.namespace.pulsar.update.offload_policies';
import offloadThreshold from './pulsar/service.namespace.pulsar.update.offload_threshold';
import { ConfigValueUpdateType } from '@/classes/Internal';
import permissions from './pulsar/service.namespace.pulsar.update.permissions';
import persistenceSettings from './pulsar/service.namespace.pulsar.update.persistence_settings';
import properties from './pulsar/service.namespace.pulsar.update.properties';
import replication from './pulsar/service.namespace.pulsar.update.replication';
import replicatorDispatchRate from './pulsar/service.namespace.pulsar.update.replicator_dispatch_rate';
import resourceGroupName from './pulsar/service.namespace.pulsar.update.resource_group_name';
import retentionPolicies from './pulsar/service.namespace.pulsar.update.retention_policies';
import schemaAutoUpdateCompatibilityStrategy from './pulsar/service.namespace.pulsar.update.schema_auto_update_compatibility_strategy';
import schemaCompatibilityStrategy from './pulsar/service.namespace.pulsar.update.schema_compatibility_strategy';
import schemaValidationEnforced from './pulsar/service.namespace.pulsar.update.schema_validation_enforced';
import subscriptionAuthMode from './pulsar/service.namespace.pulsar.update.subscription_auth_mode';
import subscriptionExpirationTimeMinutes from './pulsar/service.namespace.pulsar.update.subscription_expiration_time_minutes';
import subscriptionTypesEnabled from './pulsar/service.namespace.pulsar.update.subscription_types_enabled';
// import offLoadThresholdInSeconds from './pulsar/service.namespace.pulsar.update.offload_threshold_in_seconds';

export const getService = (client=PulsarRestClient) => 
  async (
    meta: ResourceMeta, 
    namespace: PulsarNamespaceInK8s, 
    ps: StatusPatcher<PulsarNamespaceInK8s>
  ):Promise<void> => {
    await ps({
      state: 'updating'
    });

    const nsNow = await getBefore(namespace);

    if (!nsNow){
      logger.warn(`${getFullName(meta)}: could not update namespace as no previous configuration could be found`);
      await ps(getErrorPatch(ErrorTypeNamespace.UpdateFailed));
      return;
    }

    const updateOperations:Record<string,PulsarNamespaceConfigUpdateFunc> = {
      autoSubscriptionCreation,
      autoTopicCreation,
      backLogQuota,
      compactionThreshold,
      deduplication,
      deduplicationSnapshotInterval,
      delayedDelivery,
      subscriptionDispatchRate,
      encryptionRequired,
      entryFilters, // ENTRY FILTERS STILL HAS ISSUES,
      inactiveTopicPolicies,
      isAllowAutoUpdateSchema,
      maxConsumersPerSubscription,
      maxConsumersPerTopic,
      maxProducersPerTopic,
      maxSubsriptionsPerTopic,
      maxTopicsPerNamespace,
      maxUnackedMessagesPerConsumer,
      maxUnackedMessagesPerSubscription,
      messageTTLInSeconds,
      offloadDeletionLag,
      offloadPolicies, // STILL UNTESTED DELETE MIGHT NOT WORK AS IT IS NOT DOCUMENTED IN REST API
      offloadThreshold,
      permissions,
      persistenceSettings,
      properties,
      replication,
      replicatorDispatchRate,
      resourceGroupName,
      retentionPolicies,
      schemaAutoUpdateCompatibilityStrategy,
      schemaCompatibilityStrategy,
      schemaValidationEnforced,
      subscriptionAuthMode,
      subscriptionExpirationTimeMinutes,
      subscriptionTypesEnabled

      // offLoadThresholdInSeconds FIXME: returns 405 at the moment
      
    
    };

    // const operationResults:Array<[StatusPatch | null, ConfigValueUpdateType]> = [];
    const operationResults = {
      changed: 0,
      added: 0,
      removed: 0,
      error: 0,
      ops: [] as string[]
    };

    const erroredOps:Array<[StatusPatch | null, ConfigValueUpdateType]> = [];

    for (const [name, op] of Object.entries(updateOperations)){
      const result = await op(nsNow, namespace);
      if (result[0]){
        operationResults.error++;
        erroredOps.push(result);
        continue;
      } else if (result[1] !== ConfigValueUpdateType.UNCHANGED){
        operationResults.ops.push(name);
      }
      switch (result[1]){
        case ConfigValueUpdateType.ADDED:
          operationResults.added++;
          break;

        case ConfigValueUpdateType.CHANGED:
          operationResults.changed++;
          break;

        case ConfigValueUpdateType.REMOVED:
          operationResults.removed++;
          break;

        default:
          break;
      }
      
    }
    
    
    const firstOperationWithError = erroredOps[0]?.[0];
    

    if (firstOperationWithError){
      await ps(firstOperationWithError);
      return;
    }
    // 
    // TODO: dont upate on status updates
    logger.info(`${getFullName(meta)}: update complete`);
    logger.info(`\tsettings added:  \t${operationResults.added}`);
    logger.info(`\tsettings changed:\t${operationResults.changed}`);
    logger.info(`\tsettings removed:\t${operationResults.removed}`);
    logger.info(`\tsettings errored:\t${erroredOps.length}`);
    logger.info(`\tconfigs treated: \t${operationResults.ops.join(', ')}`);
    
    // DONE: autoSubscriptionCreation (override settings or inherit from broker 2 methods)
    // DONE: autoTopicCreation (override settings or inherit from broker)
    // DONE: backlog quota
    // DONE: compaction threshold (uncompacted bytes in topic)
    // DONE: deduplication
    // DONE: deduplication snapshot interval
    // DONE: delayed delivery policies
    // DONE: dispatch rate (clusterDispatchRate)
    // DONE: encryption required
    // DONE: entry filters
    // DONE: inactive topic policies
    // DONE: is_auto_update_schema
    // DONE: set max consumers per subscription
    // DONE: set max consumers per topic
    // DONE: set max producer per topic
    // DONE: max subscriptions per topic
    // DONE: max unacked messages
    // DONE: ttl messages
    // DONE: offload deletion lag ms
    // DONE: offload policies
    // DONE: offload threshold
    // DONE: offload threshold in seconds (not implemented at the moment as it seems toi have issues)
    // DONE: permissions
    // DONE: persistence settings
    // TODO: bookie affinity?? not found in schema. Missing?
    // DONE: properties (remove needs to happen explicitly. works as patching)
    // DONE: replication clusters
    // DONE: replicator dispatch rate
    // DONE: resource group
    // DONE: retention policies
    // DONE: schema auto update compatibilty strategy
    // DONE: schema compatibility strategy
    // DONE: schema validation enforced
    // DONE: subscribeRate (not subscriptionDispatchRate???)
    // DONE: subscription auth mode
    // DONE: subscription expiration time
    // DONE: subscriptionDispatchRate
    // DONE: subscriptionExpirationTime
    // DONE: subscriptionTypes


    await ps({
      state: 'active',
      statusMessage: 'OK'
    });
  };

export default getService();
