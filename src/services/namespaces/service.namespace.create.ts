import { PulsarNamespaceInK8s } from '@/classes/PulsarNamespace';
import logger from '@/log/logger.default';
import { ResourceMeta } from '@dot-i/k8s-operator';
import getFullName from '../shared/misc/service.shared.get_full_name';
import PulsarRestClient from '../shared/service.shared.get_pulsar_rest_client';
import { StatusPatcher } from '../shared/k8s/service.shared.k8s.get_status_patcher';
import getTenant from '@/services/tenant/misc/service.tenant.managed.get_one';

export const getService = (client = PulsarRestClient) => 
  async (
    meta: ResourceMeta,
    namespace: PulsarNamespaceInK8s,
    patchStatus: StatusPatcher<PulsarNamespaceInK8s>
  ):Promise<void> => {
    logger.info(`${getFullName(meta)}: Creating new namespace`);
    const tenant = await getTenant(meta, namespace.spec.tenant);
    
    if (!tenant || !tenant.status.state){
      const msg = `${getFullName(meta)}: no tenant found in this namespace`;
      logger.error(msg);
      await patchStatus({
        state: 'error',
        statusMessage: `${namespace.spec.tenant} not present in this namespace`
      });
    }

    try {
      await patchStatus({ state:  'creating' });
      logger.debug(`${getFullName(meta)}: creating new namespace in pulsar...`);
      await client.put(
        `/namespaces/${namespace.spec.tenant}/${namespace.spec.name}`,{
          ...namespace.spec.config
        }
      );
      logger.debug(`${getFullName(meta)}: namespace successfully created. Patching status to active...`);
      await patchStatus({
        state: 'active'
      });
      logger.info(`${getFullName(meta)}: namespace creation completed succesfully`);
    } catch (err:any) {
      logger.error(`${getFullName(meta)}: could not create namespace`);
      await patchStatus({
        state: 'error',
        statusMessage: err.message || 'error on namespace creation'
      });
    }
  };

export default getService();