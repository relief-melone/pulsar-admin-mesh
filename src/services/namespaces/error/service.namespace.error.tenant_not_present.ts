import { PulsarNamespaceInK8s } from '@/classes/PulsarNamespace';
import logger from '@/log/logger.default';
import logError from '@/services/errors/service.errors.try_to_log_error_message';
import { StatusPatcher } from '@/services/shared/k8s/service.shared.k8s.get_status_patcher';

export default async (patchStatus: StatusPatcher<PulsarNamespaceInK8s>):Promise<void> =>  {
  try {
    await patchStatus({
      state: 'error',
      statusMessage: 'corresponding tenant not found'
    });
  } catch (err:any) {
    logError('could not patch error status for tenant not present in namespace');
  }
};