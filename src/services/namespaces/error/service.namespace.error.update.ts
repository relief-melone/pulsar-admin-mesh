export const isInvalidConfiguration = (err: any): boolean => 
  err.name === 'AxiosError' &&
  err.response?.status === 412 &&
  err.response?.statusText?.toLowerCase().includes('precondition failed') &&
  err.response?.data?.reason;

