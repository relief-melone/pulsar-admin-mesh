import { PulsarNamespaceInK8s } from '@/classes/PulsarNamespace';
import logger from '@/log/logger.default';
import { StatusPatcher } from '../../shared/k8s/service.shared.k8s.get_status_patcher';

export default async (patchStatus: StatusPatcher<PulsarNamespaceInK8s>):Promise<void> =>  {
  try {
    await patchStatus({
      state: 'error',
      statusMessage: 'corresponding tenant is in error state'
    });
  } catch (err:any) {
    logger.error('could not patch error status for tenant in error state in namespace');
  }
};