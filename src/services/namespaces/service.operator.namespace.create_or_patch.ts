import { PulsarNamespaceInK8s } from '@/classes/PulsarNamespace';
import { ResourceEvent } from '@dot-i/k8s-operator';
import { StatusPatcher } from '@/services/shared/k8s/service.shared.k8s.get_status_patcher';
import getNamespaceFromResourceEvent from './misc/service.namespace.get_from_resource_event';
import getCurrent from './misc/service.namespace.rest.get_one';
import getFullName from '../shared/misc/service.shared.get_full_name';
import logger from '@/log/logger.default';
import createNamespace from './service.namespace.create';
import isTenantReady from './conditions/service.namespace.await_tenant_ready';
import updateNamespace from './service.namespace.update';
import updateIsNecessary from './conditions/service.namespace.update_is_necessary';
import logError from '../errors/service.errors.try_to_log_error_message';
import getCurrentNsInPulsar from './misc/service.namespace.rest.get_one.internal';
import { getNamespace } from '../shared/service.shared.get_namespace';
import setLastApplied from '../shared/k8s/service.shared.k8s.set_last_applied_config';
import configPulsarNamespace from '@/configs/config.pulsar.namespace';


export const getService = (configNamespace = configPulsarNamespace) =>
  async (e: ResourceEvent, sp: StatusPatcher<PulsarNamespaceInK8s>) => {
    const meta = e.meta;
    const pulsarNamespaceInK8s = getNamespaceFromResourceEvent(e);
    const k8sNamespace= getNamespace(meta);
    const tenantActive = await isTenantReady(e.meta, pulsarNamespaceInK8s);
    
    if (!tenantActive)
      return;

    const namespacePulsar = await getCurrentNsInPulsar(
      pulsarNamespaceInK8s.spec.tenant, 
      pulsarNamespaceInK8s.spec.name,
      e.meta.name,
      k8sNamespace
    );

    try {
      if (namespacePulsar){
        logger.debug(`${getFullName(meta)}: namespace already exists. patchting...`);
        await updateNamespace(meta, pulsarNamespaceInK8s, sp);

      } else {
        logger.debug(`${getFullName(e.meta)}: Namespace does not yet exist. Creating new one`);
        await createNamespace(meta, pulsarNamespaceInK8s, sp);
      }
      await setLastApplied(pulsarNamespaceInK8s, configNamespace);

    } catch (err) {
      logError(err, `${getFullName(meta)}: error in create_or_patch for namespace`);
    }
    
  };

export default getService();