import { PulsarTopicInK8s, PulsarTopicStats } from '@/classes/PulsarTopic';
import { StatusPatchGetter } from '@/classes/StatusMonitorNew';
import logError from '@/services/errors/service.errors.try_to_log_error_message';
import getCurrentResource from '@/services/shared/k8s/service.shared.k8s.get_custom_resource_from_meta';
import getMeta from '@/services/shared/service.shared.get_resource_meta_from_kube_object_meta';
import getTopicStats from '@/services/topics/pulsar/service.topics.get_stats';
import deepEqual from 'fast-deep-equal';
import getProducersFromStats from '../misc/service.topics.get_producers_from_stats';
import getSubscriptionsFromStats from '../misc/service.topics.get_subscriptions_from_stats';

export const getService = ():StatusPatchGetter<PulsarTopicStats, PulsarTopicInK8s> => 
  async (pulsarState, itemInK8s) => {
    try {
      const status = itemInK8s.status;
      const meta = getMeta(itemInK8s);
      const patch:Partial<PulsarTopicInK8s['status']> = {};

      const subs = getSubscriptionsFromStats(pulsarState);
      const prods = getProducersFromStats(pulsarState);

      if (!deepEqual(subs, status.subscriptions))
        patch.subscriptions = subs;

      if (!deepEqual(prods, status.producers))
        patch.producers = prods;
      
      if ( status.state !== 'active'){
        
        // TODO: make sure all relevant info is compared
        patch.state = 'active';
      }

      return patch;
      
    } catch (err) {
      logError(err);
      return {};
    }
  };

export default getService();