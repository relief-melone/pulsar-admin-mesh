import { PulsarNamespaceInternal } from '@/classes/PulsarNamespace';
import { PulsarTopicInK8s, PulsarTopicInternal } from '@/classes/PulsarTopic';
import ConfigK8s from '@/configs/config.k8s';
import ConfigPulsarTopic from '@/configs/config.pulsar.topic';
import k8sToInternal from '../misc/service.topic.k8s_topic_to_internal_topic';


export const getService = (configK8s = ConfigK8s, configTopics = ConfigPulsarTopic) =>
  async (ns: PulsarNamespaceInternal):Promise<PulsarTopicInternal[]> => {
    const k8s = ns.k8s;

    if (!k8s)
      throw new Error('cannot get k8s topics as no k8s info present in internal namespace');

    const { apiCustom } = configK8s;
    const { group, version, plural } = configTopics.main;

    const topics = (await apiCustom.listNamespacedCustomObject(
      group, version, k8s.namespace, plural
    ) as any).body?.items as PulsarTopicInK8s[] || undefined;

    if (!topics)
      throw new Error('cannot get topics for namespace. no topics returned from api');
    
    return topics.map(
      t => k8sToInternal(t)
    );
  };

export default getService();