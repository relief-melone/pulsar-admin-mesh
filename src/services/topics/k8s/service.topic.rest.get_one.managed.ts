import { PulsarTopicInK8s } from '@/classes/PulsarTopic';
import configPulsarTopic from '@/configs/config.pulsar.topic';
import logError from '@/services/errors/service.errors.try_to_log_error_message';
import getCustomResource from '@/services/shared/k8s/service.shared.k8s.get_resource';

export const getService = (configTopic = configPulsarTopic) =>
  async (topicName: string, k8sNamepsace: string): Promise<PulsarTopicInK8s | null> => {
    const { group, version, plural } = configTopic.main;
    
    try {
      return await getCustomResource<PulsarTopicInK8s>(
        group, version, k8sNamepsace, plural, topicName
      );
      
    } catch (err:any) {
      logError(err, `could not get topic ${topicName} in namespace ${k8sNamepsace}`);
      throw err;
    }
  };
  
export default getService();