import { PulsarNamespaceInternal } from '@/classes/PulsarNamespace';
import logger from '@/log/logger.default';
import getFullName from '@/services/shared/misc/service.shared.get_full_name';
import deleteManagedTopic from './service.topic.k8s.delete_one.managed';
import getTopicsForNamespace from './service.topic.k8s.get_topics_for_namespace';

export const getService = () => 
  async (namespace: PulsarNamespaceInternal):Promise<void> => {
    
    const k8s = namespace.k8s;

    if (!k8s)
      throw new Error('cannot delete k8s topics as no k8s info present in internal namepsace');
    
    logger.info(`${getFullName(k8s.meta)}: deleting all managed topics for namespace ${namespace.name}`);
    const topics = await getTopicsForNamespace(namespace);

    const operations = topics.map(
      t => {
        const metaName = t.k8s?.meta?.name;
        if (!metaName)
          throw new Error(`${getFullName(k8s.meta)}: cannot delete k8s topic as meta did not include name`);
        return deleteManagedTopic(t);
      }
    );

    await Promise.all(operations);
    logger.info(`${getFullName(k8s.meta)}: Successfully deleted all managed topics`);

  };

export default getService();