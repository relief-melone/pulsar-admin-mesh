import { PulsarTopicInK8s, PulsarTopicInternal } from '@/classes/PulsarTopic';
import ConfigK8s from '@/configs/config.k8s';
import configPulsarTopic from '@/configs/config.pulsar.topic';
import logError from '@/services/errors/service.errors.try_to_log_error_message';
import getCustomResource from '@/services/shared/k8s/service.shared.k8s.get_resource';
import awaitResourceDeletion from '@/services/shared/k8s/service.shared.await_custom_resource_deletion';
import { ResourceMeta } from '@dot-i/k8s-operator';
import logger from '@/log/logger.default';
import getFullName from '@/services/shared/misc/service.shared.get_full_name';
import isTopicNotFound from '@/services/errors/service.errors.is_topic_not_found';

export const getService = (
  configTopic = configPulsarTopic,
  configK8s = ConfigK8s
) =>
  async (topic: PulsarTopicInternal): Promise<void> => {


    const { group, version, plural } = configTopic.main;
    const { apiCustom } = configK8s;
    const k8s = topic.k8s;
    
    if (!k8s)
      throw new Error('cannot delete managed topic as no k8s info present in internal topic');
    if (!k8s.meta?.name)
      throw new Error('cannot delete managed topic as no k8s meta.name present');

    try {
      await apiCustom.deleteNamespacedCustomObject(
        group, version, k8s.namespace, plural, k8s.meta.name
      );

      await awaitResourceDeletion(
        k8s.meta,
        configTopic.main.plural,
        configTopic.probing.deletion
      );
      
    } catch (err:any) {
      if (isTopicNotFound(err)){
        logger.info(`${getFullName(k8s.meta)}: topic was already deleted`);
        return;
      }
      logError(err, `${getFullName(k8s.meta)}: could not delete topic ${topic.name} in namespace ${topic.namespace}`);
      throw err;
    }
  };
  
export default getService();