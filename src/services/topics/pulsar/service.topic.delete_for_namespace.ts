import { PulsarNamespaceInK8s, PulsarNamespaceInternal } from '@/classes/PulsarNamespace';
import PulsarRestClient from '@/services/shared/service.shared.get_pulsar_rest_client';
import collectTopicsForNamespace from './service.topics.collect_for_namespace';
import { ResourceMeta } from '@dot-i/k8s-operator';
import { getTopicTypePrefix } from '@/services/shared/pulsar/service.shared.get_topic_type_prefix';
import logger from '@/log/logger.default';
import serviceTopicGet_full_name from '../misc/service.topic.get_full_name';
import deleteTopic from './service.topics.delete_one';
import logError from '@/services/errors/service.errors.try_to_log_error_message';
import getFullName from '@/services/shared/misc/service.shared.get_full_name';

export const getService = () =>
  async (nsMeta: ResourceMeta, ns: PulsarNamespaceInK8s):Promise<void|Error> => {
    try {
      const topics = await collectTopicsForNamespace(nsMeta, ns);
      if (topics instanceof Error){
        const msg = `${getFullName(nsMeta)} could not delete topics for namespace. error collecting topics`;        
        return new Error(msg);
      }

      await Promise.all(
        topics
          .map(topic => deleteTopic(topic))
      );
      
    } catch (err) {
      const msg = `${getFullName(nsMeta)} could not delete topics for namespace`;
      logError(err);
      return new Error(msg);
    }
  };

export default getService();