import { PulsarTopicInK8s, PulsarTopicInternal } from '@/classes/PulsarTopic';
import isTopicNotFound from '@/services/errors/service.errors.is_topic_not_found';
import logError from '@/services/errors/service.errors.try_to_log_error_message';
import pulsarRestClient from '@/services/shared/service.shared.get_pulsar_rest_client';

export const getService = (client = pulsarRestClient) =>
  async (topicK8s:PulsarTopicInK8s):Promise<boolean> => {
    try {
      const { tenant, namespace, name: topic } = topicK8s.spec;
      const topicType = topicK8s.spec.persistent
        ? 'persistent'
        : 'non-persistent';
      
      const urlSuffix = '/partitions';
      const url = `${topicType}/${tenant}/${namespace}/${topic}${urlSuffix}`;
      await client.get(url);
      return true;
    } catch (err:any){
      if (isTopicNotFound(err))
        return false;
      else {
        logError(err);
        throw err;
      }
    }
  };
export default getService();