import { PulsarNamespaceInK8s } from '@/classes/PulsarNamespace';
import { PulsarPartition, PulsarTopicInternal } from '@/classes/PulsarTopic';
import logger from '@/log/logger.default';
import { ResourceMeta } from '@dot-i/k8s-operator';
import getFullName from '../../shared/misc/service.shared.get_full_name';
import PulsarRestClient from '../../shared/service.shared.get_pulsar_rest_client';
import getPartitionsFromStats from '../misc/service.topics.get_partitions_from_stats';
import getSubsFromStats from '../misc/service.topics.get_subscriptions_from_stats';
import errorIsNsAlreadyDeleted from '../../errors/service.errors.is_namespace_is_deleted';
import errorIsNsDoesNotExist from '../../errors/service.errors.is_namespace_does_not_exist';
import logError from '../../errors/service.errors.try_to_log_error_message';
import getProducersFromStats from '../misc/service.topics.get_producers_from_stats';


export const getService = (client = PulsarRestClient) =>
  async (nsMeta: ResourceMeta, ns: PulsarNamespaceInK8s):Promise<Array<PulsarTopicInternal>> => {
    try {
      const pulsarNamespace = ns.spec.name;
      const result = await client.get(
        `persistent/${ns.spec.tenant}/${pulsarNamespace}/partitioned`
      );
      const topicNames = result.data as string[];
      const topics:Array<PulsarTopicInternal> = [];
      const calls = topicNames.map(async (tn):Promise<PulsarTopicInternal> => {
        const topicName = tn.split('/').pop();
        if (!topicName)
          throw new Error(`${getFullName(nsMeta)}: could not get topic name`);
        const stats = (await client.get(
          `persistent/${ns.spec.tenant}/${pulsarNamespace}/${topicName}/partitioned-stats`
        )).data;

        const partitions = getPartitionsFromStats(stats);
        const subscriptions = getSubsFromStats(stats);
        const producers = getProducersFromStats(stats);
        
        return {
          name: topicName,
          namespace: pulsarNamespace,
          partitioned: true,
          persistent: true,
          tenant: ns.spec.tenant,
          partitions: getPartitionsFromStats(stats).count,
          status: {
            partitions,
            raw: stats,
            state: 'active',
            storageSize: stats.storageSize,
            subscriptions,
            producers
          }
          
        };
      });
      const data = await Promise.all(calls);
      return data;
    } catch (err) {
      if (errorIsNsAlreadyDeleted(err)){
        logger.warn(`${getFullName(nsMeta)}: namespace was already deleted`);
        return [];
      }
      if (errorIsNsDoesNotExist(err)){
        logger.warn(`${getFullName(nsMeta)}: namespace does not exist`);
        return [];
      }
      const msg = `${getFullName(nsMeta)}: Could not get partitioned topics`;
      logError(err, msg);
      throw err;
    }
  };

export default getService();