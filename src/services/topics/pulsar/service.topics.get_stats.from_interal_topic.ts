import { PulsarTopicInternal, PulsarTopicStats } from '@/classes/PulsarTopic';
import PulsarRestClient from '../../shared/service.shared.get_pulsar_rest_client';
import getStats from './service.topics.get_stats';


export const getService = () => 
  async (topic: PulsarTopicInternal):Promise<PulsarTopicStats> => {
    return await getStats(
      topic.tenant,
      topic.namespace,
      topic.name,
      topic.persistent,
      topic.partitioned
    );
  };

export default getService();