import { PulsarNamespaceInK8s } from '@/classes/PulsarNamespace';
import { PulsarTopicInternal, PulsarTopicStats } from '@/classes/PulsarTopic';
import logger from '@/log/logger.default';
import { ResourceMeta } from '@dot-i/k8s-operator';
import errorIsNsAlreadyDeleted from '../../errors/service.errors.is_namespace_is_deleted';
import errorIsNsDoesNotExist from '../../errors/service.errors.is_namespace_does_not_exist';
import logError from '../../errors/service.errors.try_to_log_error_message';
import getFullName from '../../shared/misc/service.shared.get_full_name';
import PulsarRestClient from '../../shared/service.shared.get_pulsar_rest_client';
import getPartitionsFromStats from '../misc/service.topics.get_partitions_from_stats';
import getSubsFromStats from '../misc/service.topics.get_subscriptions_from_stats';
import getProducersFromStats from '../misc/service.topics.get_producers_from_stats';

export const getService = (client = PulsarRestClient) =>
  async (
    nsMeta: ResourceMeta, 
    ns: PulsarNamespaceInK8s,
    alreadyCollectedTopics: Array<PulsarTopicInternal> = []
  ):Promise<Array<PulsarTopicInternal>> => {

    const pulsarNamespace = ns.spec.name;
    const partitionNames = alreadyCollectedTopics
      .filter(t => t.persistent && t.partitioned)
      .flatMap(t => 
        t.status.partitions.items.map(p => p.name) || []
      );

    try {
      const topicCalls = (
        await client.get<Array<string>>(`persistent/${ns.spec.tenant}/${pulsarNamespace}`)
      )
        .data
        .filter(s => !partitionNames.includes(s))
        .map(async (tn):Promise<PulsarTopicInternal> => {
          const topicName = tn.split('/').pop();
          if (!topicName)
            throw new Error(`${getFullName(nsMeta)}: could not get topic name`);

          const stats = (await client.get<PulsarTopicStats>(
            `/persistent/${ns.spec.tenant}/${pulsarNamespace}/${topicName}/stats`
          )).data;

          const partitions = getPartitionsFromStats(stats);
          const subscriptions = getSubsFromStats(stats);
          const producers = getProducersFromStats(stats);
          
          return {
            name: topicName,
            namespace: pulsarNamespace,
            partitioned: false,
            persistent: true,
            tenant: ns.spec.tenant,
            partitions: getPartitionsFromStats(stats).count,
            status: {
              partitions,
              raw: stats,
              state: 'active',
              storageSize: stats.storageSize,
              subscriptions,
              producers
            }
          };
        });
      const topics = await Promise.all(topicCalls);
      
      return topics;
    } catch (err:any) {
      if (errorIsNsAlreadyDeleted(err)){
        logger.warn(`${getFullName(nsMeta)}: namespace was already deleted`);
        return [];
      }
      if (errorIsNsDoesNotExist(err)){
        logger.warn(`${getFullName(nsMeta)}: namespace does not exist`);
        return [];
      }
      const msg = `${getFullName(nsMeta)}: Could not get partitioned topics`;
      logError(err, msg);
      throw err;
      
    }

    
  };

export default getService();