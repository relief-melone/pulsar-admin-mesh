import { PulsarNamespaceInK8s } from '@/classes/PulsarNamespace';
import { PulsarTopicInternal, PulsarTopicStats } from '@/classes/PulsarTopic';
import logger from '@/log/logger.default';
import { ResourceMeta } from '@dot-i/k8s-operator';
import getFullName from '../../shared/misc/service.shared.get_full_name';
import PulsarRestClient from '../../shared/service.shared.get_pulsar_rest_client';
import getPartitionsFromStats from '../misc/service.topics.get_partitions_from_stats';
import getSubsFromStats from '../misc/service.topics.get_subscriptions_from_stats';
import errorIsNsAlreadyDeleted from '../../errors/service.errors.is_namespace_is_deleted';
import errorIsNsDoesNotExist from '../../errors/service.errors.is_namespace_does_not_exist';
import getProducersFromStats from '../misc/service.topics.get_producers_from_stats';

export const getService = ( client = PulsarRestClient) => 
  async (
    nsMeta: ResourceMeta, 
    ns: PulsarNamespaceInK8s,
    alreadyCollectedTopics: Array<PulsarTopicInternal> = []
  ):Promise<Array<PulsarTopicInternal>> => {
    try {
      const pulsarNamespace = ns.spec.name;
      const partitionNames = alreadyCollectedTopics
        .filter(t => !t.persistent && t.partitioned)
        .flatMap(t => 
          t.status.partitions.items?.map(p => p.name) || []
        );


      const topicCalls = (
        await client.get<Array<string>>(`non-persistent/${ns.spec.tenant}/${pulsarNamespace}`)
      )
        .data
        .filter( s => !partitionNames.includes(s))
        .map(async tn => {
          const topicName = tn.split('/').pop();
          const stats = (
            await client.get<PulsarTopicStats>(`non-persistent/${ns.spec.tenant}/${pulsarNamespace}/${topicName}/stats`)
          ).data;

          const partitions = getPartitionsFromStats(stats);
          const subscriptions = getSubsFromStats(stats);
          const producers = getProducersFromStats(stats);

          return {
            name: topicName,
            namespace: ns.spec.name,
            partitioned: false,
            persistent: false,
            tenant: ns.spec.tenant,
            partitions: getPartitionsFromStats(stats).count,
            status: {
              partitions,
              raw: stats,
              state: 'active',
              storageSize: stats.storageSize,
              subscriptions,
              producers
            }
          } as PulsarTopicInternal;
        });

      return await Promise.all(topicCalls);

    } catch (err: any) {
      if (errorIsNsAlreadyDeleted(err)){
        logger.warn(`${getFullName(nsMeta)}: namespace was already deleted`);
        return [];
      }
      if (errorIsNsDoesNotExist(err)){
        logger.warn(`${getFullName(nsMeta)}: namespace does not exist`);
        return [];
      }
      const msg = `${getFullName(nsMeta)}: Error getting non persistent, non partitioned topics`;
      logger.error(msg);
      throw err;
    }
  };

export default getService();