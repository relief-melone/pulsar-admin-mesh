import { PulsarNamespaceInK8s } from '@/classes/PulsarNamespace';
import { PulsarTopicInternal } from '@/classes/PulsarTopic';
import logger from '@/log/logger.default';
import { ResourceMeta } from '@dot-i/k8s-operator';
import logError from '../../errors/service.errors.try_to_log_error_message';
import getFullName from '../../shared/misc/service.shared.get_full_name';
import getNonPersistentNonPartitionedTopics from './service.topics.not_persistent.get_non_partitioned';
import getNonPersistedPartitionedTopics from './service.topics.not_persistent.get_partitioned';
import getPersistentNonPartitionedTopics from './service.topics.persistent.get_non_partitioned';
import getPersistentPartitionedTopics from './service.topics.persistent.get_partitioned';

export const getService = () =>  
  async (nsMeta: ResourceMeta, ns: PulsarNamespaceInK8s):Promise<Array<PulsarTopicInternal> | Error> => {
    try {
      const persistentPartitionedTopics = await getPersistentPartitionedTopics(nsMeta, ns);
      const persistentNonPartitionedTopics = await getPersistentNonPartitionedTopics(nsMeta, ns, persistentPartitionedTopics);
    
      const nonPersistentPartitionedTopics = await getNonPersistedPartitionedTopics(nsMeta, ns);
      const nonPersistentNonPartitionedTopics = await getNonPersistentNonPartitionedTopics(nsMeta, ns, nonPersistentPartitionedTopics);
      return [
        ...persistentPartitionedTopics,
        ...persistentNonPartitionedTopics,
        ...nonPersistentPartitionedTopics,
        ...nonPersistentNonPartitionedTopics
      ];
    } catch (err:any) {
      // FIXME: internal topics have not been generated yet should not throw
      const msg = `${getFullName(nsMeta)}: Could not collect topics for namespace`;
      logError(err, msg);
      return new Error(msg);
    }
  };

export default getService();