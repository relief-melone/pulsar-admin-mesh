import { PulsarTopicInternal, PulsarTopicStats } from '@/classes/PulsarTopic';
import PulsarRestClient from '../../shared/service.shared.get_pulsar_rest_client';

export const getService = (client = PulsarRestClient) => 
  async (
    tenant: string,
    namespace: string,
    topicName: string,
    persistent: boolean,
    partitioned: boolean    
  ):Promise<PulsarTopicStats> => {
    const topicType = persistent
      ? 'persistent'
      : 'non-persistent';
    
    const statsSuffix = partitioned
      ? 'partitioned-stats'  
      : 'stats';
      

    const stats = await client.get<PulsarTopicStats>(
      `${topicType}/${tenant}/${namespace}/${topicName}/${statsSuffix}`
    );

    return stats.data;
  };

export default getService();