import { PulsarTopicInK8s, PulsarTopicInternal } from '@/classes/PulsarTopic';
import logger from '@/log/logger.default';
import { errorPresentsReason, getReason } from '@/services/errors/service.errors.error_with_reason';
import logError from '@/services/errors/service.errors.try_to_log_error_message';
import { isInvalidConfiguration } from '@/services/namespaces/error/service.namespace.error.update';
import { StatusPatcher } from '@/services/shared/k8s/service.shared.k8s.get_status_patcher';
import getFullName from '@/services/shared/misc/service.shared.get_full_name';
import PulsarRestClient from '@/services/shared/service.shared.get_pulsar_rest_client';
import { ResourceMeta } from '@dot-i/k8s-operator';
import { AxiosError } from 'axios';


export const getService = (client = PulsarRestClient) => 
  async (
    metaTopic: ResourceMeta, 
    topic: PulsarTopicInK8s,
    patchStatus: StatusPatcher<PulsarTopicInK8s>
  ):Promise<void> => {
    const { 
      persistent, 
      partitioned, 
      tenant, 
      namespace, 
      name,
      partitions
    } = topic.spec;

    const topicType = persistent
      ? 'persistent'
      : 'non-persistent';
    
    const urlSuffix = partitioned
      ? '/partitions'
      : '';

    const url = `${topicType}/${tenant}/${namespace}/${name}${urlSuffix}`;
    try {
      await patchStatus({ state: 'creating' });

      logger.info(`${getFullName(metaTopic)}: creating new topic in pulsar...`);
      if (partitioned)
        await client.put(url, partitions);
      else
        await client.put(url);

      logger.info(`${getFullName(metaTopic)}: successfully created new topic in pulsar`);
      await patchStatus({ state: 'active' });
    } catch (err){      
      if (errorPresentsReason(err))
        await patchStatus({ state: 'error', statusMessage: getReason(err) });
      else {
        logError(err);
        throw err;
      }
      
    }
  };

export default getService();