import { ConfigValueUpdateType } from '@/classes/Internal';
import { PulsarTopicUpdateFunc } from '@/classes/PulsarTopic';
import logger from '@/log/logger.default';
import getErrorPatch from '@/services/errors/service.errors.topic.get_error_patch';
import { ErrorTypeTopic } from '@/services/errors/service.errors.topic.get_error_patch';
import logError from '@/services/errors/service.errors.try_to_log_error_message';
import logResponse from '@/services/namespaces/pulsar/service.namespace.pulsar.log_update_response';
import getUpdateType from '@/services/shared/misc/service.shared.get_config_update_type';
import getFullName from '@/services/shared/misc/service.shared.get_full_name';
import PulsarRestClient from '@/services/shared/service.shared.get_pulsar_rest_client';
import getMeta from '@/services/shared/service.shared.get_resource_meta_from_kube_object_meta';

export const getService = ( client = PulsarRestClient):PulsarTopicUpdateFunc =>
  async (topicNow, topicDes) => {
    const { tenant, namespace, name: topic } = topicDes.spec;
    const valNow = topicNow.spec.partitions;
    const valDes = topicDes.spec.partitions;
    
    const updateType = getUpdateType(valNow, valDes);
    const topicPrefix = topicNow.spec.persistent 
      ? 'persistent'
      : 'non-persistent';

    if ( updateType !== ConfigValueUpdateType.UNCHANGED && !topicNow.spec.partitioned ){
      logger.info(`${getFullName(getMeta(topicNow))}: Ignoring value change for spec.partitions as topic is not partitioned`);
      return [ null, ConfigValueUpdateType.UNCHANGED ];
    }

    if (valDes < valNow){
      return [
        getErrorPatch(ErrorTypeTopic.InvalidConfiguration, 'partitions can only be increased'),
        updateType
      ];
    }
    let response: any;

    try {
      switch (updateType){
        case ConfigValueUpdateType.ADDED:
        case ConfigValueUpdateType.CHANGED:
          response = await client.post(`${topicPrefix}/${tenant}/${namespace}/${topic}/partitions`, valDes);
          break;
        case ConfigValueUpdateType.REMOVED:
        case ConfigValueUpdateType.UNCHANGED:
          break;
      }
      logResponse(response);
      return [ null, updateType ];
    } catch (err:any) {
      logError(err);
      return [
        getErrorPatch(ErrorTypeTopic.InternalError, 'error updating topic config for partitions'),
        updateType
      ];
    }
  };

export default getService();