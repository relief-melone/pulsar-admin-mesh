import { PulsarTopicInternal } from '@/classes/PulsarTopic';
import logger from '@/log/logger.default';
import isTopicNotFound from '@/services/errors/service.errors.is_topic_not_found';
import logErr from '@/services/errors/service.errors.try_to_log_error_message';
import { getTopicTypePrefixForInternalTopic as getPrefix } from '@/services/shared/pulsar/service.shared.get_topic_type_prefix';
import pulsarRestClient from '@/services/shared/service.shared.get_pulsar_rest_client';
import getFullName from '../misc/service.topic.get_full_name';

export const getService = (client = pulsarRestClient) =>
  async (topic: PulsarTopicInternal):Promise<void> => {
    try {
      const suffix = topic.partitioned ? '/partitions' : '';
      const url = `${getPrefix(topic)}/${topic.tenant}/${topic.namespace}/${topic.name}${suffix}`;
      await client.delete(url, {
        params: {
          force: true
        }
      });

    } catch (err) {
      if (isTopicNotFound(err)){
        logger.debug(`${getFullName(topic)}: topic was already deleted`);
        return;
      }
      logErr(err);
    }
  };

export default getService();