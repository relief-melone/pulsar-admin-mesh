import { PulsarTopicInternal } from '@/classes/PulsarTopic';
import logger from '@/log/logger.default';
import isTopicNotFound from '@/services/errors/service.errors.is_topic_not_found';
import logError from '@/services/errors/service.errors.try_to_log_error_message';
import PulsarRestClient from '../../shared/service.shared.get_pulsar_rest_client';
import deleteSubscriptionsForTopic from '../../subscriptions/service.subscription.delete_for_topic';
import getFullName from '../misc/service.topic.get_full_name';

export const getService = (client = PulsarRestClient) =>
  async (topic: PulsarTopicInternal) => {
    try {
      logger.info(`${getFullName(topic)}: deleting topic...`);

      // Deletion of subscriptions should not be necessary if the topic gets force deleted
      // await deleteSubscriptionsForTopic(topic);

      const topicType = topic.persistent
        ? 'persistent'
        : 'non-persistent';

      const urlSuffix = topic.partitioned
        ? '/partitions'
        : '';

      const url = `${topicType}/${topic.tenant}/${topic.namespace}/${topic.name}${urlSuffix}`;
      await client.delete(
        url,
        {
          params: {
            force: true
          }
        }
      );
      logger.info(`${getFullName(topic)}: deletion of topic complete`);

    } catch (err: any) {
      if (isTopicNotFound(err)){
        return logger.info(`${getFullName(topic)}: topic was already deleted`);
      }
      const msg = `${getFullName(topic)}: error deleting topic`;
      logError(err, msg);
    }
  };

export default getService();