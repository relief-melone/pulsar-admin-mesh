import { PulsarTopicInternal } from '@/classes/PulsarTopic';
import configPulsarTopic from '@/configs/config.pulsar.topic';

export const getService = () =>
  (topic: PulsarTopicInternal):string => 
    `[PulsarTopic pns=${topic.namespace} name: ${topic.name}]`;

export default getService();