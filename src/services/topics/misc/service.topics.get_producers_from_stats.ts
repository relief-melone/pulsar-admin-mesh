import { Pulsartopic } from '@/classes/crd.pulsar_topic';
import { PulsarSubscription, PulsarTopicStats } from '@/classes/PulsarTopic';

export default (stats: PulsarTopicStats): Pulsartopic['status']['producers'] => {
  const items = Object
    .entries(stats.publishers)
    .map(([ key, _value ]) => ({
      name: key
    } as PulsarSubscription));

  return {
    count: items.length,
  };
};
  