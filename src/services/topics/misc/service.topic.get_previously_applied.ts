import { PulsarTopicInK8s } from '@/classes/PulsarTopic';
import isTopicNotFound from '@/services/errors/service.errors.is_topic_not_found';
import logError from '@/services/errors/service.errors.try_to_log_error_message';
import getResource from '@/services/shared/k8s/service.shared.k8s.get_custom_resource_from_meta';
import PulsarRestClient from '@/services/shared/service.shared.get_pulsar_rest_client';
import getMeta from '@/services/shared/service.shared.get_resource_meta_from_kube_object_meta';
import getStats from '../pulsar/service.topics.get_stats';

export const getService = ( client = PulsarRestClient ) => 
  async (tp: PulsarTopicInK8s):Promise<PulsarTopicInK8s | null> => {
    const lastApplied = await getResource<PulsarTopicInK8s>(getMeta(tp));
    if (!lastApplied)
      return null;
    
    const { tenant, namespace, name: topic, partitioned, persistent } = tp.spec;

    try {
      const stats = await getStats(tenant, namespace, topic, persistent, partitioned);

      if (tp.spec.partitioned && typeof stats.metadata.partitions === 'number')
        lastApplied.spec.partitions = stats.metadata.partitions;
      return lastApplied;

    } catch (err: any){
      if (isTopicNotFound(err))
        return null;
      logError(err);
      throw err;
    }
  };

export default getService();
