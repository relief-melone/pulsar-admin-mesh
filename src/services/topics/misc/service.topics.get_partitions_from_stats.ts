import { Pulsartopic } from '@/classes/crd.pulsar_topic';
import { PulsarPartition, PulsarTopicStats } from '@/classes/PulsarTopic';

export default (stats: PulsarTopicStats):Pulsartopic['status']['partitions'] => {
  
  const items = stats.partitions
    ? Object.entries<any>(stats.partitions)
      .map(([key, value]) => ({
        name: key,
        storageSize: value.storageSize,
        details: value
      } as PulsarPartition))
      .sort((a,b) => a.name.localeCompare(b.name))
    : [];

  return {
    count: items.length,
    items
  };
};