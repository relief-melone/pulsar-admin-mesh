import objectHash from 'object-hash';

export default (tenant: string, namespace: string, topic: string, persitent: boolean, partitioned: boolean):string => 
  `${topic.toLowerCase()}-${objectHash({ tenant, namespace, topic, persitent, partitioned }).slice(0,8)}`;