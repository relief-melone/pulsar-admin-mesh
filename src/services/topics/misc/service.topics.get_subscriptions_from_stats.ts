import { Pulsartopic } from '@/classes/crd.pulsar_topic';
import { PulsarSubscription, PulsarTopicStats } from '@/classes/PulsarTopic';

export default (stats: PulsarTopicStats): Pulsartopic['status']['subscriptions'] => {
  const items = Object
    .entries(stats.subscriptions)
    .map(([ key, _value ]) => ({
      name: key
    } as PulsarSubscription));

  return {
    count: items.length,
    items
  };
};
  