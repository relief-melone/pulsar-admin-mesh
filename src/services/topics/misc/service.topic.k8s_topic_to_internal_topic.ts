import { PulsarTopicInternal, PulsarTopicInK8s, PulsarPartition, PulsarSubscription } from '@/classes/PulsarTopic';
import { getNamespace } from '@/services/shared/service.shared.get_namespace';
import getMetaFromKubeObject from '@/services/shared/service.shared.get_resource_meta_from_kube_object_meta';
import { ResourceMeta } from '@dot-i/k8s-operator';

export default (
  input: PulsarTopicInK8s,
):PulsarTopicInternal => {

  const topicInternal:PulsarTopicInternal = {
    name: input.spec.name,
    tenant: input.spec.tenant,
    namespace: input.spec.namespace,
    partitions: input.spec.partitions,
    partitioned: input.spec.partitioned,
    persistent: input.spec.persistent,
    status: input.status,
    k8sMetaName: input.metadata?.name
  };

  if (input.metadata)
    topicInternal.k8s = {
      meta: getMetaFromKubeObject(input),
      namespace: getNamespace(input.metadata)
    };


  return topicInternal;
};