import { PulsarTopicInK8s } from '@/classes/PulsarTopic';
import { ResourceEvent } from '@dot-i/k8s-operator';

export default (ev: ResourceEvent):PulsarTopicInK8s => {
  return ev.object as PulsarTopicInK8s;
};