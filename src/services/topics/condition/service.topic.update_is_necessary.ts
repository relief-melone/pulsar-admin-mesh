import { PulsarTopicInK8s, PulsarTopicInternal } from '@/classes/PulsarTopic';
import logger from '@/log/logger.default';
import logError from '@/services/errors/service.errors.try_to_log_error_message';
import getFullName from '@/services/shared/misc/service.shared.get_full_name';
import { ResourceMeta } from '@dot-i/k8s-operator';
import getPrevious from '../misc/service.topic.get_previously_applied';
import deepEqual from 'fast-deep-equal';

export const getService = () =>
  async (
    metaTopic: ResourceMeta, 
    pulsarTopic: PulsarTopicInK8s
  ):Promise<boolean> => {
    try {
      // TODO: check if anything that requires patching has changed
      const beforeChange = await getPrevious(pulsarTopic);

      switch (pulsarTopic.status.state){
        case 'creating':
          return true;
        case 'terminating':
          return false;
      }

      if (
        (
          beforeChange?.status.state === 'error' && 
          ['error', 'updating'].includes(pulsarTopic.status.state)
        ) 
        && pulsarTopic.status.lastSpecOnError === JSON.stringify(pulsarTopic.spec)
      ){
        logger.warn(`${getFullName(metaTopic)}: apply failed before with current spec. will not update`);
        return false;
      }

      logger.debug('Patching topics is not yet fully supported');

      return (
        !deepEqual(pulsarTopic.spec.config, beforeChange?.spec.config || {}) ||
        pulsarTopic.spec.partitions !== beforeChange?.spec.partitions
      );
      
    } catch (err:any) {
      const msg = pulsarTopic
        ? `${getFullName(metaTopic)}: could not check if update is necessary`
        : 'could not check if update is necessary';
      
      logError(err, msg);
      throw new Error(msg);
    }
  };

export default getService();