import { PulsarNamespaceInK8s } from '@/classes/PulsarNamespace';
import { PulsarTopicInK8s } from '@/classes/PulsarTopic';
import ConfigK8s from '@/configs/config.k8s';
import configPulsarNamespace from '@/configs/config.pulsar.namespace';
import configPulsarTopic from '@/configs/config.pulsar.topic';
import logger from '@/log/logger.default';
import logError from '@/services/errors/service.errors.try_to_log_error_message';
import getFullName from '@/services/shared/misc/service.shared.get_full_name';
import { getNamespace } from '@/services/shared/service.shared.get_namespace';
import { ResourceMeta } from '@dot-i/k8s-operator';
import awaitRetryAttempt from '@/services/shared/misc/service.shared.await_retry_attempt';
import getCustomResource from '@/services/shared/k8s/service.shared.k8s.get_resource';
import getName from '@/services/namespaces/misc/service.namespace.get_meta_name';
import { V1CustomResourceDefinitionList } from '@kubernetes/client-node';


export const getService = (
  configNamespace = configPulsarNamespace, 
  configTopic = configPulsarTopic
) =>
  async (
    metaTopic: ResourceMeta,
    pulsarTopic: PulsarTopicInK8s,
    retryAttempt = 0
  ):Promise<boolean> => {
    const k8sNamespace = getNamespace(metaTopic);
    const pulsarNamespace = getName(pulsarTopic.spec.tenant, pulsarTopic.spec.namespace);
    const readiness = configTopic.probing.readiness;
    try {
      const { group, version, plural } = configNamespace.main;

      const namespacesInK8s = ((await ConfigK8s.apiCustom.listNamespacedCustomObject(
        group, version, k8sNamespace, plural
      )).body as V1CustomResourceDefinitionList)
        .items;
        
      const namespaceInK8s = namespacesInK8s
        .filter((nsK8s: any) =>
          nsK8s.spec?.name === pulsarTopic.spec.namespace &&
          nsK8s.spec?.tenant === pulsarTopic.spec.tenant
        )[0] as any as PulsarNamespaceInK8s | undefined;
      
      // const namespaceInK8s = await getCustomResource<PulsarNamespaceInK8s>(
      //   group, version, k8sNamespace, plural, pulsarNamespace
      // );
      
      if (!namespaceInK8s){
        logger.error(`${getFullName(metaTopic)}: No corresponding namespace with name ${pulsarNamespace} exists in k8s cluster.`);
        return false;
      }

      switch (namespaceInK8s.status.state){
        case 'active':
          logger.debug(`${getFullName(metaTopic)}: corresponding namespace ${pulsarNamespace} is already active`);
          return true;

        case 'creating':
        case 'updating':
          if (retryAttempt < readiness.maxAttempts){
            await awaitRetryAttempt(
              retryAttempt, readiness, `${getFullName(metaTopic)}: corresponding namespace is still in state ${namespaceInK8s.status.state}`
            );
            return await getService(configNamespace, configTopic)(metaTopic, pulsarTopic, retryAttempt+1);
          } else {
            logger.error(`${getFullName(metaTopic)}: Corresponding namespace still in state ${namespaceInK8s.status.state} after max retry attempts. Giving up`);
            return false;
          }
          
        case 'terminating':
        case 'error':
        case undefined:
          logger.error(`${getFullName(metaTopic)}: Corresponding namespace is in state ${namespaceInK8s.status.state}.`);
          return false;
      }

    } catch (err:any) {
      logError(err);
      return false;
    }
  };

export default getService();