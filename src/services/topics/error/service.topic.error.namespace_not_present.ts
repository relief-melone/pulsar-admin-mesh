import { PulsarTopicInK8s } from '@/classes/PulsarTopic';
import logError from '@/services/errors/service.errors.try_to_log_error_message';
import { StatusPatcher } from '@/services/shared/k8s/service.shared.k8s.get_status_patcher';

export default async (patchStatus: StatusPatcher<PulsarTopicInK8s>):Promise<void> => {
  try {
    await patchStatus({
      state: 'error' ,
      statusMessage: 'corresponding namespace not found',
    });
  } catch (err){
    logError('could not patch error status for namespace not present');
  }
};