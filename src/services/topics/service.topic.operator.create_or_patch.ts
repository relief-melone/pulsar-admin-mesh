import { PulsarTopicInK8s } from '@/classes/PulsarTopic';
import logger from '@/log/logger.default';
import { ResourceEvent } from '@dot-i/k8s-operator';
import logError from '../errors/service.errors.try_to_log_error_message';
import getFullName from '../shared/misc/service.shared.get_full_name';
import { getNamespace } from '../shared/service.shared.get_namespace';
import { StatusPatcher } from '../shared/k8s/service.shared.k8s.get_status_patcher';
import awaitNamespaceReady from './condition/service.topic.await_namespace_ready';
import updateIsNecessary from './condition/service.topic.update_is_necessary';
import getTopic from './misc/service.topic.get_from_resource_event';
import getTopicFromResourceEvent from './misc/service.topic.get_from_resource_event';
import topicInK8sToInternal from './misc/service.topic.k8s_topic_to_internal_topic';
import createTopic from './pulsar/service.topic.rest.create_one';
import getCurrent from './k8s/service.topic.rest.get_one.managed';
import topicExistsInPulsar from './pulsar/service.topics.topic_exists_in_pulsar';
import setLastApplied from '../shared/k8s/service.shared.k8s.set_last_applied_config';
import configPulsarTopic from '@/configs/config.pulsar.topic';
import updateTopic from './service.topic.update';

export const getService = (configTopic = configPulsarTopic) =>
  async (e: ResourceEvent, sp: StatusPatcher<PulsarTopicInK8s>) => {
    const meta = e.meta;
    const namespaceK8s = getNamespace(meta);
    const topic = getTopicFromResourceEvent(e);
    const namespaceReady = await awaitNamespaceReady(e.meta, topic);

    if (!namespaceReady)
      return;

    try {
      if (await topicExistsInPulsar(topic)){
        logger.debug(`${getFullName(meta)}: topic already exists. patching...`);
        await updateTopic(meta, topic, sp);
        // TODO: updateTopic
      
        
      } else {
        logger.debug(`${getFullName(meta)}: topic does not yet exist. creating new one...`);
        await createTopic(meta, topic, sp);
      }
      await setLastApplied(topic, configTopic);

    } catch (err:any) {
      logError(err, `${getFullName(meta)}: error in create_or_patch for topic`);
    }

  };

export default getService();