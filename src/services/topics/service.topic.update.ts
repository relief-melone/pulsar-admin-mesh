import { StatusPatch } from '@/classes/CustomResourceShared';
import { ConfigValueUpdateType } from '@/classes/Internal';
import { PulsarTopicInK8s, PulsarTopicUpdateFunc } from '@/classes/PulsarTopic';
import logger from '@/log/logger.default';
import { ResourceMeta } from '@dot-i/k8s-operator';
import getErrorPatch, { ErrorTypeTopic } from '../errors/service.errors.topic.get_error_patch';
import { StatusPatcher } from '../shared/k8s/service.shared.k8s.get_status_patcher';
import getFullName from '../shared/misc/service.shared.get_full_name';
import getPreviouslyApplied from './misc/service.topic.get_previously_applied';
import partitions from './pulsar/service.topic.pulsar.update.partitions';

export const getService = () =>
  async (
    meta: ResourceMeta,
    topic: PulsarTopicInK8s,
    ps: StatusPatcher<PulsarTopicInK8s>
  ): Promise<void> => {
    await ps({ state: 'updating' });

    const topicNow = await getPreviouslyApplied(topic);

    if (!topicNow){
      logger.warn(`${getFullName(meta)}: could not update topic as no previous configuration could be found`);
      await ps(getErrorPatch(ErrorTypeTopic.UpdateFailed, 'could not update topic as no previous config could be found' ));
      return;
    }

    const updateOperations:Record<string, PulsarTopicUpdateFunc> = {
      partitions
    };
    

    const operationResults = {
      changed: 0,
      added: 0,
      removed: 0,
      error: 0,
      ops: [] as string[]
    };

    const erroredOps:Array<[StatusPatch | null, ConfigValueUpdateType]> = [];
    
    for (const [name, op] of Object.entries(updateOperations)){
      const result = await op(topicNow, topic);
      if (result[0]){
        operationResults.error++;
        erroredOps.push(result);
        continue;
      } else if (result[1] !== ConfigValueUpdateType.UNCHANGED) {
        operationResults.ops.push(name);
      }
      switch (result[1]){
        case ConfigValueUpdateType.ADDED:
          operationResults.added++;          
          break;

        case ConfigValueUpdateType.CHANGED:
          operationResults.changed++;
          break;

        case ConfigValueUpdateType.REMOVED:
          operationResults.removed++;
          break;
          
        default:
          break;
      }
    }
    
    const firstOperationWithError = erroredOps[0]?.[0];
    

    if (firstOperationWithError){
      await ps(firstOperationWithError);
      return;
    }

    logger.info(`${getFullName(meta)}: update complete`);
    logger.info(`\tsettings added:  \t${operationResults.added}`);
    logger.info(`\tsettings changed:\t${operationResults.changed}`);
    logger.info(`\tsettings removed:\t${operationResults.removed}`);
    logger.info(`\tsettings errored:\t${erroredOps.length}`);
    logger.info(`\tconfigs treated: \t${operationResults.ops.join(', ')}`);

    await ps({ 
      failedRetries: 0,
      state: 'active',
      statusMessage: 'OK'
    });
  };

export default getService();