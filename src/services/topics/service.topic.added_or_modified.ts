import OperatorPulsarTopic from '@/classes/Operator.PulsarTopic';
import { PulsarTopicInK8s } from '@/classes/PulsarTopic';
import { ResourceEvent } from '@dot-i/k8s-operator';
import { getPatchStatus, StatusPatcher } from '../shared/k8s/service.shared.k8s.get_status_patcher';
import isInDeletionStage from '@/services/shared/k8s/service.shared.k8s.object_is_in_deletion_stage';
import logErr from '../errors/service.errors.try_to_log_error_message';
import getTopicFromResourceEvent from './misc/service.topic.get_from_resource_event';
import { getNamespace } from '../shared/service.shared.get_namespace';
import getPulsarNamespaceInK8s from '../namespaces/misc/service.namespace.rest.get_one.managed';
import getK8sResourceFromMeta from '../shared/k8s/service.shared.k8s.get_resource.from_meta';
import ConfigPulsarTopic from '@/configs/config.pulsar.topic';
import logger from '@/log/logger.default';
import getFullName from '../shared/misc/service.shared.get_full_name';
import deleteTopic from './service.topic.operator.delete';
import createOrPatch from './service.topic.operator.create_or_patch';
import updateIsNecessary from './condition/service.topic.update_is_necessary';
import getNamespaceFromResourceEvent from '../namespaces/misc/service.namespace.get_from_resource_event';
import handleNamespaceNotPresent from './error/service.topic.error.namespace_not_present';
import { addHandledResourceGeneration, resourceGenerationWasRecentlyHandled } from '../shared/k8s/service.shared.k8s.resource_was_recently_handled';

export const getService = (configTopic = ConfigPulsarTopic) =>
  async function(this: OperatorPulsarTopic, e: ResourceEvent):Promise<void>{

    const topic = getTopicFromResourceEvent(e);
    const patchStatus = getPatchStatus.bind(this)(e.meta) as StatusPatcher<PulsarTopicInK8s>;

    if (!isInDeletionStage(e)){
      if (!await updateIsNecessary(e.meta, topic))
        return;

      const ns = getNamespace(e.meta);
 
      // CHECK IF CORRESPONDING TENANT IS PRESENT AND VALID
      const pulsarNamespace = await getPulsarNamespaceInK8s(ns, topic.spec.tenant, topic.spec.namespace);
      if (!pulsarNamespace)
        return await handleNamespaceNotPresent(patchStatus);
      
      if (pulsarNamespace.status.state === 'error'){
        // TODO: handle pulsar namespace has error
      }

      const k8sResource = await getK8sResourceFromMeta(e.meta, configTopic.main.plural);
      if (!k8sResource){
        logger.info(`${getFullName(e.meta)}: topic has already been deleted. nothing to do...`);
        return;
      }
    }

    let finalizerFinishedRunning = false;
    try {
      await this.handleResourceFinalizer(
        e,
        'delete-child-resources',
        async evt => {
          try {
            const topic = getTopicFromResourceEvent(evt);
            await deleteTopic(e.meta, topic, patchStatus);
            finalizerFinishedRunning = true;
          } catch (err:any) {
            logErr(err, `${getFullName(e.meta)}: error deleting child resources for topic,`);
          }
        });
      if (!finalizerFinishedRunning){
        await createOrPatch(e, patchStatus);
      }
    } catch (err:any) {
      logErr(err);
    }
    
  }; 

export default getService();