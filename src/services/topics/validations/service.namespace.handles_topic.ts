import ConfigNamespace from '@/configs/config.pulsar.namespace';
import ConfigTenant from '@/configs/config.pulsar.tenant';
import ConfigTopic from '@/configs/config.pulsar.topic';
import logger from '@/log/logger.default';
import resourceNotIncludedInWhitelist from '@/services/shared/validations/service.shared.resource_not_included_in_whitelist';

export const getService = (
  configTenant = ConfigTenant, 
  configNamespace = ConfigNamespace, 
  configTopic = ConfigTopic
) =>
  (tenant?: string, namespace?: string, topic?: string): boolean => {
    if (!tenant || !namespace || !topic){
      logger.warn('namespace not handled as either tenant,namespace or topic is not defined');
      return false;
    }

    if (resourceNotIncludedInWhitelist(tenant, configTenant)){
      logger.debug(`tenant ${tenant} is not whitelisted. Will not handle namespace ${namespace}`);
      return false;
    }

    if (resourceNotIncludedInWhitelist(namespace, configNamespace)){
      logger.debug(`namespace ${namespace} is not whitelisted. Will not handle topic ${topic}`);
      return false;
    }

    if (resourceNotIncludedInWhitelist(topic, configTopic)){
      logger.debug(`topic ${topic} is no whitelisted. Will not be handled`);
      return false;
    }

    logger.debug(`topic ${topic} handeling topic`);
    return true;
  };

export default getService();