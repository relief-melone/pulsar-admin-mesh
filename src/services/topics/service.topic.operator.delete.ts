import { PulsarTopicInK8s } from '@/classes/PulsarTopic';
import { ResourceMeta } from '@dot-i/k8s-operator';
import logError from '../errors/service.errors.try_to_log_error_message';
import getFullName from '../shared/misc/service.shared.get_full_name';
import { StatusPatcher } from '../shared/k8s/service.shared.k8s.get_status_patcher';
import deleteTopic from './pulsar/service.topics.delete_one';
import k8sTopicToInternalTopic from './misc/service.topic.k8s_topic_to_internal_topic';
import terminateTopic from './pulsar/service.topics.terminate';

export const getService = () =>
  async (
    topicMeta: ResourceMeta,
    topicK8s: PulsarTopicInK8s,
    patchStatus: StatusPatcher<PulsarTopicInK8s>
  ):Promise<void> => {
    try {
      await patchStatus({
        state: 'terminating'
      });
      const topicInternal = k8sTopicToInternalTopic(topicK8s);
      
      await terminateTopic(topicInternal);
      await deleteTopic(topicInternal);

    } catch (err:any) {
      logError(err, `${getFullName}: error deleting managed topic`);
      // throw err;
    }
  };

export default getService();