import { PulsarTopicInternal } from '@/classes/PulsarTopic';
import ConfigPulsarTopic from '@/configs/config.pulsar.topic';
import logger from '@/log/logger.default';
import wait from '@root/test/helpers/wait';
import logError from '../errors/service.errors.try_to_log_error_message';
import PulsarRestClient from '../shared/service.shared.get_pulsar_rest_client';
import getFullName from '../topics/misc/service.topic.get_full_name';
import deleteSubscription from './service.subscription.delete_one';
import getSubscriptionsForTopic from './service.subscription.get_for_topic';

const maxAttempts = 10;
const incrementorMs = 2000;

export const getService = (client = PulsarRestClient, configPulsarTopic = ConfigPulsarTopic) => 
  async (
    topic: PulsarTopicInternal,
    subscriptions = topic.status.subscriptions,
    currentAttempt = 0
  ) => {
    try {
      logger.info(`${getFullName(topic)}: Deleting subscriptions for topic`);

      const deletionActions = subscriptions.items?.map(async s => {
        deleteSubscription(topic, s);
      }) || [];

      await Promise.all(deletionActions);
      const subsLeft = await getSubscriptionsForTopic(topic);

      if (subsLeft.count === 0)
        return;
      
      if (currentAttempt < maxAttempts){
        const waitTime = incrementorMs * ++currentAttempt;
        logger.info(`${getFullName(topic)}: still ${subsLeft.count} subscriptions left. retrying in ${waitTime} ms. Attempt ${currentAttempt}/${maxAttempts}`);
        await wait(waitTime);
        await getService(client, configPulsarTopic)(topic, subsLeft, currentAttempt);
      } else {
        const msg = `${getFullName(topic)}: still ${subsLeft.count} subscriptions left. giving up after ${maxAttempts}`;
        logger.error(msg);
        throw new Error(msg);
      } 

      logger.info(`${getFullName(topic)}: Subscriptions successfully deleted`);

    } catch (err: any) {      
      logError(err, `${getFullName(topic)}: Could not delete subscription for topic`);
      throw err;
    }
  };

export default getService();
  