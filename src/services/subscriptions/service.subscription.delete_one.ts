import { PulsarSubscription, PulsarTopicInternal } from '@/classes/PulsarTopic';
import logger from '@/log/logger.default';
import logError from '../errors/service.errors.try_to_log_error_message';
import { getTopicTypePrefixForInternalTopic as getPrefix } from '../shared/pulsar/service.shared.get_topic_type_prefix';
import PulsarRestClient from '../shared/service.shared.get_pulsar_rest_client';
import getFullName from '../topics/misc/service.topic.get_full_name';

export const getService = ( client = PulsarRestClient) =>
  async (topic: PulsarTopicInternal, subscription: PulsarSubscription):Promise<void> => {
    try {
      await client.delete(
        `${getPrefix(topic)}/${topic.tenant}/${topic.namespace}/${topic.name}/subscription/${subscription.name}`,{
          params: {
            force: true
          },
        }
      );
    } catch (err: any) {
      if (err.response.status === 404)
        return;
      else {
        logError(err,`${getFullName(topic)}: error deleting subscription ${subscription}`);
        // throw new Error(msg);
      }
    }
  };
export default getService();
  