import { PulsarSubscription, PulsarTopicInternal, PulsarTopicStatus } from '@/classes/PulsarTopic';
import logger from '@/log/logger.default';
import errIsTopicNotFound from '../errors/service.errors.is_topic_not_found';
import PulsarRestClient from '../shared/service.shared.get_pulsar_rest_client';
import getFullName from '../topics/misc/service.topic.get_full_name';
import getStatsForTopic from '../topics/pulsar/service.topics.get_stats.from_interal_topic';
import getSubsFromStats from '../topics/misc/service.topics.get_subscriptions_from_stats';

// TODO: test case. All goo
// TODO: test case. Topic not found
// TODO: test case. something went wrong

export const getService = ( client = PulsarRestClient) =>
  async (topic: PulsarTopicInternal):Promise<PulsarTopicStatus['subscriptions']> => {
    try {
      const stats = await getStatsForTopic(topic);
      return getSubsFromStats(stats);
    } catch (err: any) {
      if (errIsTopicNotFound(err)){
        logger.info(`${getFullName(topic)}: Topic was already deleted`);
        return {
          items: [],
          count: 0
        };
      }

      const msg = `${getFullName(topic)}: error getting subscriptions for topic`;
      logger.error(msg);
      throw new Error(msg);
    }
  };

export default getService();
