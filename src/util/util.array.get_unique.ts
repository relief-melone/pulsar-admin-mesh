export default <T>(arr:Array<T>):Array<T> =>
  arr.filter((item, index, self) => self.indexOf(item) === index);