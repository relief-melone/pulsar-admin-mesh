import logger from '@/log/logger.default';

export default (proc = process) => {
  logger.info(`process id: \t${proc.pid}`);
};