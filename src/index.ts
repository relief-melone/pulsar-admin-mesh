import wait from '@root/test/helpers/wait';
import { setDefaultResultOrder } from 'dns';
import OperatorPulsarNamespace from './classes/Operator.PulsarNamespace';
import OperatorPulsarTenant from './classes/Operator.PulsarTenants';
import OperatorPulsarTopic from './classes/Operator.PulsarTopic';
import ConfigPulsarCluster from './configs/config.pulsar.cluster';
import logStartupInfo from './util/util.log_startup_info';
import { setShutdown } from './services/shared/service.shutdown';
import serviceHealth_endpoint from './services/health/service.health_endpoint';
import quitIfPrerequisitesAreNotMet from './services/health/service.quit_if_prerequisites_not_met';

export default async (configPulsarCluster = ConfigPulsarCluster) => {

  if (configPulsarCluster.dnsSettings.Ipv4First)
    setDefaultResultOrder('ipv4first');

  logStartupInfo();
  await quitIfPrerequisitesAreNotMet();

  const operatorPulsarTenant = new OperatorPulsarTenant();
  const operatorPulsarNamespace = new OperatorPulsarNamespace();
  const operatorPulsarTopic = new OperatorPulsarTopic();

  await operatorPulsarTenant.start();
  await operatorPulsarNamespace.start();
  await operatorPulsarTopic.start();
  await serviceHealth_endpoint();

  await wait(20000);


  setShutdown([ 
    operatorPulsarTenant,
    operatorPulsarNamespace,
    operatorPulsarTopic
  ]);
};