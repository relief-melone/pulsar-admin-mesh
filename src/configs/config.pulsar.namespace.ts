import Config from '@/classes/Config';
import { ConfigCustomResource, CustomResourceDebugConfig, NamespaceDebugConfig, ProbingSettings } from '@/classes/Internal';
import { ConfigMinStatusMonitor } from '@/classes/StatusMonitorNew';
import { ConfigDebug } from './config.debug';

export class ConfigPulsarNamespace extends Config implements ConfigMinStatusMonitor, ConfigCustomResource {
  main = {
    version: 'v1alpha1',
    group: 'admin.pulsar.io',
    plural: 'pulsarnamespaces',
    kind: 'PulsarNamespace'
  };

  statusMonitor: ConfigMinStatusMonitor['statusMonitor'];

  deleteNamespacesWithTenant: boolean;

  probing: {
    readiness: ProbingSettings,
    deletion: ProbingSettings,
  };

  autoSync: {
    checkIntervalMs: number
  };

  debug: NamespaceDebugConfig;

  

  constructor(env = process.env){
    super(env);

    this.statusMonitor = {
      checkIntervalMs: parseInt(this.env('PULSAR__NAMESPACE__STATUS_MONITOR__CHECK_INTERVAL_MS', '10000')),
      creationTimeoutInSeconds: parseInt(this.env('PULSAR__NAMESPACE__STATUS_MONITOR__CREATION_TIMEOUT_IN_SECONDS', '120'))
    };

    this.deleteNamespacesWithTenant = this.env('PULSAR__NAMESPACE__DELETE_NAMESPACES_WITH_TENANT', 'false') === 'true';

    this.probing = {
      readiness: {
        intervalIncrementorMs: parseInt(this.env('PULSAR__NAMESPACE__PROBING__READINESS__INTERVAL_INCREMENTOR_MS', '500')),
        maxAttempts: parseInt(this.env('PULSAR__NAMESPACE__PROBING__READINESS__MAX_ATTEMPTS','5'))
      },
      deletion: {
        intervalIncrementorMs: parseInt(this.env('PULSAR__NAMESPACE__PROBING__DELETION__INTERVAL_INCREMENTOR_MS', '500')),
        maxAttempts: parseInt(this.env('PULSAR__NAMESPACE__PROBING__DELETION__MAX_ATTEMPTS','5'))
      }
    };

    this.autoSync = {
      checkIntervalMs: parseInt(this.env('PULSAR__NAMESPACE__AUTO_SYNC__CHECK_INTERVAL_MS', '10000')),
    };

    this.debug = {
      whitelist: this.env('PULSAR__NAMESPACE__DEBUG__WHITELIST', '') ? this.env('PULSAR__NAMESPACE__DEBUG__WHITELIST','').split(',').map(s => s.trim()) : null,
      ignoredIfNotSetConfigOpts: this.env('PULSAR__NAMESPACE__DEBUG__IGNORED_IF_NOT_SET_CONFIG_OPTIONS', '') ? this.env('PULSAR__NAMESPACE__DEBUG__IGNORED_IF_NOT_SET_CONFIG_OPTIONS', '').split(',') : [],
      ignoredPulsarSettings: this.env('PULSAR__NAMESPACE__IGNORED_PULSAR_SETTINGS', '') ? this.env('PULSAR__NAMESPACE__IGNORED_PULSAR_SETTINGS', '').split(',') : [],
    };

  }
}

const configPulsarNamespace = new ConfigPulsarNamespace();
export default configPulsarNamespace;