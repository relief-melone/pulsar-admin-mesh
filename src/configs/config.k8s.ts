import Config from '@/classes/Config';
import { ApiextensionsV1Api, AppsV1Api, CoreV1Api, CustomObjectsApi, KubeConfig } from '@kubernetes/client-node';

export class ConfigK8s extends Config {
  kc: KubeConfig;
  apiCustom: CustomObjectsApi;
  apiCore: CoreV1Api;
  apiApps: AppsV1Api;
  apiExtensions: ApiextensionsV1Api;
  
  constructor(env = process.env){
    super(env);
    this.kc = new KubeConfig();
    this.kc.loadFromDefault();
    this.apiCustom = this.kc.makeApiClient(CustomObjectsApi);
    this.apiCore = this.kc.makeApiClient(CoreV1Api);
    this.apiApps = this.kc.makeApiClient(AppsV1Api);
    this.apiExtensions = this.kc.makeApiClient(ApiextensionsV1Api);
    
    
  }

}

const configDefault = new ConfigK8s();

export default configDefault;