import Config from '@/classes/Config';
import { ConfigCustomResource, CustomResourceDebugConfig, ProbingSettings } from '@/classes/Internal';
import { ConfigMinStatusMonitor } from '@/classes/StatusMonitorNew';

export class ConfigPulsarTopic extends Config implements ConfigMinStatusMonitor,ConfigCustomResource {
  main = { 
    version: 'v1alpha1',
    group: 'admin.pulsar.io',
    plural: 'pulsartopics',
    kind: 'PulsarTopic'
  };
  statusMonitor:  ConfigMinStatusMonitor['statusMonitor'];

  autoSync: {
    checkIntervalMs: number
  };
  
  probing: {
    readiness: ProbingSettings,
    deletion: ProbingSettings
  };

  debug: CustomResourceDebugConfig;

  constructor( env = process.env) {
    super(env);
    this.statusMonitor = {
      checkIntervalMs: parseInt(this.env('PULSAR__TOPIC__STATUS_MONITOR__CHECK_INTERVAL_MS', '10000')),
      creationTimeoutInSeconds: parseInt(this.env('PULSAR__TOPIC__STATUS_MONITOR__CREATION_TIMEOUT_IN_SECONDS', '120'))
    };

    this.probing = {
      readiness: {
        intervalIncrementorMs: parseInt(this.env('PULSAR__TOPIC__PROBING__READINESS__INTERVAL_INCREMENTOR_MS', '2000')),
        maxAttempts: parseInt(this.env('PULSAR__TOPIC__PROBING__READINESS__MAX_ATTEMPTS', '5'))
      },
      deletion: {
        intervalIncrementorMs: parseInt(this.env('PULSAR__TOPIC__PROBING__DELETION__INTERVAL_INCREMENTOR_MS', '2000')),
        maxAttempts: parseInt(this.env('PULSAR__TOPIC__PROBING__DELETION__MAX_ATTEMPTS', '5'))
      }
    };

    this.autoSync = {
      checkIntervalMs: parseInt(this.env('PULSAR__TOPIC__AUTO_SYNC__CHECK_INTERVAL_MS', '10000')),
    };

    this.debug = {
      whitelist: this.env('PULSAR__TOPIC__DEBUG__WHITELIST', '') ? this.env('PULSAR__TOPIC__DEBUG__WHITELIST','').split(',').map(s => s.trim()) : null
    };
  }
}
const configPulsarTopic = new ConfigPulsarTopic();
export default configPulsarTopic;