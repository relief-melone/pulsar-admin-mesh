import Config from '@/classes/Config';

export class ConfigPulsarCluster extends Config {
  clusterName: string;
  dnsSettings: {
    Ipv4First
  };
  service: {
    broker: string;
    web: string;
    apiEndpoint: string
  };

  constructor(env = process.env){
    super(env);
    this.clusterName = this.env('PULSAR__CLUSTER_NAME', 'pulsar');    
    this.service = {
      apiEndpoint: this.env('PULSAR__SERVICE__API_ENDPOINT', 'https://pulsar-broker.pulsar.svc:8080/admin/v2'),
      broker: this.env('PULSAR__SERVICE__BROKER', 'pulsar://pulsar-broker.pulsar.svc:6650'),
      web: this.env('PULSAR__SERVICE__WEB', 'http://pulsar-broker.pulsar.svc:8080'),
    };
    this.dnsSettings = {
      Ipv4First: this.env('PULSAR__CLUSTER__DNS_SETTINGS__DISABLE_IPV4FIRST', 'true') === 'true'
    };
  }
}

const configPulsarCluster = new ConfigPulsarCluster();
export default configPulsarCluster;