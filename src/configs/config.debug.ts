import Config from '@/classes/Config';

export interface ReportConfig {
  enabled: boolean
  intervalMs: number
}

export class ConfigDebug extends Config {
  memoryReports: ReportConfig;
  httpTracing: {
    enabled: boolean
  };

  constructor(env = process.env) {
    super(env);

    this.memoryReports = {
      enabled: this.env('DEBUG__MEMORY_REPORTS__ENABLED', 'false') !== 'false',
      intervalMs: parseInt(this.env('DEBUG__MEMORY_REPORTS__INTERVAL_MS', '10000')),      
    };

    this.httpTracing = {
      enabled: this.env('DEBUG__HTTP_TRACING__ENABLED', 'false') !== 'false'
    };

  }
}

const config = new ConfigDebug();
export default config;