import Config from '@/classes/Config';
import { ConfigCustomResource, CustomResourceDebugConfig, ProbingSettings } from '@/classes/Internal';
import { ConfigMinStatusMonitor } from '@/classes/StatusMonitorNew';

export class ConfigPulsarTenant extends Config implements ConfigMinStatusMonitor,ConfigCustomResource {
  main = {
    version: 'v1alpha1',
    group: 'admin.pulsar.io',
    plural: 'pulsartenants',
    kind: 'PulsarTenant'
  };
  statusMonitor: ConfigMinStatusMonitor['statusMonitor'];
  
  probing: {
    readiness: ProbingSettings,
    deletion: ProbingSettings
  };

  autoSync: {
    checkIntervalMs: number, // has no autoimporter yet
  };

  debug: CustomResourceDebugConfig;
  

  constructor(env = process.env){
    super(env);

    this.probing = {
      readiness: {
        intervalIncrementorMs: parseInt(this.env('PULSAR__TENANT__PROBING__READINESS__INTERVAL_INCREMENTOR_MS', '2000')),
        maxAttempts: parseInt(this.env('PULSAR__TENANT__PROBING__READINESS__MAX_ATTEMPTS', '5'))
      },
      deletion: {
        intervalIncrementorMs: parseInt(this.env('PULSAR__TENANT__PROBING__DELETION__INTERVAL_INCREMENTOR_MS', '2000')),
        maxAttempts: parseInt(this.env('PULSAR__TENANT__PROBING__DELETION__MAX_ATTEMPTS', '5'))
      }
    };

    this.statusMonitor = {
      checkIntervalMs: parseInt(this.env('PULSAR__TENANT__STATUS_MONITOR__CHECK_INTERVAL_MS', '10000')),
      creationTimeoutInSeconds: parseInt(this.env('PULSAR__TENANT__STATUS_MONITOR__CREATION_TIMEOUT_IN_SECONDS', '120'))
    };

    this.autoSync = {
      checkIntervalMs: parseInt(this.env('PULSAR__TENANT__AUTO_SYNC__CHECK_INTERVAL_MS', '10000')),
    };

    this.debug = {
      whitelist: this.env('PULSAR__TENANT__DEBUG__WHITELIST', '') ? this.env('PULSAR__TENANT__DEBUG__WHITELIST','').split(',').map(s => s.trim()) : null
    };
  }
}

const configPulsarTenant = new ConfigPulsarTenant();
export default configPulsarTenant;