export interface GenerationHandled {
  generation: number,
  timestamp: number
}

export default class K8sResourceGenerationHandledTracker {
  generationsRecentlyHandled: Record<string, GenerationHandled>;
  cleanIntervalSeconds: number;
  maxAgeEntrySeconds: number;

  constructor( 
    cleanIntervalSeconds = 30, 
    maxAgeSeconds = 30
  ){
    this.generationsRecentlyHandled = {};
    this.cleanIntervalSeconds = cleanIntervalSeconds;
    this.maxAgeEntrySeconds = maxAgeSeconds;

    setInterval(() => this.cleanGenerationsHandled(), this.cleanIntervalSeconds*1000);
  }

  cleanGenerationsHandled(){
    const now = Date.now();
    for ( const resourceId of Object.keys(this.generationsRecentlyHandled)) {
      const generation = this.generationsRecentlyHandled[resourceId];
      if ( now - generation.timestamp > this.maxAgeEntrySeconds * 1000)
        delete this.generationsRecentlyHandled[resourceId];
    }
  }

  generationWasRecentlyHandled(resourceId?: string, resourceGeneration?: number):boolean {
    if (resourceId === undefined || resourceGeneration === undefined)
      return false;

    const lastGenerationHandled = this.generationsRecentlyHandled[resourceId];

    if (!lastGenerationHandled)
      return false;
    else 
      return lastGenerationHandled.generation === resourceGeneration;
  }

  addHandledGeneration(resourceId?: string, resourceGeneration?: number){
    if (resourceId === undefined || resourceGeneration === undefined)
      return;

    else
      this.generationsRecentlyHandled[resourceId] = {
        generation: resourceGeneration,
        timestamp: Date.now()
      };
  }
}