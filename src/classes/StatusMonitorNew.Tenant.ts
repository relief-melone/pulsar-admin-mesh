import configK8s, { ConfigK8s } from '@/configs/config.k8s';
import configTenant, { ConfigPulsarTenant } from '@/configs/config.pulsar.tenant';
import logger from '@/log/logger.default';
import listNamespacedCustomObjectsAllNamespaces from '@/services/shared/k8s/service.shared.k8s.list_namespaced_object_all_namespaces';
import PulsarRestClient from '@/services/shared/service.shared.get_pulsar_rest_client';
import isActive from '@/services/tenant/status_monitor/service.tenant.status_monitor.check_if_change_to_active';
import updateNamespaces from '@/services/tenant/status_monitor/service.tenant.status_monitor.update_namespaces';
import { PulsarTenantInK8s, PulsarTenantInPulsar } from './PulsarTenant';
import StatusMonitorV2, { StatusMonitorV2Config } from './StatusMonitorNew';
import operatorHandlesTenant from '@/services/tenant/validations/service.tenant.validations.handles_tenant';
import configPulsarTenant from '@/configs/config.pulsar.tenant';


export class StatusMonitorTenantV2Config implements StatusMonitorV2Config {
  configTenant: ConfigPulsarTenant;
  pulsarClient: typeof PulsarRestClient;
  name: string;
  intervalMs: number;


  constructor(cTenant = configTenant, client=PulsarRestClient ){
    this.configTenant = cTenant;
    this.pulsarClient = client;
    this.name = 'SM Tenants';
    this.intervalMs = cTenant.statusMonitor.checkIntervalMs;
  }
}

export default class StatusMonitorTenantV2 extends StatusMonitorV2<PulsarTenantInPulsar, PulsarTenantInK8s> {
  configTenant: ConfigPulsarTenant;
  pulsarClient: typeof PulsarRestClient;

  constructor(
    config= new StatusMonitorTenantV2Config(),
    configItem = configPulsarTenant
  ){
    super(config, configItem);
    this.name = config.name;
    this.configTenant = config.configTenant || configTenant;
    this.pulsarClient = config.pulsarClient || PulsarRestClient;

    this.statePatchers.push(
      updateNamespaces,
      isActive
    );
  }

  async getK8sItems(): Promise<PulsarTenantInK8s[]> {
    const { group, version, plural } = this.configTenant.main;
    const items = await listNamespacedCustomObjectsAllNamespaces<PulsarTenantInK8s>(group, version, plural);
    logger.debug(`Status Monitor Tenant: Watching ${items.length} items`);
    return items;
  }

  async getExternalItemState(item: PulsarTenantInK8s): Promise<PulsarTenantInPulsar | null> {
    const tenant = item.metadata?.name;
    if (!tenant){
      logger.error('status monitor tenants: tenant has no name');
      return null;
    }

    try {
      const currentState = (await this.pulsarClient.get<PulsarTenantInPulsar>(
        `tenants/${tenant}`
      )).data;
      return currentState || {};
    } catch (err:any) {
      logger.error('status monitor for tenant could not get external state');
      return null;
    }
  }

  statusMonitorHandlesItem(item: PulsarTenantInK8s): boolean {
    const name = item.metadata?.name;
    
    if (!name){
      const msg = 'error in status monitor for tenant. Cannot get name of tenant';
      logger.error(msg);
      throw new Error(msg);
    }
      
    return operatorHandlesTenant(item.metadata?.name);
  }
}