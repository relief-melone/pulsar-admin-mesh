import { KubernetesObject } from '@kubernetes/client-node';
import { Pulsartopic } from './crd.pulsar_topic';
import { StatusPatch } from './CustomResourceShared';
import { IncludeK8sInfo, ConfigValueUpdateType } from './Internal';

export type PulsarTopicInK8s = Pulsartopic & KubernetesObject;
export type PulsarTopicStatus = PulsarTopicInK8s['status'];

export interface PulsarTopicStats {
  [k: string]: any,
  storageSize: number,
  backlogSize: number,
  partitions?: Record<string,PulsarTopicStats>,
  subscriptions: Record<string,any>,
  publishers: Array<Record<string,any>>
  metadata: {
    partitions?: number
  }
}

export interface PulsarTopicInternal extends Partial<IncludeK8sInfo> {
  name: string
  persistent: boolean,
  partitioned: boolean,
  tenant: string,  
  namespace: string
  partitions: number,
  status: PulsarTopicStatus

  k8sMetaName?: string  
  // stats: PulsarTopicStats 
}

export interface PulsarPartition {
  name: string,
  storageSize: number,
  details: Record<string,any>
}

export interface PulsarSubscription {
  name: string
}

export type PulsarTopicUpdateFunc = (
  topicNow: PulsarTopicInK8s,
  topicDesired: PulsarTopicInK8s,
) => Promise<[StatusPatch | null, ConfigValueUpdateType]>;