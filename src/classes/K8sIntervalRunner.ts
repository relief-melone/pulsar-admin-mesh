import configK8s, { ConfigK8s } from '@/configs/config.k8s';
import { KubernetesObject } from '@kubernetes/client-node';
import { CustomResourceMin } from './Internal';

export interface ConfigK8sIntervalRunnerMin<T extends KubernetesObject & CustomResourceMin> {
  configK8s: ConfigK8s,
  intervalMs: number
  operations?: Array<IntervalRunner<T>>
}


export type IntervalRunner<T extends KubernetesObject & CustomResourceMin> = (input:T) => Promise<void>;
export default class K8sIntervalRunner<T extends KubernetesObject & CustomResourceMin> {
  
  configK8s: ConfigK8s;
  intervalMs: number;
  operations: Array<IntervalRunner<T>>;

  constructor(
    config: ConfigK8sIntervalRunnerMin<T>
  ){
    this.configK8s = config.configK8s;
    this.intervalMs = config.intervalMs;
    this.operations = config.operations || [];
  }


}