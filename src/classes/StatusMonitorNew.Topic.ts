import configK8s, { ConfigK8s } from '@/configs/config.k8s';
import configPulsarTopic, { ConfigPulsarTopic } from '@/configs/config.pulsar.topic';
import logger from '@/log/logger.default';
import listNamespacedCustomObjectsAllNamespaces from '@/services/shared/k8s/service.shared.k8s.list_namespaced_object_all_namespaces';
import PulsarRestClient from '@/services/shared/service.shared.get_pulsar_rest_client';
import { PulsarTopicInK8s, PulsarTopicStats } from './PulsarTopic';
import StatusMonitorV2, { StatusMonitorV2Config } from './StatusMonitorNew';
import getTopicStats from '@/services/topics/pulsar/service.topics.get_stats';
import logError from '@/services/errors/service.errors.try_to_log_error_message';
import processTopicStats from '@/services/topics/status_monitor/service.topic.status_monitor.process_topic_stats';
import isPartitionsNotYetCreated from '@/services/errors/service.errors.is_partitions_not_yet_created';
import operatorHandlesTopic from '@/services/topics/validations/service.namespace.handles_topic';
import isTopicNotFound from '@/services/errors/service.errors.is_topic_not_found';
import getTopicName from '@/services/topics/misc/service.topic.get_full_name';
import getInternalTopic from '@/services/topics/misc/service.topic.k8s_topic_to_internal_topic';

export class StatusMonitorTopicV2Config implements StatusMonitorV2Config {
  configK8s: ConfigK8s;
  configTopic: ConfigPulsarTopic;
  pulsarClient: typeof PulsarRestClient;
  name: string;
  intervalMs: number;


  constructor(ck8s = configK8s, cTopic = configPulsarTopic, client=PulsarRestClient ){
    this.configK8s = ck8s;
    this.configTopic = cTopic;
    this.pulsarClient = client;
    this.name = 'SM Topics';
    this.intervalMs = cTopic.statusMonitor.checkIntervalMs;
  }
}

export default class StatusMonitorTopicV2 extends StatusMonitorV2<PulsarTopicStats, PulsarTopicInK8s> {
  configTopic: ConfigPulsarTopic;
  pulsarClient: typeof PulsarRestClient;

  constructor(
    config = new StatusMonitorTopicV2Config(),
    configItem = configPulsarTopic
  ){
    super(config, configItem);
    this.name = config.name;
    this.configTopic = config.configTopic || configPulsarTopic;
    this.pulsarClient = config.pulsarClient || PulsarRestClient;

    this.statePatchers.push(
      processTopicStats
    );
  }

  async getK8sItems(): Promise<PulsarTopicInK8s[]> {
    const { group, version, plural } = this.configTopic.main;
    const items = await listNamespacedCustomObjectsAllNamespaces<PulsarTopicInK8s>(group, version, plural);
    logger.debug(`Status monitor topic: Watching ${items.length} items`);
    return items;
  }

  async getExternalItemState(item: PulsarTopicInK8s): Promise<PulsarTopicStats | null> {
    const { tenant, namespace, name: topic, partitioned, persistent } = item.spec;
    try {
      const stats = await getTopicStats(
        tenant, namespace, topic, persistent, partitioned
      );
      return stats;
    } catch (err:any) {
      if (
        isPartitionsNotYetCreated(err)
        || (isTopicNotFound(err) && item.status.state === 'creating')
      ){
        logger.info(`${getTopicName(getInternalTopic(item))}: topic not yet created. setting status to initial values`);
        return {
          backlogSize: 0,
          metadata: {},
          publishers: [],
          subscriptions: {},
          storageSize: 0
        };
      }
      if (isTopicNotFound(err)){
        logger.info(`${getTopicName(getInternalTopic(item))}: could not get topic stats as topic not existant`);
      } else {
        logError(err);
      }
      
      return null;
    }
  }

  statusMonitorHandlesItem(item: PulsarTopicInK8s): boolean {
    return operatorHandlesTopic(item.spec.tenant, item.spec.namespace, item.spec.name);
  }
}