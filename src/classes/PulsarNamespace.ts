import { KubernetesObject } from '@kubernetes/client-node';
import { Pulsarnamespaces } from './crd.pulsar_namespace';
import { StatusPatch } from './CustomResourceShared';
import { ConfigValueUpdateType, IncludeK8sInfo } from './Internal';

export type PulsarNamespaceInK8s = Pulsarnamespaces & KubernetesObject;

export interface PulsarNamespaceInternal extends IncludeK8sInfo {
  name: string
  tenant: string
  config: PulsarConfig
  permissions: PulsarPermissions
}

export type PulsarConfig = Pulsarnamespaces['spec']['config'];
export type PulsarPermissions = Pulsarnamespaces['spec']['permissions'];

export type PulsarNamespaceConfigUpdateFunc = (
  nsNow: PulsarNamespaceInK8s, 
  nsDesired: PulsarNamespaceInK8s
) => Promise<[StatusPatch | null, ConfigValueUpdateType]>;