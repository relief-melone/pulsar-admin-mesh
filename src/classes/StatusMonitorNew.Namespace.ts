import configK8s, { ConfigK8s } from '@/configs/config.k8s';
import ConfigNamespace, { ConfigPulsarNamespace } from '@/configs/config.pulsar.namespace';
import logger from '@/log/logger.default';
import updateTopics from '@/services/namespaces/status_monitor/service.namespace.status_monitor.update_topics';
import namespaceIsActive from '@/services/namespaces/status_monitor/service.namespace.status_montior.is_active';
import listNamespacedCustomObjectsAllNamespaces from '@/services/shared/k8s/service.shared.k8s.list_namespaced_object_all_namespaces';
import getFullName from '@/services/shared/misc/service.shared.get_full_name';
import { getNamespace } from '@/services/shared/service.shared.get_namespace';
import PulsarRestClient from '@/services/shared/service.shared.get_pulsar_rest_client';
import getMeta from '@/services/shared/service.shared.get_resource_meta_from_kube_object_meta';
import { PatchUtils } from '@kubernetes/client-node';
import { PulsarConfig, PulsarNamespaceInK8s } from './PulsarNamespace';
import StatusMonitorV2, { StatusMonitorV2Config } from './StatusMonitorNew';
import operatorHandlesNamespace from '@/services/namespaces/validations/service.namespace.handles_namespace';

export class StatusMonitorNamespaceV2Config implements StatusMonitorV2Config {
  configK8s: ConfigK8s;
  configNamespace: ConfigPulsarNamespace;
  pulsarClient: typeof PulsarRestClient;
  name: string;
  intervalMs: number;


  constructor(ck8s = configK8s, cNamespace = ConfigNamespace, client=PulsarRestClient ){
    this.configK8s = ck8s;
    this.configNamespace = cNamespace;
    this.pulsarClient = client;
    this.name = 'SM Namespaces';
    this.intervalMs = cNamespace.statusMonitor.checkIntervalMs;
  }
}

export default class StatusMonitorNamespaceV2 extends StatusMonitorV2<Record<string,any>,PulsarNamespaceInK8s> {
  configNamespace: ConfigPulsarNamespace;
  pulsarClient: typeof PulsarRestClient;

  constructor(
    config= new StatusMonitorNamespaceV2Config(),
    configItem = ConfigNamespace
  ){
    super(config, configItem);
    this.name = config.name;
    this.configNamespace = config.configNamespace || ConfigNamespace;
    this.pulsarClient = config.pulsarClient || PulsarRestClient;

    this.statePatchers.push(
      updateTopics,
      namespaceIsActive
    );
  }

  async getK8sItems(){
    const { group, version, plural } = this.configNamespace.main;
    const items = await listNamespacedCustomObjectsAllNamespaces<PulsarNamespaceInK8s>(group, version, plural);
    logger.debug(`Status Monitor Namespace: Watching ${items.length} items`);
    return items;
  }

  async getExternalItemState(item: PulsarNamespaceInK8s): Promise<Record<string, any> | null> {
    const { tenant, name: namespace } = item.spec;
    try {
      const currentState = (await this.pulsarClient.get<PulsarConfig>(
        `namespaces/${tenant}/${namespace}`
      )).data;
  
      return currentState || {};
    } catch (err) {
      logger.error('Status monitor for Namespace could not get external item state');
      return null;
    }
  }

  statusMonitorHandlesItem(item: PulsarNamespaceInK8s): boolean {
    return operatorHandlesNamespace(item.spec.tenant, item.spec.name);
  }
}