import configPulsarNamespace, { ConfigPulsarNamespace } from '@/configs/config.pulsar.namespace';
import logger from '@/log/logger.default';
import { addedOrModified } from '@/services/namespaces/service.namespace.added_or_modified';
import getNamespaceFromResourceEvent from '@/services/namespaces/misc/service.namespace.get_from_resource_event';
import getFullName from '@/services/shared/misc/service.shared.get_full_name';
import getHeadline from '@/services/shared/service.shared.get_headline';
import Operator, { ResourceEventType, ResourceMeta } from '@dot-i/k8s-operator';
import StatusMonitorNamespaceV2 from './StatusMonitorNew.Namespace';
import operatorHandlesNamespace from '@/services/namespaces/validations/service.namespace.handles_namespace';
import K8sAutoImporterNamespace from '@/classes/K8sAutoImporter.Namespace';
import configPulsarTenant from '@/configs/config.pulsar.tenant';
import { addHandledResourceGeneration, resourceGenerationWasRecentlyHandled } from '@/services/shared/k8s/service.shared.k8s.resource_was_recently_handled';
export default class OperatorPulsarNamespace extends Operator {
  
  config: ConfigPulsarNamespace;
  sm: StatusMonitorNamespaceV2;
  autoImporter: K8sAutoImporterNamespace;

  constructor(
    configNamespace = configPulsarNamespace, 
    configTenant = configPulsarTenant,
  ){
    super();
    this.config = configNamespace;    
    
    this.sm = new StatusMonitorNamespaceV2();
    this.autoImporter = new K8sAutoImporterNamespace(configNamespace, configTenant);
  }

  protected async init(): Promise<void> {
    logger.info(getHeadline('starting operator for pulsar namespaces'));
    this.watchResource(
      this.config.main.group,
      this.config.main.version,
      this.config.main.plural,
      async e => {
        const namespace = getNamespaceFromResourceEvent(e);
        if (!operatorHandlesNamespace(namespace.spec.tenant,namespace.spec.name))
          return;

        if (resourceGenerationWasRecentlyHandled(e.object.metadata)){
          logger.debug(`${getFullName(e.meta)}: namespace resource generation was already recently handled`);
          return;
        } else {
          logger.silly(`${getFullName(e.meta)}: namespace resource generation has not been handled recently`);
          addHandledResourceGeneration(e.object.metadata);
        }

        switch (e.type){
          case ResourceEventType.Added:
          case ResourceEventType.Modified:
            await addedOrModified.bind(this)(e);
            break;
          case ResourceEventType.Deleted:
            const ns = getNamespaceFromResourceEvent(e);
            logger.info(`${getFullName(e.meta)}: resource deleted`);
            break;
        }
      }
    );
    this.autoImporter.start();
  }
}

