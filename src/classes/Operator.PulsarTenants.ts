import configPulsarTenant, { ConfigPulsarTenant } from '@/configs/config.pulsar.tenant';
import logger from '@/log/logger.default';

import getFullName from '@/services/shared/misc/service.shared.get_full_name';
import Operator, { ResourceEventType } from '@dot-i/k8s-operator';
import createOrPatch from '@/services/tenant/service.operator.tenant.create_or_patch';
import deleteTenant from '@/services/tenant/service.tenant.delete';
import getResource from '@/services/shared/k8s/service.shared.k8s.get_resource.from_meta';
import { getPatchStatus } from '@/services/shared/k8s/service.shared.k8s.get_status_patcher';
import getHeadline from '@/services/shared/service.shared.get_headline';
import resourceMarkedForDeletion from '@/services/shared/k8s/service.shared.k8s.object_is_in_deletion_stage';
import ConfigNamespace, { ConfigPulsarNamespace } from '@/configs/config.pulsar.namespace';
import StatusMonitorTenantV2 from './StatusMonitorNew.Tenant';
import operatorHandlesTenant from '@/services/tenant/validations/service.tenant.validations.handles_tenant';
import { addHandledResourceGeneration, resourceGenerationWasRecentlyHandled } from '@/services/shared/k8s/service.shared.k8s.resource_was_recently_handled';
import tenantIsInErrorBecauseExistedInOtherNamespace from '@/services/tenant/conditions/service.tenant.tenant_in_error_because_other_tenant_exists';

export default class OperatorPulsarTenant extends Operator {

  config: ConfigPulsarTenant;
  configNamespace: ConfigPulsarNamespace;
  sm: StatusMonitorTenantV2;
  
  constructor(
    configTenant = configPulsarTenant, 
    configNamespace = ConfigNamespace
  ){
    super();
    this.config = configTenant;
    this.configNamespace = configNamespace;
    this.sm = new StatusMonitorTenantV2();
  }

  protected async init(): Promise<void> {
    logger.info(getHeadline('starting operator for pulsar tenants'));
    this.watchResource(
      this.config.main.group,
      this.config.main.version,
      this.config.main.plural,
      async e => {
        if (!operatorHandlesTenant(e.meta.name))
          return;

        if (resourceGenerationWasRecentlyHandled(e.object.metadata)){
          logger.debug(`${getFullName(e.meta)}: tenant resource generation was already recently handled`);
          return;
        } else {
          logger.silly(`${getFullName(e.meta)}: tenant resource generation has not been handled recently`);
          addHandledResourceGeneration(e.object.metadata);
        }
        
        
        const patchStatus = getPatchStatus.bind(this)(e.meta);
        const addedOrModified = async () => {

          if (!resourceMarkedForDeletion(e)){
            const k8sResource = await getResource(e.meta, this.config.main.plural);
            if (!k8sResource){
              logger.info(`${getFullName(e.meta)}: Tenant has already been deleted from cluster. nothing to do...`);
              return;
            }
          }

          let resourceDeleted = false;
          try {
            await this.handleResourceFinalizer(
              e, 
              'delete-child-resources', 
              async evt => {
                try {
                  if (tenantIsInErrorBecauseExistedInOtherNamespace(e)){
                    logger.info(`${getFullName(e.meta)}: deleting namespace that is existend in another namespace. no deletion from pulsar necessary`);
                    return;
                  }
                  await deleteTenant(evt, patchStatus);
                  resourceDeleted = true;
                } catch (err) {
                  logger.error(`${getFullName(e.meta)}: Could not delete resource`);
                }
                
              }
            );
  
            if (!resourceDeleted){
              await createOrPatch(e, patchStatus);
            }
          } catch (err) {
            logger.error(`${getFullName(e.meta)}: An error occured executing addedOrModified for Operator pulsar tenant`);
          }
          
        };

        switch (e.type){
          case ResourceEventType.Added:
          case ResourceEventType.Modified:
            await addedOrModified();
            break;
          case ResourceEventType.Deleted:
            logger.info(`${getFullName(e.meta)}: Resource deleted!`);
            break;
        }
      });
  }

  
}