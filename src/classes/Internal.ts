import { ResourceMeta } from '@dot-i/k8s-operator';
import { Pulsartenant } from './crd.pulsar_tenant';

export interface IncludeK8sInfo {
  k8s?: {
    namespace: string,
    meta?: ResourceMeta
  }
  
}

export interface ProbingSettings {
  maxAttempts: number,
  intervalIncrementorMs: number
}

export interface CustomResourceInfo {
  group: string,
  version: string,
  plural: string
  kind: string
}

export interface ConfigCustomResource {
  autoSync: {
    checkIntervalMs: number,
  }

  main: CustomResourceInfo
}

export interface CustomResourceMin {
  metadata: {
    namespace: string
  }
  status: {
    state?: Pulsartenant['status']['state']
    statusMessage?: string
    failedRetries?: number
    lastSpecOnError?: string
  }
}

export enum ConfigValueUpdateType {
  ADDED,
  CHANGED,
  REMOVED,
  UNCHANGED
}

export interface CustomResourceDebugConfig {
  whitelist: null | Array<string>
}

export interface NamespaceDebugConfig extends CustomResourceDebugConfig {
  ignoredPulsarSettings: Array<string>,
  ignoredIfNotSetConfigOpts: Array<string>
}