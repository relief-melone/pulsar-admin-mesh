import ConfigK8s from '@/configs/config.k8s';
import configPulsarTopic, { ConfigPulsarTopic } from '@/configs/config.pulsar.topic';
import addedOrModified from '@/services/topics/service.topic.added_or_modified';
import getHeadline from '@/services/shared/service.shared.get_headline';
import Operator, { ResourceEventType } from '@dot-i/k8s-operator';
import logger from '@/log/logger.default';
import getFullName from '@/services/shared/misc/service.shared.get_full_name';
import StatusMonitorTopicV2 from './StatusMonitorNew.Topic';
import operatorHandlesTopic from '@/services/topics/validations/service.namespace.handles_topic';
import getTopicFromResourceEvent from '@/services/topics/misc/service.topic.get_from_resource_event';
import K8sAutoImporterTopic from './K8sAutoImporter.Topic';
import { addHandledResourceGeneration, resourceGenerationWasRecentlyHandled } from '@/services/shared/k8s/service.shared.k8s.resource_was_recently_handled';


export default class OperatorPulsarTopic extends Operator {
  config: ConfigPulsarTopic;  
  sm: StatusMonitorTopicV2;
  autoImporter: K8sAutoImporterTopic;

  constructor(
    configTopic = configPulsarTopic, 
    configK8s = ConfigK8s,
  ){
    super();
    this.config = configTopic;
    this.sm = new StatusMonitorTopicV2();
    this.autoImporter = new K8sAutoImporterTopic();
  }

  protected async init(): Promise<void> {
    const { group, version, plural } = this.config.main;
    
    logger.info(getHeadline('starting operator for pulsar topics'));
    this.watchResource(
      group,
      version,
      plural,
      async e => {
        const topic = getTopicFromResourceEvent(e);
        if (!operatorHandlesTopic(topic.spec.tenant, topic.spec.namespace, topic.spec.name))
          return;
        
        if (resourceGenerationWasRecentlyHandled(e.object.metadata)){
          logger.debug(`${getFullName(e.meta)}: topic resource generation was already recently handled`);
          return;
        } else {
          logger.silly(`${getFullName(e.meta)}: topic resource generation has not been handled recently`);
          addHandledResourceGeneration(e.object.metadata);
        }

        switch (e.type){
          case ResourceEventType.Added:
          case ResourceEventType.Modified:
            await addedOrModified.bind(this)(e);
            break;
          case ResourceEventType.Deleted:
            logger.info(`${getFullName(e.meta)}: resource deleted`);
        }
      });

    this.autoImporter.start();
    

  }
}