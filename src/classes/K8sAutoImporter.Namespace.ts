import { ConfigPulsarNamespace } from '@/configs/config.pulsar.namespace';
import { ConfigPulsarTenant } from '@/configs/config.pulsar.tenant';
import getK8sObjectsAllNamespaces from '@/services/shared/k8s/service.shared.k8s.list_namespaced_object_all_namespaces';
import { PulsarNamespaceInK8s, PulsarNamespaceInternal } from './PulsarNamespace';
import { PulsarTenantInK8s } from './PulsarTenant';
import getInternalNamespace from '@/services/namespaces/misc/service.namespace.get_internal';
import getNamespaceList from '@/services/namespaces/pulsar/service.namespaces.get_for_tenant';
import getApiVersion from '@/services/shared/misc/service.shared.get_api_version_from_cri';
import getMetaName from '@/services/namespaces/misc/service.namespace.get_meta_name';
import operatorHandlesNamespace from '@/services/namespaces/validations/service.namespace.handles_namespace';
import K8sAutoImporter from './K8sAutoImporter';

export default class K8sAutoImporterNamespace extends K8sAutoImporter<PulsarNamespaceInK8s, PulsarNamespaceInternal>{
  configTenant: ConfigPulsarTenant;

  constructor(
    configPNS:ConfigPulsarNamespace,
    configTenant: ConfigPulsarTenant,
  ){
    super('namespace', configPNS);
    this.configTenant = configTenant;
  }

  async getItemsPulsar(){
    const { group, version, plural } = this.configTenant.main;
    const k8sTenants = await getK8sObjectsAllNamespaces<PulsarTenantInK8s>(group, version, plural);

    const namespacesInTenants = k8sTenants.map(async tenant => {
      const k8sNamespace = tenant.metadata?.namespace;
      const k8sTenantName = tenant.metadata?.name;
      if (!k8sNamespace)
        throw new Error('K8sAutoImporter Namespace: a tenant that was returned did not include a namespace');
      if (!k8sTenantName)
        throw new Error('K8sAutoImporter Namespace: a tenant that was returned did not include a name');

      const pulsarNamespaceList = (await getNamespaceList(k8sTenantName))
        .map(fullName => fullName.split('/').pop())
        .filter(shortName => shortName !== undefined) as Array<string>;
        
      const internalNamespaces = (await Promise.all(pulsarNamespaceList
        .map(pulsarNamespaceName => getInternalNamespace(k8sTenantName, pulsarNamespaceName, k8sNamespace))
      ))
        .filter(pnsI => pnsI !== null) as PulsarNamespaceInternal[];

      return internalNamespaces;
    });

    return (await Promise.all(namespacesInTenants)).flat();
  }

  itemsCorrelate(itemK8s: PulsarNamespaceInK8s, itemPulsar: PulsarNamespaceInternal) {
    return (
      itemK8s.spec.tenant === itemPulsar.tenant &&
      itemK8s.spec.name === itemPulsar.name 
    );
  }

  convertItemPulsarToItemK8s(itemPulsar: PulsarNamespaceInternal): PulsarNamespaceInK8s {
    const { group, kind, plural, version } = this.config.main;
    const k8sNamespace = itemPulsar.k8s?.namespace;

    if (!k8sNamespace)
      throw new Error(`AutoImporter ${this.name}: cannot convert pulsar item to k8s as namespace could not be determined`);
    

    return {
      apiVersion: getApiVersion({ group, kind, plural, version }),
      kind,
      metadata: {
        name: getMetaName(itemPulsar.tenant, itemPulsar.name),
        namespace: k8sNamespace
      },
      spec: {
        name: itemPulsar.name,
        tenant: itemPulsar.tenant,
        config: itemPulsar.config
      },
      // status is omitted as K8s creates it      
    } as PulsarNamespaceInK8s;
  }

  k8sItemWillBeHandled(item: PulsarNamespaceInK8s): boolean {
    return operatorHandlesNamespace(item.spec.tenant, item.spec.name);
  }
}