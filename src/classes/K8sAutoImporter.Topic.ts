import configPulsarNamespace, { ConfigPulsarNamespace } from '@/configs/config.pulsar.namespace';
import configPulsarTopic from '@/configs/config.pulsar.topic';
import logErr from '@/services/errors/service.errors.try_to_log_error_message';
import getResourceAllNamespaces from '@/services/shared/k8s/service.shared.k8s.list_namespaced_object_all_namespaces';
import K8sAutoImporter from './K8sAutoImporter';
import { PulsarNamespaceInK8s } from './PulsarNamespace';
import { PulsarTopicInK8s, PulsarTopicInternal } from './PulsarTopic';
import collectTopicsForNamespace from '@/services/topics/pulsar/service.topics.collect_for_namespace';
import getMetaFromKubeObject from '@/services/shared/service.shared.get_resource_meta_from_kube_object_meta';
import getMetaName from '@/services/topics/misc/service.topic.get_meta_name';
import operatorHandlesTopic from '@/services/topics/validations/service.namespace.handles_topic';
import deepmerge from 'deepmerge';



export default class K8sAutoImporterTopic extends K8sAutoImporter<PulsarTopicInK8s, PulsarTopicInternal>{
  
  configNamespace:ConfigPulsarNamespace;

  constructor(
    configTopic = configPulsarTopic,
    configNamespace = configPulsarNamespace
  ){
    super('topics', configTopic);    

    this.configNamespace = configNamespace;
  }

  async getItemsPulsar(): Promise<PulsarTopicInternal[]> {
    const { group, version, plural } = this.configNamespace.main;

    const pulsarNamespacesInK8s = await getResourceAllNamespaces<PulsarNamespaceInK8s>(
      group, version, plural
    );

    const topicsInNamespaces = pulsarNamespacesInK8s.map(async pns => {
      const k8sNamespace = pns.metadata?.namespace;
      const pnsName = pns.metadata?.name;

      if (!k8sNamespace)
        throw new Error(`K8sAutoImporter ${this.name}: pulsar namespace did not contain metadata.namespace`);
      if (!pnsName)
        throw new Error(`K8sAutoImporter ${this.name}: pulsar namespace did not contain metadata.name`);

      const pulsarTopicList = (await collectTopicsForNamespace(
        getMetaFromKubeObject(pns), pns
      ));

      if (pulsarTopicList instanceof Error)
        throw pulsarTopicList;
      
      return pulsarTopicList.map(
        topicInternal => deepmerge<PulsarTopicInternal>(
          topicInternal,
          { k8s: { namespace: k8sNamespace } }
        )
      );
    });
    
    return (await Promise.all(topicsInNamespaces)).flat();
  }
  
  itemsCorrelate(itemK8s: PulsarTopicInK8s, itemPulsar: PulsarTopicInternal): boolean {
    return (
      itemK8s.spec.name === itemPulsar.name && 
      itemK8s.spec.namespace === itemPulsar.namespace &&
      itemK8s.spec.tenant === itemPulsar.tenant
    );
  }

  convertItemPulsarToItemK8s(itemPulsar: PulsarTopicInternal): PulsarTopicInK8s {
    const { group, kind ,version } = this.config.main;
    const { tenant, namespace, name: topic, partitioned, persistent } = itemPulsar;
  
    const k8sNamespace = itemPulsar.k8s?.namespace;

    if (!k8sNamespace)
      throw new Error(`AutoImporter ${this.name}: cannot convert pulsar item to k8s as k8s namespace cannot be determined`);

    return {
      apiVersion: `${group}/${version}`,
      kind,
      metadata: {
        name: getMetaName(tenant, namespace, topic, partitioned, persistent),
        namespace: k8sNamespace
      },
      spec: {
        name: itemPulsar.name,
        namespace: itemPulsar.namespace,
        partitioned: itemPulsar.partitioned,
        partitions: itemPulsar.partitions,
        persistent: itemPulsar.persistent,
        tenant: itemPulsar.tenant,
        config: {},
        topicMeta: {}
      },
      // STATUS WILL BE SET BY STATUSMONITOR
      // status: pi.status,
    } as PulsarTopicInK8s;
  }

  k8sItemWillBeHandled(item: PulsarTopicInK8s): boolean {
    return operatorHandlesTopic(item.spec.tenant, item.spec.namespace, item.spec.name);
  }
}