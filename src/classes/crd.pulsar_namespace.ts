/* eslint-disable */
/**
 * This file was automatically generated by json-schema-to-typescript.
 * DO NOT MODIFY IT BY HAND. Instead, modify the source JSONSchema file,
 * and run json-schema-to-typescript to regenerate this file.
 */

export interface Pulsarnamespaces {
  spec: {
    name: string;
    tenant: string;
    permissions?: {
      [k: string]: unknown;
    };
    config?: {
      auth_policies?: {
        namespaceAuthentication?: {
          [k: string]: unknown;
        };
        subscriptionAuthentication?: {
          [k: string]: unknown;
        };
        topicAuthentication?: {
          [k: string]: unknown;
        };
        [k: string]: unknown;
      };
      autoSubscriptionCreationOverride?: {
        allowAutoSubscriptionCreation?: boolean;
        [k: string]: unknown;
      };
      autoTopicCreationOverride?: {
        allowAutoTopicCreation?: boolean;
        defaultNumPartitions?: number;
        topicType?: "partitioned" | "non-partitioned";
        [k: string]: unknown;
      };
      backlog_quota_map?: {
        limitSize?: number;
        limitTime?: number;
        policy?: "producer_request_hold" | "producer_exception" | "consumer_backlog_eviction";
        [k: string]: unknown;
      };
      bundles?: {
        boundaries?: string[];
        numBundles?: number;
        [k: string]: unknown;
      };
      clusterDispatchRate?: {
        [k: string]: unknown;
      };
      clusterSubscribeRate?: {
        [k: string]: unknown;
      };
      compaction_threshold?: number;
      deduplicationEnabled?: boolean;
      deduplicationSnapshotIntervalSeconds?: number;
      delayed_delivery_policies?: {
        active?: boolean;
        tickTime?: number;
        [k: string]: unknown;
      };
      deleted?: boolean;
      encryption_required?: boolean;
      entryFilters?: {
        entryFilterNames?: string;
        [k: string]: unknown;
      };
      inactive_topic_policies?: {
        deleteWhileInactive?: boolean;
        inactiveTopicDeleteMode?: "delete_when_no_subscriptions" | "delete_when_subscriptions_caught_up";
        maxInactiveDurationSeconds?: number;
        [k: string]: unknown;
      };
      is_allow_auto_update_schema?: boolean;
      latency_stats_sample_rate?: {
        [k: string]: unknown;
      };
      max_consumers_per_subscription?: number;
      max_consumers_per_topic?: number;
      max_producers_per_topic?: number;
      max_subscriptions_per_topic?: number;
      max_topics_per_namespace?: number;
      max_unacked_messages_per_consumer?: number;
      max_unacked_messages_per_subscription?: number;
      message_ttl_in_seconds?: number;
      offload_deletion_lag_ms?: number;
      offload_policies?: {
        fileSystemProfilePath?: string;
        fileSystemURI?: string;
        gcsManagedLedgerOffloadBucket?: string;
        gcsManagedLedgerOffloadMaxBlockSizeInBytes?: number;
        gcsManagedLedgerOffloadRegion?: string;
        gcsManagedLedgerOffloadServiceAccountKeyFile?: string;
        managedLedgerOffloadBucket?: string;
        managedLedgerOffloadDeletionLagInMillis?: number;
        managedLedgerOffloadDriver?: string;
        managedLedgerOffloadMaxBlockSizeInBytes?: number;
        managedLedgerOffloadMaxThreads?: number;
        managedLedgerOffloadPrefetchRounds?: number;
        managedLedgerOffloadReadBufferSizeInBytes?: number;
        managedLedgerOffloadRegion?: string;
        managedLedgerOffloadServiceEndpoint?: string;
        managedLedgerOffloadThresholdInBytes?: number;
        managedLedgerOffloadThresholdInSeconds?: number;
        managedLedgerOffloadedReadPriority?: string;
        offloadersDirectory?: string;
        s3ManagedLedgerOffloadBucket?: string;
        s3ManagedLedgerOffloadCredentialId?: string;
        s3ManagedLedgerOffloadCredentialSecret?: string;
        s3ManagedLedgerOffloadMaxBlockSizeInBytes?: number;
        s3ManagedLedgerOffloadReadBufferSizeInBytes?: number;
        s3ManagedLedgerOffloadRegion?: string;
        s3ManagedLedgerOffloadRole?: string;
        s3ManagedLedgerOffloadRoleSessionName?: string;
        s3ManagedLedgerOffloadServiceEndpoint?: string;
        [k: string]: unknown;
      };
      offload_threshold?: number;
      offload_threshold_in_seconds?: number;
      persistence?: {
        bookkeeperAckQuorum?: number;
        bookkeeperEnsemble?: number;
        bookkeeperWriteQuorum?: number;
        managedLedgerMaxMarkDeleteRate?: number;
        [k: string]: unknown;
      };
      properties?: {
        [k: string]: unknown;
      };
      publishMaxMessageRate?: {
        [k: string]: unknown;
      };
      replication_clusters?: string[];
      replicatorDispatchRate?: {
        dispatchThrottlingRateInByte?: number;
        dispatchThrottlingRateInMsg?: number;
        ratePeriodInSecond?: number;
        relativeToPublishRate?: boolean;
        [k: string]: unknown;
      };
      resource_group_name?: string;
      retention_policies?: {
        retentionSizeInMB?: number;
        retentionTimeInMinutes?: number;
        [k: string]: unknown;
      };
      schema_auto_update_compatibility_strategy?: string;
      schema_compatibility_strategy?:
        | "UNDEFINED"
        | "ALWAYS_COMPATIBLE"
        | "ALWAYS_INCOMPATIBLE"
        | "BACKWARD"
        | "BACKWARD_TRANSITIVE"
        | "FORWARD"
        | "FORWARD_TRANSITIVE"
        | "FULL"
        | "FULL_TRANSITIVE";
      schema_validation_enforced?: boolean;
      subscriptionDispatchRate?: {
        dispatchThrottlingRateInByte?: number;
        dispatchThrottlingRateInMsg?: number;
        ratePeriodInSecond?: number;
        relativeToPublishRate?: boolean;
        [k: string]: unknown;
      };
      subscription_auth_mode?: "None" | "Prefix";
      subscription_expiration_time_minutes?: number;
      subscription_types_enabled?: ("Exclusive" | "Shared" | "Failover" | "Key_Shared")[];
      topicDispatchRate?: {
        [k: string]: unknown;
      };
    };
    [k: string]: unknown;
  };
  status: {
    state: "creating" | "updating" | "active" | "terminating" | "error";
    statusMessage?: string;
    failedRetries?: number;
    lastSpecOnError?: string;
    topics: {
      count?: number;
      [k: string]: unknown;
    };
  };
}
