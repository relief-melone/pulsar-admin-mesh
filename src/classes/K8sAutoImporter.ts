import CK8s, { ConfigK8s } from '@/configs/config.k8s';
import { getService as getK8sObjectsAllNamespaces } from '@/services/shared/k8s/service.shared.k8s.list_namespaced_object_all_namespaces';
import { KubernetesObject } from '@kubernetes/client-node';
import { CustomResourceAdditions } from './CustomResourceShared';
import { ConfigCustomResource } from './Internal';
import { ConfigMinStatusMonitor } from './StatusMonitorNew';
import logger from '@/log/logger.default';
import getFullName from '@/services/shared/misc/service.shared.get_full_name';
import getResourceMeta from '@/services/shared/service.shared.get_resource_meta_from_kube_object_meta';
import logError from '@/services/errors/service.errors.try_to_log_error_message';

export type ConfigMin = ConfigMinStatusMonitor & ConfigCustomResource;
export type CustomResourceMin = KubernetesObject & CustomResourceAdditions & { [k:string]: any };
export type Operation = 'create' | 'delete';

export default abstract class K8sAutoImporter<
  K8S extends CustomResourceMin, 
  PULS extends Record<string,any>
>{
  abstract getItemsPulsar():Promise<PULS[]>;

  abstract itemsCorrelate(itemK8s: K8S, itemPulsar: PULS):boolean;

  abstract convertItemPulsarToItemK8s( itemPulsar: PULS ): K8S;

  abstract k8sItemWillBeHandled(item: K8S):boolean;

  name: string;
  config: ConfigMin;
  checkIntervalMs: number;
  
  interval: null | NodeJS.Timer;
  importIsInProgress: boolean;

  configK8s: ConfigK8s;

  constructor(name: string, config:ConfigMin, configK8s = CK8s){
    this.name = name;
    this.config = config;
    this.checkIntervalMs = config.autoSync.checkIntervalMs;
    this.interval = null;
    this.importIsInProgress = false;
    this.configK8s = configK8s;
  }

  start(){
    logger.info(`AutoImporter ${this.name}: Starting...`);
    if (this.interval)
      this.stop();

    this.interval = setInterval(async () => {
      if (this.importIsInProgress){
        logger.warn('AutoImporter: Interval is still running. skipping update cycle. If this warning persists increase autoSync interval');
        return;
      }
      this.importIsInProgress = true;
      await this.runnerFunc();
      this.importIsInProgress = false;
    }, this.checkIntervalMs);

    logger.info(`AutoImporter ${this.name}: Started`);

  }
  stop(){
    logger.info(`AutoImporter ${this.name}: Stopping...`);
    if (this.interval){
      clearInterval(this.interval);
      this.interval = null;
    }
      
    logger.info(`AutoImporter ${this.name}: Stopped`);

  }

  async runnerFunc(){
    const start = Date.now();
    let itemsK8s: Array<K8S>;
    let itemsPulsar: Array<PULS>;
    try {
      
      [ itemsK8s, itemsPulsar ] 
      = await Promise.all([ 
          await this.getItemsK8s(),
          await this.getItemsPulsar()
        ]);

    } catch (err){
      logger.error('K8sAutoImporter. Error retrieving k8s or pulsar items. Will skip import cycle');
      logError(err);
      return;
    }

    let exclusiveInK8s:K8S[];
    let exclusiveInPulsar:K8S[];
    try {
      exclusiveInK8s = this.getItemsInK8sButNotInPulsar(itemsK8s, itemsPulsar);
      exclusiveInPulsar = this.getItemsInPulsarButNotInK8s(itemsK8s, itemsPulsar);
    } catch (err) {
      logger.error(err);
      return;
    }
    

    const operationsCreateInK8s:Array<Promise<Error|void>> = exclusiveInPulsar
      .filter(this.handleTreatment.bind(this))
      .map(this.deleteOrCreateInK8s('create').bind(this));

    const operationsDeleteFromK8s:Array<Promise<Error|void>> = exclusiveInK8s
      .filter(this.handleTreatment.bind(this))
      .filter(item => item.status?.state !== 'creating')
      .map(this.deleteOrCreateInK8s('delete').bind(this));
    
    this.logOperationErrors('create', await Promise.all(operationsCreateInK8s));
    this.logOperationErrors('delete', await Promise.all(operationsDeleteFromK8s));

    logger.debug(`K8sAutoImporter ${this.name}: Total runtime was ${Date.now() - start} ms`);

  }

  async getItemsK8s():Promise<Array<K8S>>{
    const { group, plural, version } = this.config.main;
    const k8sItems = await getK8sObjectsAllNamespaces(
      this.configK8s.apiCustom
    )<K8S>(group, version, plural);
    return k8sItems;
  }

  logOperationErrors(opType: Operation, ops: Array<void|Error>){    
    const errors = ops.filter(op => op instanceof Error) as Array<Error>;
    const message = `AutoImporter: ${this.name}: ${ops.length - errors.length}/${ops.length} ${opType === 'create' ? 'creation' : 'deletion' } operations successfull`;
    
    ops.length > 0 || errors.length > 0
      ? logger.info(message)
      : logger.debug(message);
    
    if (errors.length > 0){
      logger.error(`AutoImporter: ${this.name}: The following errors occured`);
      errors.forEach(err => {
        logger.error(err);
      });
    }
  }

  deleteOrCreateInK8s(op: Operation){
    return async (item: K8S) => {
      const { group, version, plural } = this.config.main;
      const k8sNamespace = item.metadata?.namespace;
      const k8sName = item.metadata?.name;
      
      if (!k8sNamespace)        
        return (new Error(`K8sAutoimporter: cannot ${op} ${getFullName(item)} as metadata.namespace not present in object`));
      if (!k8sName)
        return new Error(`K8sAutoimporter: cannot ${op} ${getFullName(item)} as metadata.name not present in object`);
  
      try {
        op === 'create'
          ? await this.configK8s.apiCustom.createNamespacedCustomObject(group, version, k8sNamespace, plural, item)
          : await this.configK8s.apiCustom.deleteNamespacedCustomObject(group, version, k8sNamespace, plural, k8sName);
      } catch (err:any) {
        logError(err);
        return err instanceof Error
          ? err
          : new Error(err);
      }
    };
  }

  handleTreatment(item: K8S):boolean{
    const treated = this.k8sItemWillBeHandled(item);
    logger.debug(`${getFullName(getResourceMeta(item))}: item will ${treated ? '': 'not' }be treated`);
    return treated;
  }

  getItemsInK8sButNotInPulsar(itemsK8s:K8S[], itemsPulsar:PULS[]):K8S[]{
    return itemsK8s
      .filter(k8si => !itemsPulsar.some(pi =>
        this.itemsCorrelate(k8si, pi)
      ));
  }

  getItemsInPulsarButNotInK8s( itemsK8s: K8S[], itemsPulsar:PULS[]):K8S[]{
    return itemsPulsar
      .filter(pi => !itemsK8s.some(k8si => 
        this.itemsCorrelate(k8si,pi)
      ))
      .map(pi => this.convertItemPulsarToItemK8s(pi));
  }
}