export type State = 'creating' | 'updating' | 'active' | 'terminating' | 'error';

export interface StatusPatch {
  state: State;
  statusMessage: string
}

export interface CustomResourceAdditions {
  status: {
    state: State
  }
}