import { ResourceMeta } from '@dot-i/k8s-operator';
import { KubernetesObject } from '@kubernetes/client-node';
import { Pulsartenant } from './crd.pulsar_tenant';
import { IncludeK8sInfo } from './Internal';

export type PulsarTenantInK8s = Pulsartenant & KubernetesObject;

export interface PulsarTenantInternalInput extends IncludeK8sInfo {
  name: string;
  config: PulsarTenantInPulsar;
}


export class PulsarTenantInternal implements PulsarTenantInternalInput, IncludeK8sInfo {
  name: string;
  config: PulsarTenantInPulsar;
  k8s?: { 
    namespace: string;
    meta?: ResourceMeta
  };

  constructor(input: PulsarTenantInternalInput){
    this.config = input.config;
    this.k8s = input.k8s;
    this.name = input.name;
  }
}

// Refine later
export interface PulsarTenantInPulsar {
  adminRoles: Array<string>
  allowedClusters: Array<string>
}