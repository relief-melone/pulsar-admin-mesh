
import { KubernetesObject } from '@kubernetes/client-node';
import deepmerge from 'deepmerge';
import deepequal from 'fast-deep-equal';
import getResourceMeta from '@/services/shared/service.shared.get_resource_meta_from_kube_object_meta';
import logger from '@/log/logger.default';
import getFullName from '@/services/shared/misc/service.shared.get_full_name';
import logError from '@/services/errors/service.errors.try_to_log_error_message';
import { PulsarConfig } from './PulsarNamespace';
import configDebug from '@/configs/config.debug';
import { sizeFormatter } from 'human-readable';
import sizeOf from 'object-sizeof';
import { ConfigCustomResource, ProbingSettings } from './Internal';
import { ConfigMin } from './K8sAutoImporter';
import patchCustomResource from '@/services/shared/k8s/service.shared.k8s.patch_custom_resource';
import resourceHasTimedOutCreating from '@/services/shared/k8s/service.shared.k8s.resource_timed_out_in_creation';
import CK8s, { ConfigK8s } from '@/configs/config.k8s';
import { getNamespace } from '@/services/shared/service.shared.get_namespace';

export interface ConfigMinStatusMonitor extends ConfigCustomResource {
  statusMonitor: {
    checkIntervalMs: number,
    creationTimeoutInSeconds: number
  };
  
  probing: {
    readiness: ProbingSettings,
    deletion: ProbingSettings,
  }
}

export interface MonitoredItemMin extends KubernetesObject{
  [k: string]: any | undefined
  spec: Record<string,any>
  status: {
    [k: string]: any
    state: 'active' | 'creating' | 'updating' | 'error' | 'terminating',
    failedRetries?: number
    lastSpecOnError?: string | null
    statusMessage?: string
  }
}
export type ItemStatePulsar = Record<string, any>;
export type NamespaceConfig = Exclude<PulsarConfig,undefined>;

export type StatusPatchGetter<
  PULSSTAT extends ItemStatePulsar,
  ITEM extends MonitoredItemMin
> = (initStat:PULSSTAT, currentItem: ITEM) => Promise<Partial<ITEM['status']>>; 

export interface StatusMonitorV2Config {
  intervalMs: number
  name: string
}

export class ErrorStatusMontitor extends Error {
  constructor(msg: string){
    super(msg);
  }
}

export default abstract class StatusMonitorV2<
  PULSSTAT extends ItemStatePulsar,
  ITEM extends MonitoredItemMin, 
> {
  name: string;
  itemConfig: ConfigMin;
  statePatchers: Array<StatusPatchGetter<PULSSTAT, ITEM>>;
  configK8s: ConfigK8s;

  intervalMs: number;
  intervalIsRunning: boolean;
  
  abstract getK8sItems():Promise<Array<ITEM>>;
  abstract getExternalItemState(item: ITEM):Promise<PULSSTAT|null>;
  abstract statusMonitorHandlesItem(item:ITEM):boolean;

  constructor(
    config: StatusMonitorV2Config, 
    itemConfig: ConfigMin,
    configK8s = CK8s
  ){
    this.name = config.name || 'n/a';
    this.itemConfig = itemConfig;
    this.statePatchers = [];
    this.configK8s = configK8s;

    this.intervalMs = config.intervalMs;
    setInterval(this.intervalRunner.bind(this), this.intervalMs);
    this.intervalIsRunning = false;

    if (configDebug.memoryReports.enabled)
      setInterval(() => {        
        logger.info(`${this.name}:${' '.repeat(30-this.name.length)}${sizeFormatter()(sizeOf(this))}`);
      }, configDebug.memoryReports.intervalMs);
  }

  // TODO: needed for testing. intervalRunner does not get picked up by spy. Needs to be solved then intervalRunning can be set directly
  setIntervalRunning(isRunning:boolean){
    this.intervalIsRunning = isRunning;
  }

  async intervalRunner(){
    if (this.intervalIsRunning){
      logger.warn(`StatusMonitor: Interval for ${this.name} is still running. skipping update cycle. If this warning persists maybe increase the interval time`);
      return;
    }
    this.setIntervalRunning(true);

    logger.debug(`${this.name}: Running status monitor...`);
    const k8sItems = await this.getK8sItems();
    const operations = k8sItems
      .filter(item => this.statusMonitorHandlesItem(item))
      .map(async item => {
        const currentStatus = item.status;
        const meta = getResourceMeta(item);

        if (resourceHasTimedOutCreating(item, this.itemConfig)){
          logger.error(`${getFullName(meta)}: resource has timed out waiting to be created in pulsar. deleting...`);
          const result = await this.deleteItem(item);

          if (result instanceof Error){
            logger.error(`${getFullName(meta)}: could not delete timed out k8s resource`);
            logger.error(result);
          } else 
            logger.warn(`${getFullName(meta)}: successfully deleted timed out resource`);

          return;
        }

        const stateExternal = await this.getExternalItemState(item);
        if (!stateExternal){
          logger.warn(`${getFullName(meta)}: could not get external item state. skipping status patch`);
          return;
        }
        const patches = await Promise.all(
          this.statePatchers.map(sp => sp(stateExternal, item))
        );

        const finalPatch = deepmerge.all(
          patches, 
          { arrayMerge: (_t,source,_o) => source }
        );

        const newStatus = deepmerge(
          currentStatus, finalPatch,
          { arrayMerge: (_t, source, _o) => source }
        );
        
        if (!deepequal(currentStatus, newStatus)){
          logger.debug(`${getFullName(meta)}: namespace status monitor wil update the status to ${newStatus}`);
          try {
            await this.patchStatus(
              newStatus, 
              item.metadata, 
              getFullName(getResourceMeta(item))
            );
          } catch (err) {
            logError(err);
          }
        } else {
          logger.debug(`${getFullName(item)}: state would not change. not updating`);
        }

        logger.debug(`${this.name}: status monitor run complete`);

      });

    try {
      await Promise.all(operations);
    } catch (err) {
      logError(err);
    } finally {
      this.setIntervalRunning(false);
      logger.debug(`${this.name}: interval runner completed`);
    }
    
  }

  async patchStatus(
    status: Partial<ITEM['status']>,
    meta: ITEM['metadata'],
    resourceName: string
  ):Promise<void | Error> {
    const result = await patchCustomResource(
      status, meta, this.itemConfig.main
    );

    if (result instanceof Error)
      return new Error(`Status Monitor ${this.name}: error updating status for ${resourceName}`);
  }

  async deleteItem(item:ITEM):Promise<void|Error>{
    const { group, version, plural } = this.itemConfig.main;
    const { apiCustom } = this.configK8s;
    const meta = getResourceMeta(item);

    try {
      await apiCustom.deleteNamespacedCustomObject(
        group, version, getNamespace(meta), plural, meta.name
      );
    } catch (err) {
      logger.error(`Status Monitor ${this.name}: could not delete item`);
    }
  }
}