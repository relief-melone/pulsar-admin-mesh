import winston, { format, transports } from 'winston';
import configLogger from '../configs/config.logger';

const getColorCode = (level: string):string => {
  let colorCode = '';
  switch (level.toUpperCase()){
    case 'INFO':
      colorCode = '\x1b[32m';
      break;
    case 'WARN':
      colorCode = '\x1b[33m';
      break;
    case 'ERROR':
      colorCode = '\x1b[31m';
      break;

    default:
      colorCode = '\x1b[0m';
      break;
  }
  return colorCode;    
}; 

const defaultFormat = format.printf(({ level, message, label, timestamp }) => 
  `[ ${getColorCode(level)}${level.toUpperCase()} \x1b[0m]\t[ ${timestamp} ]\t ${message} `
);

export const getLogger = (config = configLogger) => {
  const logger = winston.createLogger({
    level: config.logLevel,
    format: format.combine(
      format.timestamp(),
      format.simple(),
      defaultFormat,
      
    ),
    transports: [
      new transports.Console({
        
      })
    ],
  
    


  });

  logger.info('Logger successfully initialized');
  logger.info(`LogLevel: ${config.logLevel}`);


  return logger;
};

const logger = getLogger();


export default logger;