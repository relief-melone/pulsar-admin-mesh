BASEDIR=$(dirname $0)
BUILD_DIR=$BASEDIR/../build

mkdir $BASEDIR/../.ci/helm/.crds

node $BASEDIR/../dist/_build_crds.js

cat $BASEDIR/../schemas/schema.crd.pulsar_tenant.yaml \
  | $BUILD_DIR/yq_linux_amd64 '.spec.versions[0].schema.openAPIV3Schema' -o json \
  | npx json-schema-to-typescript \
  > $BASEDIR/../src/classes/crd.pulsar_tenant.ts

cat $BASEDIR/../schemas/schema.crd.pulsar_namespace.yaml \
  | $BUILD_DIR/yq_linux_amd64 '.spec.versions[0].schema.openAPIV3Schema' -o json \
  | npx json-schema-to-typescript \
  > $BASEDIR/../src/classes/crd.pulsar_namespace.ts

cat $BASEDIR/../schemas/schema.crd.pulsar_topic.yaml \
  | $BUILD_DIR/yq_linux_amd64 '.spec.versions[0].schema.openAPIV3Schema' -o json \
  | npx json-schema-to-typescript \
  > $BASEDIR/../src/classes/crd.pulsar_topic.ts