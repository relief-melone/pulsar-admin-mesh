#!/bin/sh

BASEDIR=$(dirname $0)
echo "Starting Script: $RUN_SCRIPT"

echo \
"""
=========NODE.JS==========
    NODE: $(node --version)
    NPM:  $(npm --version)
    ENV:  ${NODE_ENV:=production}
==========================
"""

if [ "$NODE_ENV" = "development" ]; then
  npm run dev
else
  npm run start
fi