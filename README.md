<div style="display: flex; width: 100%; justify-content: center"><img src="media/logo.png" height="400px" style=""></img></div>

# PULSAR ADMIN MESH

Pulsar Admin Mesh (or PAM) is a tool that lets you manage your pulsar cluster resources such as tenants, namespaces and topics as native kubernetes objects. 

This makes it easy in a kubernetes environment to automate your pulsar configuration. With pam you just tell kubernetes what your tenants namespaces and topic on pulsar should look like and pam takes care of the rest.

creating a tenanant for example is as easy as.

```sh
kubectl -f my-tenant.yaml
```

my-tenant.yaml
```yaml
apiVersion: admin.pulsar.io/v1alpha1
kind: PulsarTenant
metadata:
  name: my-new-tenant
spec:
  config:
    adminRoles:
    - special_user
    allowedClusters:
    - pulsar
```

Extensive documentation will be added to this repository over time. However if you want to know what options are available look over at the [Pulsar Rest API Documentation](https://pulsar.apache.org/admin-rest-api). The options that are available on the respective create endpoints are also available in the custom resources config section.

Pulsar Admin Mesh also watches your pulsar cluster and automatically imports namespaces and topics to k8s as long as you have applied the corresponding tenant (tenants do not get autoimported)

## Installation

Pulsar Admin Mesh comes with a helm chart. So given you have pulsar with the default values and the pulsar broker is reachable from within the cluster under pulsar-broker.pulsar.svc no config is needed. To install use

```sh
helm install pam -n pam --create-namespace .ci/helm
```

### Helm
You can check the values.yaml for configuration until documentation gets updated here. However the most important settings should be explained here

installCRDs: sets wether or not you want to install **the** custom resource definitions with your release. Is set to true by default but we recommend installing the CRDs separate from the release for production use

config.service.apiEndpoint: the endpoint that pam will use to communicate with pulsars restApi. By default its set to http://pulsar-broker.pulsar.svc:8080/admin/v2

| variable                                                | description                                    | default value                                       |
| ------------------------------------------------------- | ---------------------------------------------- | --------------------------------------------------- |
| installCRDs                                             | install CustomResourceDefinitions with release | true                                                |
| config.clusterName                                      | name of the pulsar cluster                     | pulsar                                              |
| config.service.apiEndpoint                              | web endpoint of pulsar broker serving rest api | http://pulsar-broker.pulsar.svc                     |
| config.tenant.statusMonitor.checkIntervalMs             | interval to update tenant stats                | 3000                                                |
| config.tenant.statusMonitor.creationTimeoutInSeconds    | timeout after which to give up creating tenant | 120                                                 |
| config.tenant.autoSync.checkIntervalMs                  | interval for resource change check with pulsar | 2000                                                |
| config.namespace.statusMonitor.checkIntervalMs          | same option as with tenants but for namespaces | 3000                                                |
| config.namespace.statusMonitor.creationTimeoutInSeconds | see tenant                                     | 120                                                 |
| config.namespace.autoSync.checkIntervalMs               | see tenant                                     | 2000                                                |
| config.topic.statusMonitor.checkIntervalMs              | same option as with tenants but for namespaces | 3000                                                |
| config.topic.statusMonitor.creationTimeoutInSeconds     | see tenant                                     | 120                                                 |
| config.topic.autoSync.checkIntervalMs                   | see tenant                                     | 2000                                                |
| image.repository                                        | repository for docker image                    | registry.gitlab.com/relief-melone/pulsar-admin-mesh |
| image.pullPolicy                                        | pull policy for image                          | IfNotPresent                                        |
| image.tag                                               | tag for image                                  | -                                                   |


## Custom Resources

### Tenant
To create a basic tenant use
```yaml
apiVersion: admin.pulsar.io/v1alpha1
kind: PulsarTenant
metadata:
  name: my-tenant
spec:
  config:
    adminRoles: []
    allowedClusters:
    - pulsar
```

### Namespace

To create a basic pulsar namespace

```yaml
apiVersion: admin.pulsar.io/v1alpha1
kind: PulsarNamespace
metadata:
  name: namespace-1
spec:
  name: namespace-1
  tenant: my-tenant
  config:
    message_ttl_in_seconds: 20
```

### Under development
The project is currently under development. So certain functionalities like pam being able to autoscale and working with pulsar clusters that require authentication has not yet been implemented but is a work in progress.