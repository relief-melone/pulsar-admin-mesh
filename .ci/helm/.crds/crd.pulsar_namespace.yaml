apiVersion: apiextensions.k8s.io/v1
kind: CustomResourceDefinition
metadata:
  name: pulsarnamespaces.admin.pulsar.io
spec:
  group: admin.pulsar.io
  names:
    plural: pulsarnamespaces
    singular: pulsarnamespace
    kind: PulsarNamespace
    shortNames:
      - pns
      - pn
      - pnamespaces
      - pnamespace
  scope: Namespaced
  versions:
    - name: v1alpha1
      served: true
      storage: true
      subresources:
        status: {}
      schema:
        openAPIV3Schema:
          title: pulsarnamespaces
          type: object
          properties:
            spec:
              type: object
              required:
                - name
                - tenant
              properties:
                name:
                  type: string
                tenant:
                  type: string
                permissions:
                  type: object
                  additionalProperties: true
                config:
                  type: object
                  properties:
                    auth_policies:
                      type: object
                      properties:
                        namespaceAuthentication:
                          type: object
                        subscriptionAuthentication:
                          type: object
                        topicAuthentication:
                          type: object
                    autoSubscriptionCreationOverride:
                      type: object
                      properties:
                        allowAutoSubscriptionCreation:
                          type: boolean
                    autoTopicCreationOverride:
                      type: object
                      properties:
                        allowAutoTopicCreation:
                          type: boolean
                        defaultNumPartitions:
                          type: number
                        topicType:
                          type: string
                          enum:
                            - partitioned
                            - non-partitioned
                    backlog_quota_map:
                      type: object
                      properties:
                        limitSize:
                          type: number
                        limitTime:
                          type: number
                        policy:
                          type: string
                          enum:
                            - producer_request_hold
                            - producer_exception
                            - consumer_backlog_eviction
                    bundles:
                      type: object
                      properties:
                        boundaries:
                          type: array
                          items:
                            type: string
                        numBundles:
                          type: number
                    clusterDispatchRate:
                      type: object
                    clusterSubscribeRate:
                      type: object
                    compaction_threshold:
                      type: number
                    deduplicationEnabled:
                      type: boolean
                    deduplicationSnapshotIntervalSeconds:
                      type: number
                    delayed_delivery_policies:
                      type: object
                      properties:
                        active:
                          type: boolean
                        tickTime:
                          type: number
                    deleted:
                      type: boolean
                    encryption_required:
                      type: boolean
                    entryFilters:
                      type: object
                      properties:
                        entryFilterNames:
                          type: string
                    inactive_topic_policies:
                      type: object
                      properties:
                        deleteWhileInactive:
                          type: boolean
                        inactiveTopicDeleteMode:
                          type: string
                          enum:
                            - delete_when_no_subscriptions
                            - delete_when_subscriptions_caught_up
                        maxInactiveDurationSeconds:
                          type: number
                    is_allow_auto_update_schema:
                      type: boolean
                    latency_stats_sample_rate:
                      type: object
                    max_consumers_per_subscription:
                      type: number
                    max_consumers_per_topic:
                      type: number
                    max_producers_per_topic:
                      type: number
                    max_subscriptions_per_topic:
                      type: number
                    max_topics_per_namespace:
                      type: number
                    max_unacked_messages_per_consumer:
                      type: number
                    max_unacked_messages_per_subscription:
                      type: number
                    message_ttl_in_seconds:
                      type: number
                    offload_deletion_lag_ms:
                      type: number
                    offload_policies:
                      type: object
                      properties:
                        fileSystemProfilePath:
                          type: string
                        fileSystemURI:
                          type: string
                        gcsManagedLedgerOffloadBucket:
                          type: string
                        gcsManagedLedgerOffloadMaxBlockSizeInBytes:
                          type: number
                        gcsManagedLedgerOffloadRegion:
                          type: string
                        gcsManagedLedgerOffloadServiceAccountKeyFile:
                          type: string
                        managedLedgerOffloadBucket:
                          type: string
                        managedLedgerOffloadDeletionLagInMillis:
                          type: number
                        managedLedgerOffloadDriver:
                          type: string
                        managedLedgerOffloadMaxBlockSizeInBytes:
                          type: number
                        managedLedgerOffloadMaxThreads:
                          type: number
                        managedLedgerOffloadPrefetchRounds:
                          type: number
                        managedLedgerOffloadReadBufferSizeInBytes:
                          type: number
                        managedLedgerOffloadRegion:
                          type: string
                        managedLedgerOffloadServiceEndpoint:
                          type: string
                        managedLedgerOffloadThresholdInBytes:
                          type: number
                        managedLedgerOffloadThresholdInSeconds:
                          type: number
                        managedLedgerOffloadedReadPriority:
                          type: string
                        offloadersDirectory:
                          type: string
                        s3ManagedLedgerOffloadBucket:
                          type: string
                        s3ManagedLedgerOffloadCredentialId:
                          type: string
                        s3ManagedLedgerOffloadCredentialSecret:
                          type: string
                        s3ManagedLedgerOffloadMaxBlockSizeInBytes:
                          type: number
                        s3ManagedLedgerOffloadReadBufferSizeInBytes:
                          type: number
                        s3ManagedLedgerOffloadRegion:
                          type: string
                        s3ManagedLedgerOffloadRole:
                          type: string
                        s3ManagedLedgerOffloadRoleSessionName:
                          type: string
                        s3ManagedLedgerOffloadServiceEndpoint:
                          type: string
                    offload_threshold:
                      type: number
                    offload_threshold_in_seconds:
                      type: number
                    persistence:
                      type: object
                      properties:
                        bookkeeperAckQuorum:
                          type: number
                        bookkeeperEnsemble:
                          type: number
                        bookkeeperWriteQuorum:
                          type: number
                        managedLedgerMaxMarkDeleteRate:
                          type: number
                    properties:
                      type: object
                      additionalProperties: true
                      properties: {}
                    publishMaxMessageRate:
                      type: object
                    replication_clusters:
                      type: array
                      items:
                        type: string
                    replicatorDispatchRate:
                      type: object
                      properties:
                        dispatchThrottlingRateInByte:
                          type: number
                        dispatchThrottlingRateInMsg:
                          type: number
                        ratePeriodInSecond:
                          type: number
                        relativeToPublishRate:
                          type: boolean
                    resource_group_name:
                      type: string
                    retention_policies:
                      type: object
                      properties:
                        retentionSizeInMB:
                          type: number
                        retentionTimeInMinutes:
                          type: number
                    schema_auto_update_compatibility_strategy:
                      type: string
                    schema_compatibility_strategy:
                      type: string
                      enum:
                        - UNDEFINED
                        - ALWAYS_COMPATIBLE
                        - ALWAYS_INCOMPATIBLE
                        - BACKWARD
                        - BACKWARD_TRANSITIVE
                        - FORWARD
                        - FORWARD_TRANSITIVE
                        - FULL
                        - FULL_TRANSITIVE
                    schema_validation_enforced:
                      type: boolean
                    subscriptionDispatchRate:
                      type: object
                      properties:
                        dispatchThrottlingRateInByte:
                          type: number
                        dispatchThrottlingRateInMsg:
                          type: number
                        ratePeriodInSecond:
                          type: number
                        relativeToPublishRate:
                          type: boolean
                    subscription_auth_mode:
                      type: string
                      enum:
                        - None
                        - Prefix
                    subscription_expiration_time_minutes:
                      type: number
                    subscription_types_enabled:
                      type: array
                      items:
                        type: string
                        enum:
                          - Exclusive
                          - Shared
                          - Failover
                          - Key_Shared
                    topicDispatchRate:
                      type: object
            status:
              type: object
              properties:
                state:
                  type: string
                  enum:
                    - creating
                    - updating
                    - active
                    - terminating
                    - error
                statusMessage:
                  type: string
                failedRetries:
                  type: number
                lastSpecOnError:
                  type: string
                topics:
                  type: object
                  properties:
                    count:
                      type: number
              default:
                state: creating
                statusMessage: ""
                failedRetries: 0
                topics:
                  count: 0
      additionalPrinterColumns:
        - name: ns-name
          type: string
          jsonPath: .spec.name
        - name: tenant
          type: string
          jsonPath: .spec.tenant
        - name: topics
          type: number
          jsonPath: .status.topics.count
        - name: state
          type: string
          jsonPath: .status.state
