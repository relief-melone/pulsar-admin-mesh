import StatusMonitorV2, { ConfigMinStatusMonitor, ItemStatePulsar, MonitoredItemMin, StatusPatchGetter } from '@/classes/StatusMonitorNew';
import logger from '@/log/logger.default';
import { expect } from 'chai';
import sinon, { SinonFakeTimers, SinonSpy, SinonStub } from 'sinon';
import { getItemStatePulsar } from '../helpers/getItemStatePulsar';
import { getMonitoredItemMin } from '../helpers/getMonitoredItemMin';
import silenceLogger from '../helpers/silenceLogger';
import wait from '../helpers/wait';
import getConfigMin from '../helpers/getConfigMin';
import getResourceName from '@/services/shared/k8s/service.shared.k8s.get_resource_name';
import getConfigK8sStub from '../helpers/getConfigK8sStub';
import { ConfigMin } from '@/classes/K8sAutoImporter';
import { DeepPartial } from '@/classes/Helpers';

type TestPulsarStat = ItemStatePulsar;
type TestItem = MonitoredItemMin;

class TestMonitor extends StatusMonitorV2<TestPulsarStat,TestItem> {

  getK8sStub: typeof this['getK8sItems'];
  getExternalItemStateStub: typeof this['getExternalItemState'];
  statusMonitorHandlesItemStub: SinonStub<any, ReturnType<typeof this['statusMonitorHandlesItem']>>;

  constructor(
    getK8s,
    getExternalItemState,
    statusMonitorHandlesItem,
    intervalMs,
    configPatch:DeepPartial<ConfigMin> = {}
  ){
    super(
      {
        intervalMs,
        name: 'Test Status Monitor'
      }, 
      getConfigMin(configPatch),
      getConfigK8sStub()
    );

    this.getK8sStub = getK8s;
    this.getExternalItemStateStub = getExternalItemState;
    sinon.stub(this, 'patchStatus');
    this.statusMonitorHandlesItemStub = statusMonitorHandlesItem;
  }


  getK8sItems(): Promise<TestItem[]>{
    return this.getK8sStub();
  }
  getExternalItemState(item: TestItem): Promise<ItemStatePulsar | null> {
    return this.getExternalItemStateStub(item);
  }
  statusMonitorHandlesItem(item: TestItem): boolean {
    return this.statusMonitorHandlesItemStub(item);
  }
}

describe('class.StatusMonitor', () => {

  silenceLogger();

  let getK8sStub ;
  let getExternalItemStateStub;
  let statusMonitorHandlesItemStub;

  let clock:SinonFakeTimers;
  const sandbox = sinon.createSandbox();

  beforeEach(() => {
    getK8sStub = sinon.stub();
    getExternalItemStateStub = sinon.stub();
    statusMonitorHandlesItemStub = sinon.stub();

    clock = sinon.useFakeTimers();

    sandbox.spy(logger);
  });

  afterEach(() => {
    clock.restore();

    sandbox.restore();
  });

  it('should start the according interval runner once', async () => {
    // Prepare
    const sm = new TestMonitor(
      getK8sStub, getExternalItemStateStub, statusMonitorHandlesItemStub, 50
    );    
    statusMonitorHandlesItemStub.returns(true);
    getK8sStub.resolves([getMonitoredItemMin()]);
    const spy = sinon.spy(sm, 'setIntervalRunning');

    // Execute
    await clock.tickAsync(10);
    await clock.nextAsync();

    // Assert
    sinon.assert.callCount(spy, 2);
    expect(spy.firstCall.args[0]).to.equal(true);
    expect(spy.secondCall.args[0]).to.equal(false);
    
  });

  it('interval runner should have wun twice after intervalMs is surpassed', async () => {
    // Prepare
    const sm = new TestMonitor(
      getK8sStub, getExternalItemStateStub, statusMonitorHandlesItemStub, 50
    );    
    statusMonitorHandlesItemStub.returns(true);
    getK8sStub.resolves([getMonitoredItemMin()]);
    const spy = sinon.spy(sm, 'setIntervalRunning');

    // Execute
    await clock.tickAsync(60);
    await clock.nextAsync();

    // Assert
    sinon.assert.callCount(spy, 4);
    expect(spy
      .getCalls()
      .map(call => call.args[0])
    ).to.deep
      .equal([true,false,true,false]);

  });

  it('will skip to run the status monitor if another interval is still ongoing. called once if resolving takes longer than interval', async () => {
    // Prepare
    getK8sStub.resolves((async () => {
      await wait(60);
      return [ getMonitoredItemMin() ];
    })());

    const sm = new TestMonitor(
      getK8sStub, getExternalItemStateStub, statusMonitorHandlesItemStub, 50
    );
    const spy = sinon.spy(sm, 'setIntervalRunning');
    statusMonitorHandlesItemStub.returns(true);

    // Execute
    await clock.tickAsync(40);
    await clock.nextAsync();

    // Assert    
    sinon.assert.calledOnceWithExactly(spy, true);
  });

  it('will skip to run the status monitor if another interval is still ongoing. called twice if one cycle is skipped but first one finishes before tick', async () => {
    // Prepare
    getK8sStub.resolves((async () => {
      await wait(39);
      return [ getMonitoredItemMin() ];
    })());

    const sm = new TestMonitor(
      getK8sStub, getExternalItemStateStub, statusMonitorHandlesItemStub, 50
    );
    
    const spy = sinon.spy(sm, 'setIntervalRunning');
    statusMonitorHandlesItemStub.returns(true);
    
    // Execute
    await clock.tickAsync(40);
    await clock.nextAsync();

    // Assert    
    sinon.assert.calledTwice(spy);
    expect(spy
      .getCalls()
      .map(call => call.args[0])
    ).to.deep
      .equal([true,false]);
  });

  it('will apply all state patchers with the external state', async () => {
    // Prepare
    const itemState = getItemStatePulsar();
    const sp1 = sinon.stub().resolves((async () => {
      await wait(20);
      return { sp1: 'ran' };
    })());
    const sp2 = sinon.stub().resolves((async () => {
      await wait(50);
      return { sp2: 'ran' };
    })());

    getK8sStub.resolves([ getMonitoredItemMin() ]);
    getExternalItemStateStub.resolves( itemState );
    

    const sm = new TestMonitor(
      getK8sStub, getExternalItemStateStub, statusMonitorHandlesItemStub, 2000
    );
    const patchStatusStub = sm.patchStatus as SinonStub;
    sinon.spy(sm, 'setIntervalRunning');
    statusMonitorHandlesItemStub.returns(true);
    sm.statePatchers.push(sp1,sp2);
    
    // Execute
    await clock.tickAsync(51);
    await clock.nextAsync();


    // Assert
    sinon.assert.calledOnceWithMatch(sp1, itemState, getMonitoredItemMin());
    sinon.assert.calledOnceWithMatch(sp2, itemState, getMonitoredItemMin());
    sinon.assert.alwaysCalledWithMatch(
      patchStatusStub, 
      {
        ...getMonitoredItemMin().status,
        sp1: 'ran',
        sp2: 'ran'
      }, 
      getMonitoredItemMin().metadata,
      getResourceName(getMonitoredItemMin())
    );
  });

  it('will apply update only changed contents', async () => {
    // Prepare
    const itemStatus = getItemStatePulsar();
    const item = getMonitoredItemMin({ status: { nested: { property: 'hello', bing: 'baz' } } });
    
    const sp1 = sinon.stub().resolves((async () => {
      await wait(20);
      return { nested: { bing: 'pong' } };
    })());    

    getK8sStub.resolves([ item ]);
    getExternalItemStateStub.resolves( itemStatus );
    
    const sm = new TestMonitor(
      getK8sStub, getExternalItemStateStub, statusMonitorHandlesItemStub, 2000, {}
    );
    const patchStatusStub = sm.patchStatus as SinonStub;
    patchStatusStub.resolves();
    sm.statePatchers.push(sp1);
    const spy = sinon.spy(sm, 'setIntervalRunning');
    statusMonitorHandlesItemStub.returns(true);
    
    // Execute
    await clock.tickAsync(51);
    await clock.nextAsync();


    // Assert
    sinon.assert.calledOnceWithMatch(
      patchStatusStub,
      {
        nested: {
          property: 'hello',
          bing: 'pong'
        }
      },
      item.metadata,
      getResourceName(item)
    );
  });

  it('will not call deleteItem for k8s items that have not timed out yet', async () => {
    // Prepare
    clock.setSystemTime(new Date('2023-01-01T00:00:00.000Z'));
    const itemStatus = getItemStatePulsar();
    const item = getMonitoredItemMin({
      metadata: { creationTimestamp: Date.now() },
      status: { state: 'creating' }
    });  

    getK8sStub.resolves([ item ]);
    getExternalItemStateStub.resolves( itemStatus );
    

    const sm = new TestMonitor(
      getK8sStub, getExternalItemStateStub, statusMonitorHandlesItemStub, 2000, {
        statusMonitor: { creationTimeoutInSeconds: 1 }
      }
    );
    sm.statusMonitorHandlesItemStub.returns(true);
    sinon.stub(sm, 'deleteItem').resolves();
    
    // Execute
    await clock.tickAsync(999);


    // Assert
    sinon.assert.notCalled(sm.deleteItem as SinonSpy);
  });

  it('will not call deleteItem on k8s items that would have expired but are not in state creating', async () => {
    // Prepare
    clock.setSystemTime(new Date('2023-01-01T00:00:00.000Z'));
    const itemStatus = getItemStatePulsar();
    const item = getMonitoredItemMin({
      metadata: { creationTimestamp: Date.now() },
      status: { state: 'active' }
    });  

    getK8sStub.resolves([ item ]);
    getExternalItemStateStub.resolves( itemStatus );
    
    const sm = new TestMonitor(
      getK8sStub, getExternalItemStateStub, statusMonitorHandlesItemStub, 2000, {
        statusMonitor: { 
          creationTimeoutInSeconds: 1,
          checkIntervalMs: 1
        }
      }
    );
    sm.statusMonitorHandlesItemStub.returns(true);
    sinon.stub(sm, 'deleteItem').resolves();
    
    // Execute
    await clock.tickAsync(1001);
    await clock.runToLastAsync();


    // Assert
    sinon.assert.notCalled(sm.deleteItem as SinonSpy);
  });

  it('will call deleteItem on k8s items that have expired and are in state creating', async () => {
    // Prepare
    clock.setSystemTime(new Date('2023-01-01T00:00:00.000Z'));
    const itemStatus = getItemStatePulsar();
    const item = getMonitoredItemMin({
      metadata: { creationTimestamp: Date.now() },
      status: { state: 'creating' }
    });  

    getK8sStub.resolves([ item ]);
    getExternalItemStateStub.resolves( itemStatus );
    
    const sm = new TestMonitor(
      getK8sStub, getExternalItemStateStub, statusMonitorHandlesItemStub, 2000, {
        statusMonitor: { creationTimeoutInSeconds: 1 }
      }
    );
    sm.statusMonitorHandlesItemStub.returns(true);
    (sm.configK8s.apiCustom.deleteNamespacedCustomObject as SinonStub).resolves();
    sinon.spy(sm, 'deleteItem');
    
    // Execute
    await clock.tickAsync(1001);
    await clock.nextAsync();


    // Assert
    sinon.assert.calledOnce(sm.deleteItem as SinonSpy);
    sinon.assert.calledWithMatch(
      sm.configK8s.apiCustom.deleteNamespacedCustomObject as SinonStub,
      'main-group',
      'v1alpha1',
      'my-test-namespace',
      'configminkinds',
      'my-test-object'
    );
  });

  it('will log out the error id deleteItem returned an error', async () => {
    // Prepare
    clock.setSystemTime(new Date('2023-01-01T00:00:00.000Z'));
    const itemStatus = getItemStatePulsar();
    const item = getMonitoredItemMin({
      metadata: { creationTimestamp: Date.now() },
      status: { state: 'creating' }
    });  

    getK8sStub.resolves([ item ]);
    getExternalItemStateStub.resolves( itemStatus );
    
    const sm = new TestMonitor(
      getK8sStub, getExternalItemStateStub, statusMonitorHandlesItemStub, 2000, {
        statusMonitor: { creationTimeoutInSeconds: 1 }
      }
    );
    sm.statusMonitorHandlesItemStub.returns(true);
    sinon.stub(sm, 'deleteItem').resolves(new Error('something went wrong'));
    
    // Execute
    await clock.tickAsync(1001);
    await clock.nextAsync();


    // Assert
    sinon.assert.calledOnce(sm.deleteItem as SinonSpy);
    sinon.assert.calledThrice(logger.error as SinonSpy);

    expect((logger.error as SinonStub).firstCall.firstArg)
      .to.deep.equal('[test/v1/TestObject ns=my-test-namespace name=my-test-object]: resource has timed out waiting to be created in pulsar. deleting...',);
    expect((logger.error as SinonStub).secondCall.firstArg)
      .to.deep.equal('[test/v1/TestObject ns=my-test-namespace name=my-test-object]: could not delete timed out k8s resource');
    expect((logger.error as SinonStub).thirdCall.firstArg.message)
      .to.deep.equal('something went wrong');


  });

  it('will not run if interval runner is already running', async () => {
    // Prepare
    clock.setSystemTime(new Date('2023-01-01T00:00:00.000Z'));
    const itemStatus = getItemStatePulsar();
    const item = getMonitoredItemMin();  

    getK8sStub.resolves([ item ]);
    getExternalItemStateStub.resolves( itemStatus );
    
    const sm = new TestMonitor(
      getK8sStub, getExternalItemStateStub, statusMonitorHandlesItemStub, 2000, {
        statusMonitor: { creationTimeoutInSeconds: 1 }
      }
    );
    sm.intervalIsRunning = true;
    sinon.spy(sm, 'setIntervalRunning');
    
    // Execute
    await clock.tickAsync(1001);
    await clock.nextAsync();


    // Assert    
    sinon.assert.calledOnceWithExactly(logger.warn as SinonSpy, 'StatusMonitor: Interval for Test Status Monitor is still running. skipping update cycle. If this warning persists maybe increase the interval time');
  });
});