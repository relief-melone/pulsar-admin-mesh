import K8sAutoImporter, { ConfigMin } from '@/classes/K8sAutoImporter';
import logger from '@/log/logger.default';
import { expect } from 'chai';
import sinon, { SinonFakeTimers, SinonSpy, SinonStatic, SinonStub } from 'sinon';

import getConfigK8sStub from '../helpers/getConfigK8sStub';
import getConfigMin from '../helpers/getConfigMin';
import silenceLogger from '../helpers/silenceLogger';
import configK8s, { ConfigK8s } from '@/configs/config.k8s';

class AutoImporterTester extends K8sAutoImporter<any,any>{

  getItemsPulsarStub =  sinon.stub();
  convertItemPulsarToItemK8sStub = sinon.stub();
  itemsCorrelateStub = sinon.stub();
  k8sItemWillBeHandledStub = sinon.stub();

  constructor( config = getConfigMin(), configK8s = getConfigK8sStub()){
    super('tester', config, configK8s);
  }

  async getItemsPulsar(): Promise<any[]> {
    return this.getItemsPulsarStub();
  }

  convertItemPulsarToItemK8s(itemPulsar: any) {
    return this.convertItemPulsarToItemK8sStub(itemPulsar);
  }

  itemsCorrelate(itemK8s: any, itemPulsar: any): boolean {
    return this.itemsCorrelateStub(itemK8s, itemPulsar);
  }

  k8sItemWillBeHandled(item: any): boolean {
    return this.k8sItemWillBeHandledStub(item);
  }
}

describe('K8sAutoImporter', () => {

  silenceLogger();

  let clock: SinonFakeTimers;
  const sandbox = sinon.createSandbox();
  let config: ConfigK8s;

  beforeEach(() => {
    clock = sinon.useFakeTimers();
    sandbox.spy(logger);
    config = getConfigK8sStub();
  });

  afterEach(() => {
    clock.restore();
    sandbox.restore();
  });

  it('will correctly instantiate', async () => {
    // Prepare/Execute
    const ai = new AutoImporterTester();

    // Assert
    expect(ai.checkIntervalMs).to.equal(getConfigMin().autoSync.checkIntervalMs);
    expect(ai.importIsInProgress).to.be.false;
    expect(ai.interval).to.be.null;
    expect(ai.name).to.equal('tester');
  });

  it('will correctly start up', async () => {
    // Prepare
    const ai = new AutoImporterTester();

    // Execute
    ai.start();
    const runnerFunc = sinon.spy(ai, 'runnerFunc');

    // Assert
    expect(ai.interval).not.to.be.null;
    await clock.nextAsync();
    expect(ai.importIsInProgress).to.be.false;
    sinon.assert.calledOnce(runnerFunc);
  });

  it('will not run the runnerFunc if import is currently in progress', async () => {
    // Prepare
    const ai = new AutoImporterTester();

    // Execute
    const runnerFunc = sinon.spy(ai, 'runnerFunc');
    ai.importIsInProgress = true;
    ai.start();
    

    // Assert
    expect(ai.interval).not.to.be.null;
    await clock.runToLastAsync();
    expect(ai.importIsInProgress).to.be.true;
    sinon.assert.notCalled(runnerFunc);
  });

  it('will have run twice after 101 ms if interval is set to 50ms', async () => {
    // Prepare
    const config = getConfigMin({
      autoSync: { checkIntervalMs: 50 }
    });
    const ai = new AutoImporterTester(config);
    

    // Execute
    const runnerFunc = sinon.spy(ai, 'runnerFunc');
    
    ai.start();
    ai.getItemsK8s = sinon.stub().resolves([]); 
    ai.getItemsPulsarStub.resolves([]);
    

    // Assert
    expect(ai.interval).not.to.be.null;
    await clock.tickAsync(101);
    
    sinon.assert.calledTwice(runnerFunc);
  });

  it('will continue to run if it encounters an error getting the items', async () => {
    // Prepare
    const config = getConfigMin({
      autoSync: { checkIntervalMs: 50 }
    });
    const ai = new AutoImporterTester(config);
    

    // Execute
    const runnerFunc = sinon.spy(ai, 'runnerFunc');
    
    ai.start();
    ai.getItemsK8s = sinon.stub().rejects(); 
    ai.getItemsPulsarStub.resolves([]);
    

    // Assert
    expect(ai.interval).not.to.be.null;
    await clock.tickAsync(101);
    
    sinon.assert.calledTwice(runnerFunc);
  });

  it('will log out the error if there is an error getting items exclusive in pulsar', async () => {
    // Prepare
    const config = getConfigMin({
      autoSync: { checkIntervalMs: 50 }
    });
    const ai = new AutoImporterTester(config);
    
    sinon.stub(ai,'getItemsK8s').resolves([]);
    sinon.stub(ai, 'getItemsPulsar').resolves([]);
    sinon
      .stub(ai, 'getItemsInK8sButNotInPulsar')
      .returns([]);

    sinon
      .stub(ai, 'getItemsInPulsarButNotInK8s')
      .throws('something went wrong');
    

    // Execute
    const runnerFunc = sinon.spy(ai, 'runnerFunc');
    ai.start();
    

    // Assert
    await clock.tickAsync(51);
    sinon.assert.calledOnce(ai.getItemsInK8sButNotInPulsar as SinonStub);
    sinon.assert.calledOnce(logger.error as SinonSpy);
  });

  it('will correctly stop', async () => {
    // Prepare
    const ai = new AutoImporterTester();

    // Execute
    ai.start();
    const runnerFunc = sinon.spy(ai, 'runnerFunc');

    // Assert
    await clock.nextAsync();
    sinon.assert.calledOnce(runnerFunc);
    ai.stop();
    await clock.nextAsync();
    expect(ai.interval).to.be.null;
  });

  describe('deleteOrCreateInK8s', () => {
    it('will return an error when item does not include namespace', async () => {
      // Prepare
      const config = getConfigMin({
        autoSync: { checkIntervalMs: 50 }
      });
      const ai = new AutoImporterTester(config);
    
    

      // Execute
      const result = await ai.deleteOrCreateInK8s('create')({
        metadata: { }
      });    

      // Assert
      expect(result instanceof Error).to.be.true;
      sinon.assert.notCalled(ai.configK8s.apiCustom.createNamespacedCustomObject as SinonStub);
      sinon.assert.notCalled(ai.configK8s.apiCustom.deleteNamespacedCustomObject as SinonStub);
    });

    it('will call createCustomNamespacedObject with the correct parameters for creation operations', async () => {
      // Prepare
      const config = getConfigMin({
        autoSync: { checkIntervalMs: 50 }
      });
      const ai = new AutoImporterTester(config);
      const item = {
        metadata: {
          name: 'my-object',
          namespace: 'default'
        },
        spec: {
          foo: 'bar'
        }
      };
    
    

      // Execute
      const result = await ai.deleteOrCreateInK8s('create')(item);    

      // Assert
      expect(result instanceof Error).to.be.false;
      sinon.assert.notCalled(ai.configK8s.apiCustom.deleteNamespacedCustomObject as SinonStub);
      sinon.assert.calledOnceWithExactly(ai.configK8s.apiCustom.createNamespacedCustomObject as SinonStub,
        'main-group',
        'v1alpha1',
        'default',
        'configminkinds',
        item
      );
    });

    it('log and return an error if something goes wrong during deletion', async () => {
      // Prepare
      const config = getConfigMin({
        autoSync: { checkIntervalMs: 50 }
      });
      const ai = new AutoImporterTester(config);
      const item = {
        metadata: {
          name: 'my-object',
          namespace: 'default'
        },
        spec: {
          foo: 'bar'
        }
      };
    
    

      // Execute      
      (ai.configK8s.apiCustom.deleteNamespacedCustomObject as SinonStub)
        .rejects(new Error('something went wrong'));
      const result = await ai.deleteOrCreateInK8s('delete')(item);

      // Assert      
      sinon.assert.calledOnceWithExactly(ai.configK8s.apiCustom.deleteNamespacedCustomObject as SinonStub,
        'main-group',
        'v1alpha1',
        'default',
        'configminkinds',
        'my-object'
      );
      sinon.assert.notCalled(ai.configK8s.apiCustom.createNamespacedCustomObject as SinonStub);
      expect(result instanceof Error).to.be.true;
      sinon.assert.calledOnce(logger.error as SinonStub);
    });
  });
});