import { DeepPartial } from '@/classes/Helpers';
import { ConfigMin } from '@/classes/K8sAutoImporter';
import deepmerge from 'deepmerge';

const getBaseConfig = ():ConfigMin => ({
  autoSync: {
    checkIntervalMs: 50,
  },
  main: {
    group: 'main-group',
    kind: 'ConfigMinKind',
    plural: 'configminkinds',
    version: 'v1alpha1'
  },
  probing: {
    deletion: {
      intervalIncrementorMs: 20,
      maxAttempts: 10
    },
    readiness: {
      intervalIncrementorMs: 20,
      maxAttempts: 10
    }
  },
  statusMonitor: {
    checkIntervalMs: 20,
    creationTimeoutInSeconds: 130
  }
});

export default (patch:DeepPartial<ConfigMin> = {}):ConfigMin => 
  deepmerge(getBaseConfig(), patch) as ConfigMin;