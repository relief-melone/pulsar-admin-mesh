import { ConfigK8s } from '@/configs/config.k8s';
import sinon, { SinonSpy, SinonStub, SinonStubbedInstance } from 'sinon';

type ReturnValues = {
  apiApps?: Partial<Record<keyof ConfigK8s['apiApps'], any>>
  apiExtensions?: Partial<Record<keyof ConfigK8s['apiExtensions'], any>>,
};

export default ( 
  env = process.env, 
  returnValues:ReturnValues = {
    apiApps: { 
      createNamespacedDeployment: null,
      deleteNamespacedDeployment: null
    },
    apiExtensions: {
      listCustomResourceDefinition: {
        body : {
          apiVersion: 'apiextensions.k8s.io/v1',
          items: [],
          kind: 'CustomResourceDefinitionList',
          metadata: {
            resourceVersion: '12345678',
          }
        },
        response: {
          // Would be http response but not used
        }
      }
    }
  }
) => {
  const config:SinonStubbedInstance<ConfigK8s> = {
    
    apiApps: {
      createNamespacedDeployment: sinon.stub().resolves(returnValues.apiApps?.createNamespacedDeployment),
      deleteNamespacedDeployment: sinon.stub().resolves(returnValues.apiApps?.deleteNamespacedDeployment)
    },

    apiExtensions: {
      listCustomResourceDefinition: sinon.stub().resolves(returnValues.apiExtensions?.listCustomResourceDefinition)
    } as any as ConfigK8s['apiExtensions'],

    apiCustom: {
      createNamespacedCustomObject: sinon.stub<any,ReturnType<ConfigK8s['apiCustom']['createNamespacedCustomObject']>>(),
      deleteNamespacedCustomObject: sinon.stub<any,ReturnType<ConfigK8s['apiCustom']['deleteNamespacedCustomObject']>>(),
      
      listNamespacedCustomObject: sinon.stub<any,ReturnType<ConfigK8s['apiCustom']['listNamespacedCustomObject']>>()
        .resolves({ 
          body: {
            items: []
          },
          response: {} as any
        })
    } as any
  } as any;

  return config;
};