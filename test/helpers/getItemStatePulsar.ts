import { DeepPartial } from '@/classes/Helpers';
import { ItemStatePulsar, MonitoredItemMin } from '@/classes/StatusMonitorNew';
import deepmerge from 'deepmerge';

export const getItemStatePulsar = <T extends ItemStatePulsar = ItemStatePulsar>(patch:DeepPartial<T> = {} as DeepPartial<T>):T => deepmerge<T>({
  
} as T, patch);