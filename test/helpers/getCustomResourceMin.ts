// import { DeepPartial } from '@/classes/Helpers';
// import { CustomResourceMin } from '@/classes/K8sAutoImporter';
// import deepmerge from 'deepmerge';

// const getDefaultResource = ():CustomResourceMin => ({
//   apiVersion: 'test/apps',
//   kind: 'CustomResourceMin',
//   metadata: { 
//     name: 'my-custom-resource-min',
//     namespace: 'default'
//   },
//   spec: {
//     foo: 'bar',
//     bing: 'baz'
//   },
//   status: {
//     state: 'active'
//   }
// });

// export default (patch:DeepPartial<CustomResourceMin> = {}):CustomResourceMin => {
//   return deepmerge(getDefaultResource(), patch) as CustomResourceMin;
// };