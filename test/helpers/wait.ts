export default (waitMs: number):Promise<void> => new Promise((res) => {
  setTimeout(() => res(), waitMs);
});