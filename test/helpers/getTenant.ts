import { DeepPartial } from '@/classes/Helpers';
import { PulsarTenantInK8s, PulsarTenantInPulsar } from '@/classes/PulsarTenant';
import deepmerge from 'deepmerge';

const validTenantK8s = {
  apiVersion: 'pulsar.admin.io/v1alpha1',
  kind: 'Tenant',
  metadata: {
    name: 'my-tenant',
    namespace: 'my-namespace',
    resourceVersion: '123',
    uid: '123'
  },
  spec: {
    config: {
      allowedClusters: [ 'pulsar' ],
      adminRoles: []
    },
  },
  status: {
    state: 'active',
    functions: {
      count: 3
    },
    namespaces: {
      count: 2
    },
    failedRetries: 2,
    lastSpecOnError: '',
    statusMessage: 'test'
  }
} as PulsarTenantInK8s;

const validTenantPulsar = {
  adminRoles: [],
  allowedClusters: ['pulsar']
} as PulsarTenantInPulsar;


Object.freeze(validTenantK8s);
Object.freeze(validTenantPulsar);

export const getTenantInK8s = (
  patch: DeepPartial<PulsarTenantInK8s> = {}
):PulsarTenantInK8s => 
  deepmerge(validTenantK8s, patch, { clone: true }) as PulsarTenantInK8s;

export const getTenantInPulsar = (
  patch: DeepPartial<PulsarTenantInPulsar> = {}
):PulsarTenantInPulsar => 
  deepmerge(validTenantPulsar, patch, { clone: true }) as PulsarTenantInPulsar;