import logger from '@/log/logger.default';

export default () => {
  const log = console.log;

  beforeEach(() => {
    logger.silent = true;
    console.log = () => { };
  });

  afterEach(() => {
    logger.silent = false;
    console.log = log;
  });
};