import { DeepPartial } from '@/classes/Helpers';
import { MonitoredItemMin } from '@/classes/StatusMonitorNew';
import deepmerge from 'deepmerge';

export const getMonitoredItemMin = <T extends MonitoredItemMin = MonitoredItemMin>(patch:DeepPartial<T> = {} as DeepPartial<T>):T => deepmerge<T>({
  apiVersion: 'test/v1',
  kind: 'TestObject',
  metadata: {
    name: 'my-test-object',
    namespace: 'my-test-namespace',
    resourceVersion: '2',
    uid: '1234'
  },
  spec: {

  },
  status: {
    state: 'active',
    failedRetries: 0,
    lastSpecOnError: '{}',
    statusMessage: 'OK'
  }
} as T, patch);