import { ErrorTypeTenant } from '@/services/errors/service.errors.tenant.get_error_patch';
import inErrorBecauseExistsInOtherNamespace from '@/services/tenant/conditions/service.tenant.tenant_in_error_because_other_tenant_exists';
import { ResourceEvent } from '@dot-i/k8s-operator';
import { getTenantInK8s } from '@root/test/helpers/getTenant';
import { expect } from 'chai';

describe('service.tenant.tenant_in_error_because_other_tenant_exists', () => {
  it('should return true if tenant is in error because it existed in another k8s namespace', () => {
    // Prepare
    const tenant = getTenantInK8s({
      status: {
        state: 'error',
        statusMessage: ErrorTypeTenant.TenantAlreadyExistsInK8s
      }
    });

    // Execute
    const result = inErrorBecauseExistsInOtherNamespace({
      object: tenant
    } as any as ResourceEvent);

    // Assert
    expect(result).to.be.true;
  });

  it('should return false for regular tenants', () => {
    // Prepare
    const tenant = getTenantInK8s();

    // Execute
    const result = inErrorBecauseExistsInOtherNamespace({
      object: tenant
    } as any as ResourceEvent);

    // Assert
    expect(result).to.be.false;
  });
});