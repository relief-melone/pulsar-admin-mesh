import { PulsarTenantInK8s } from '@/classes/PulsarTenant';
import getMeta from '@/services/shared/service.shared.get_resource_meta_from_kube_object_meta';
import serviceTenantRestGet_one from '@/services/tenant/misc/service.tenant.rest.get_one';
import defaultService, { getService } from '@/services/tenant/status_monitor/service.tenant.status_monitor.check_if_change_to_active';
import tenantPresentInAnotherNamespace from '@/services/tenant/validations/service.tenant.k8s_tenant_present_in_different_namespace';
import { getTenantInK8s, getTenantInPulsar } from '@root/test/helpers/getTenant';
import silenceLogger from '@test/helpers/silenceLogger';
import { expect } from 'chai';
import sinon from 'sinon';


describe('service.tenant.status_monitor.check_if_change_to_active', () => {
  silenceLogger();

  let tenantPresentInAnotherNamepace;
  let getTenant;
  let checkIfChangeToActive;


  beforeEach(() => {
    tenantPresentInAnotherNamepace = sinon
      .stub<any, ReturnType<typeof tenantPresentInAnotherNamespace>>();
    getTenant = sinon
      .stub<any, ReturnType<typeof serviceTenantRestGet_one>>();

    checkIfChangeToActive = getService(tenantPresentInAnotherNamepace, getTenant);

  });

  it('will correctly break without a status change if status is already active', async () => {
    // Prepare
    const itemInK8s = getTenantInK8s();

    const status:Partial<PulsarTenantInK8s['status']> = {};

    // Execute
    await checkIfChangeToActive({} as any ,itemInK8s);

    // Assert
    expect(status).to.deep.equal({});
    sinon.assert.notCalled(tenantPresentInAnotherNamepace);
  });

  it('will correctly patch to active if prerequisites are met', async () => {
    // Prepare
    const itemInK8s = getTenantInK8s({
      status: {
        state: 'error'
      }
    });
    const itemInPulsar = getTenantInPulsar();    
    getTenant.resolves(itemInPulsar);
    tenantPresentInAnotherNamepace.resolves(false);
    

    // Execute
    const patch = await checkIfChangeToActive({} as any ,itemInK8s);

    // Assert
    expect(patch).to.deep.equal({
      state: 'active'
    });
    sinon.assert.calledWithMatch(tenantPresentInAnotherNamepace, {
      metadata: getMeta(itemInK8s),
      status: itemInK8s.status
    });
  });


});