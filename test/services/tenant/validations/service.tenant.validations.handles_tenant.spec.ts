import { ConfigPulsarTenant } from '@/configs/config.pulsar.tenant';
import silenceLogger from '@root/test/helpers/silenceLogger';
import { getService } from '@/services/tenant/validations/service.tenant.validations.handles_tenant';
import { expect } from 'chai';

describe('service.tenant.validations.handles_tenant', () => {
  silenceLogger();

  it('will correctly handle any tenant if whitelist is set to null in config', () => {
    // Prepare
    const config:ConfigPulsarTenant = new ConfigPulsarTenant({
      'PULSAR__TENANT__DEBUG__WHITELIST': undefined
    });
    const handlesTenant = getService(config);
    
    // Execute
    const result = handlesTenant('any_tenant');

    // Assert
    expect(result).to.be.true;
  });

  it('will correctly return false if the tenant is not in the list', () => {
    // Prepare
    const config:ConfigPulsarTenant = new ConfigPulsarTenant({
      'PULSAR__TENANT__DEBUG__WHITELIST': 'tenant1,another_tenant'
    });
    const handlesTenant = getService(config);
    
    // Execute
    const result = handlesTenant('any_tenant');

    // Assert
    expect(result).to.be.false;
  });
  
  it('will correctly return true if the tenant is in the list', () => {
    // Prepare
    const config:ConfigPulsarTenant = new ConfigPulsarTenant({
      'PULSAR__TENANT__DEBUG__WHITELIST': 'tenant1, tenant2,anoter_tenant'
    });
    const handlesTenant = getService(config);
    
    // Execute
    const result = handlesTenant('tenant2');

    // Assert
    expect(result).to.be.true;
  });

  it('will correctly return true if just 1 item is in the list and that is the tenant specified', () => {
    // Prepare
    const config:ConfigPulsarTenant = new ConfigPulsarTenant({
      'PULSAR__TENANT__DEBUG__WHITELIST': 'my-tenant'
    });
    const handlesTenant = getService(config);
    
    // Execute
    const result = handlesTenant('my-tenant');

    // Assert
    expect(result).to.be.true;
  });
});