import silenceLogger from '@test/helpers/silenceLogger';
import { getService } from '@/services/health/service.quit_if_prerequisites_not_met';
import getConfigK8sStub from '@root/test/helpers/getConfigK8sStub';
import { ConfigK8s } from '@/configs/config.k8s';
import sinon, { SinonSpy, SinonStub, SinonStubbedInstance } from 'sinon';
import { V1CustomResourceDefinitionList } from '@kubernetes/client-node';
import configPulsarTenant from '@/configs/config.pulsar.tenant';
import configPulsarNamespace from '@/configs/config.pulsar.namespace';
import configPulsarTopic from '@/configs/config.pulsar.topic';

describe('service.quit_if_prerequisites_not_met', () => {

  silenceLogger();

  let configK8s: SinonStubbedInstance<ConfigK8s>;
  let quitIfPrerequisitesNotMet: ReturnType<typeof getService>;
  let exit;

  beforeEach(() => {
    exit = sinon.stub();
    configK8s =  getConfigK8sStub();
    quitIfPrerequisitesNotMet = getService(exit, configK8s);
  });


  it('will correctly exit if crd list cannot be retrieved', async () => {
    // Prepare
    (configK8s.apiExtensions.listCustomResourceDefinition as SinonStub).throws();

    // Execute
    await quitIfPrerequisitesNotMet();

    // Assert
    sinon.assert.calledOnceWithExactly(exit, 1);
  });

  it('will correctly exit if crd list does not contain all prerequisites', async () => {
    // Prepare
    const list: V1CustomResourceDefinitionList = {
      items: [{
        spec: {
          group: configPulsarTenant.main.group,
          names: {
            plural: configPulsarTenant.main.plural
          },
          versions: [{ name: configPulsarTenant.main.version }]
        }
      }, {
        spec: {
          group: configPulsarNamespace.main.group,
          names: {
            plural: configPulsarNamespace.main.plural
          },
          versions: [{ name: configPulsarNamespace.main.version }]
        } 
      }]
    } as V1CustomResourceDefinitionList;

    (configK8s.apiExtensions.listCustomResourceDefinition as SinonStub).resolves({ body: list });


    // Execute
    await quitIfPrerequisitesNotMet();

    // Assert
    sinon.assert.calledOnceWithExactly(exit, 1);

  });

  it('will correctly return void if all prerequisites are met', async () => {
    // Prepare
    const list: V1CustomResourceDefinitionList = {
      items: [{
        spec: {
          group: configPulsarTenant.main.group,
          names: {
            plural: configPulsarTenant.main.plural
          },
          versions: [{ name: configPulsarTenant.main.version }]
        }
      }, {
        spec: {
          group: configPulsarNamespace.main.group,
          names: {
            plural: configPulsarNamespace.main.plural
          },
          versions: [{ name: configPulsarNamespace.main.version }]
        } 
      },{
        spec: {
          group: configPulsarTopic.main.group,
          names: {
            plural: configPulsarTopic.main.plural
          },
          versions: [{ name: configPulsarTopic.main.version }]
        } 
      }],
    } as V1CustomResourceDefinitionList;

    (configK8s.apiExtensions.listCustomResourceDefinition as SinonStub).resolves({ body: list });


    // Execute
    await quitIfPrerequisitesNotMet();

    // Assert
    sinon.assert.notCalled(exit);

  });
});