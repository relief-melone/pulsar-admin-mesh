import isClusterDoesNotExist from '@/services/errors/service.errors.tenant.is_cluster_does_not_exist';
import { AxiosError, AxiosResponse } from 'axios';
import { expect } from 'chai';

describe('service.errors.tenant.is_cluster_does_not_exist', () => {
  it('will correcly return true if the error is an axios error', () => {
    // Prepare
    const err = new AxiosError(
      undefined,
      undefined,
      undefined,
      undefined,{
        status: 412,
        statusText: 'Clusters do not exist'
      } as AxiosResponse
    );

    // Execute
    const result = isClusterDoesNotExist(err);

    // Assert
    expect(result).to.be.true;
  });

  it('will correctly return false if status does not match', () => {
    // Prepare
    const err = new AxiosError(
      undefined,
      undefined,
      undefined,
      undefined,{
        status: 500,
        statusText: 'Clusters do not exist'
      } as AxiosResponse
    );

    // Execute
    const result = isClusterDoesNotExist(err);

    // Assert
    expect(result).to.be.false;
  });

  it('will correctly return false if statusText does not match', () => {
    // Prepare
    const err = new AxiosError(
      undefined,
      undefined,
      undefined,
      undefined,{
        status: 412,
        statusText: 'another error'
      } as AxiosResponse
    );

    // Execute
    const result = isClusterDoesNotExist(err);

    // Assert
    expect(result).to.be.false;
  });

  it('will correctly return false if error is no Axios Error', () => {
    // Prepare
    const err = new Error();
    (err as any).response = {
      sstatus: 412,
      statusText: 'Clusters do not exist'
    };

    // Execute
    const result = isClusterDoesNotExist(err as AxiosError);

    // Assert
    expect(result).to.be.false;
  });
});