import { expect } from 'chai';
import getErrorPatch, { ErrorTypeTenant } from '@/services/errors/service.errors.tenant.get_error_patch';

describe('service.errors.tenant.get_error_patch', () => {
  it('will correctly return the state and the status message for a tenant that does not exist', () => {
    // Assert
    expect(getErrorPatch(ErrorTypeTenant.TenantAlreadyExistsInK8s, undefined)).to.deep.equal({
      state: 'error',
      statusMessage: 'tenant already exists in another namespace'
    });
  });

  it('will correcly use messageOverrides', () => {
    // Assert
    expect(getErrorPatch(ErrorTypeTenant.TenantAlreadyExistsInK8s, 'my override')).to.deep.equal({
      state: 'error',
      statusMessage: 'my override'
    });
  });
});