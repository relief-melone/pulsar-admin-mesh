import isTenantNotFound from '@/services/errors/service.errors.is_tenant_not_found';
import { AxiosError, AxiosResponse } from 'axios';
import { expect } from 'chai';

describe('service.errors.is_tenant_not_found', () => {
  it('will respond with false if error is not axios error', () => {
    // Prepare
    const err = new Error('I am not an Axios error');
    
    // Execute/Assert
    expect(isTenantNotFound(err)).to.be.false;
  });

  it('will respond if error is not of status 404', () => {
    // Prepare
    const err = new AxiosError(
      undefined,
      undefined,
      undefined, 
      undefined, {
        status: 403,
        data: { reason: 'tenant not found but...' }
      } as AxiosResponse);

    // Execute/Assert
    expect(isTenantNotFound(err)).to.be.false;
  });

  it('will respond with false if reason does not match',  () => {
    // Prepare
    const err = new AxiosError(
      undefined,
      undefined,
      undefined, 
      undefined, {
        status: 404,
        data: { reason: 'namespace not found' }
      } as AxiosResponse);

    // Execute/Assert
    expect(isTenantNotFound(err)).to.be.false;
  });

  it('Will respond with true if prerequisites are met', () => {
    // Prepare
    const err = new AxiosError(
      undefined,
      undefined,
      undefined, 
      undefined, {
        status: 404,
        data: { reason: 'Tenant not found' }
      } as AxiosResponse);

    // Execute/Assert
    expect(isTenantNotFound(err)).to.be.true;
  });


});