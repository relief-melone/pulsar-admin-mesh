import { expect } from 'chai';
import getErrorPatch, { ErrorTypeNamespace } from '../../../src/services/errors/service.errors.namespace.get_error_patch';

describe('service.errors.namespace.get_error_patch', () => {
  it('will correctly return the state and the status message for a tenant that does not exist', () => {
    // Assert
    expect(getErrorPatch(ErrorTypeNamespace.TenantDoesNotExist, undefined)).to.deep.equal({
      state: 'error',
      statusMessage: 'tenant does not exist'
    });
  });

  it('will correcly use messageOverrides', () => {
    // Assert
    expect(getErrorPatch(ErrorTypeNamespace.TenantDoesNotExist, 'my override')).to.deep.equal({
      state: 'error',
      statusMessage: 'my override'
    });
  });
});