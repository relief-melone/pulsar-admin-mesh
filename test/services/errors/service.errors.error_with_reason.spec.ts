import { errorPresentsReason, getReason } from '@/services/errors/service.errors.error_with_reason';
import { AxiosError } from 'axios';
import { expect } from 'chai';

describe('service.errors.error_with_reason', () => {
  describe('errorPresentsReason', () => {
    it('will return true if error is of type AxisError and has a reason', () => {
      // Prepare
      const err = new AxiosError(undefined, undefined, undefined, undefined, {
        data: {
          reason: 'I am the reason'
        }
      } as AxiosError['response']);
  
      // Execute
      const result = errorPresentsReason(err);
  
      // Assert
      expect(result).to.be.true;
    });
  
    it('will return false for AxiosErrors that dont present a reason', () => {
      // Prepare
      const err = new AxiosError();
  
      // Execute
      const result = errorPresentsReason(err);
  
      // Assert
      expect(result).to.be.false;
    });

    it('will return false for normal errors', () => {
      // Prepare
      const err = new Error();
      (err as any).response = { data: { reason: 'Reason is here' } };
  
      // Execute
      const result = errorPresentsReason(err);
  
      // Assert
      expect(result).to.be.false;
    });
  });

  describe('getReason', () => {
    it('will return the reason for AxiosErrors that have a reason', () => {
      // Prepare
      const err = new AxiosError(undefined, undefined, undefined, undefined, {
        data: {
          reason: 'I am the reason'
        }
      } as AxiosError['response']);
  
      // Execute
      const result = getReason(err);
  
      // Assert
      expect(result).to.equal('I am the reason');
    });

    it('will correctly return an empty string for normal errors', () => {
      // Prepare
      const err = new Error('some message');

      // Execute
      const result = getReason(err);
      
      // Assert
      expect(result).to.equal('');
    });
  });
  
});