import isPartitionsNotYetCreated from '@/services/errors/service.errors.is_partitions_not_yet_created';
import { AxiosError, AxiosResponse } from 'axios';
import { expect } from 'chai';

describe('service.errors.is_tenant_not_found', () => {
  it('will respond with false if error is not of status 404', () => {
    // Prepare
    const err = new AxiosError(
      undefined, 
      undefined,
      undefined, 
      undefined, {
        statusText: 'Topic partitions were not yet created',
        status: 403,
      } as AxiosResponse);

    // Execute/Assert
    expect(isPartitionsNotYetCreated(err)).to.be.false;
  });

  it('will respond with false if statusText does not match',  () => {
    // Prepare
    const err = new AxiosError(
      undefined, 
      undefined,
      undefined, 
      undefined, {
        statusText: 'something else was not found',
        status: 404,
      } as AxiosResponse);

    // Execute/Assert
    expect(isPartitionsNotYetCreated(err)).to.be.false;
  });

  it('Will respond with true if prerequisites are met', () => {
    // Prepare
    const err = new AxiosError(
      undefined,
      undefined,
      undefined, 
      undefined, {
        statusText: 'Topic partitions were not yet created',
        status: 404,
      } as AxiosResponse);

    // Execute/Assert
    expect(isPartitionsNotYetCreated(err)).to.be.true;
  });


});