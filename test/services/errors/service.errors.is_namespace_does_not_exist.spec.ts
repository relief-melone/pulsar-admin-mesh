import isNamespaceDoesNotExist from '@/services/errors/service.errors.is_namespace_does_not_exist';
import { AxiosError, AxiosResponse } from 'axios';
import { expect } from 'chai';

describe('service.errors.is_namespace_does_not_exist', () => {
  it('will correctly return true for error that indicates the namespace does not exist', () => {
    // Preapare
    const err = new AxiosError(undefined, undefined, undefined, undefined, {
      status: 404,
      data: { reason: 'namespace does not exist' }
    } as AxiosResponse);

    // Execute
    const result = isNamespaceDoesNotExist(err);

    // Assert
    expect(result).to.be.true;
  });

  it('will correctly return false for other errors', () => {
    // Preapare
    const err = new AxiosError(undefined, undefined, undefined, undefined, {
      status: 404,
      data: { reason: 'another resource not found' }
    } as AxiosResponse);

    // Execute
    const result = isNamespaceDoesNotExist(err);

    // Assert
    expect(result).to.be.false;
  });
});