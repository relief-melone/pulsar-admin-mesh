import waitOnPatchError from '@/services/errors/service.errors.wait_on_patch_error';
import { expect } from 'chai';

describe('service.errors.wait_on_patch_error', () => {
  it('will return 4 times the base value on the second try', () => {
    // Prepare
    const result = waitOnPatchError(2, 1, 5000);

    // Assert
    expect(result).to.equal(4);
  });

  it('will return max wait time if that is lower', () => {
    // Prepare
    const result = waitOnPatchError(5, 1, 30);

    // Assert
    expect(result).to.equal(30);
  });
});