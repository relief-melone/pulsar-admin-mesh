import isCustomResourceNotFound from '@/services/errors/service.errors.is_resource_not_found';
import { expect } from 'chai';

describe('service.errors.is_resource_not_found', () => {
  it('will correctly return true if conditions match', () => {
    // Prepare
    const err:any = new Error();
    err.name='HttpError';
    err.statusCode = 404;

    // Execute
    const result = isCustomResourceNotFound(err);

    // Assert
    expect(result).to.be.true;
  });

  it('will correctly return false if error is missing statusCode', () => {
    // Prepare
    const err:any = new Error();
    err.name='HttpError';

    // Execute
    const result = isCustomResourceNotFound(err);

    // Assert
    expect(result).to.be.false;
  });
});