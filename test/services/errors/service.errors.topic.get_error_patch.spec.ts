import getErrorPatch, { ErrorTypeTopic } from '@/services/errors/service.errors.topic.get_error_patch';
import { expect } from 'chai';

describe('service.errors.topic.get_error_patch', () => {
  it('will recurn the correct error message without an override', () => {
    // Execute
    const result = getErrorPatch(ErrorTypeTopic.NamespaceDoesNotExist);

    // Assert
    expect(result.state).to.equal('error');
    expect(result.statusMessage).to.equal('namespace does not exist');
  });

  it('will generate status messages for all known error types', () => {
    // Prepare
    const errorTypes = [
      ErrorTypeTopic.InternalError,
      ErrorTypeTopic.InvalidConfiguration,
      ErrorTypeTopic.NamespaceDoesNotExist,
      ErrorTypeTopic.UpdateFailed
    ];

    // Execute
    const results = errorTypes.map(et => getErrorPatch(et));

    // Assert
    results.forEach(r => {
      expect(r.state).not.to.be.undefined;
      expect(r.statusMessage).not.to.be.undefined;
    });
  });

  it('will use message overrides', () => {
    // Execute
    const result = getErrorPatch(ErrorTypeTopic.UpdateFailed, 'bing baz');

    // Assert
    expect(result.state).to.equal('error');
    expect(result.statusMessage).to.equal('bing baz');

  });
});