import { CustomResourceInfo } from '@/classes/Internal';
import getApiVersion from '@/services/shared/misc/service.shared.get_api_version_from_cri';
import silenceLogger from '@root/test/helpers/silenceLogger';
import { expect } from 'chai';

describe('service.shared.get_api_version_from_cri', () => {

  silenceLogger();

  
  it('will return the correct apiVersion', () => {
    // Prepare
    const input:CustomResourceInfo = {
      group: 'some-group',
      kind: 'MyResource',
      plural: 'myresources',
      version: 'v1'
    };

    // Execute
    const result = getApiVersion(input);

    // Assert
    expect(result).to.equal('some-group/v1');
  });
});