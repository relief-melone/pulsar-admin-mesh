import { PulsarNamespaceInK8s } from '@/classes/PulsarNamespace';
import { MonitoredItemMin } from '@/classes/StatusMonitorNew';
import logger from '@/log/logger.default';
import resourceTimedOut, { hasResourceTimedOut, MinConfig } from '@/services/shared/k8s/service.shared.k8s.resource_timed_out_in_creation';
import { KubernetesObject } from '@kubernetes/client-node';
import { getTenantInK8s } from '@root/test/helpers/getTenant';
import silenceLogger from '@root/test/helpers/silenceLogger';
import { expect } from 'chai';
import deepmerge from 'deepmerge';
import sinon, { SinonFakeTimers } from 'sinon';

describe('service.shared.resource_timed_out_in_creation.spec', () => {
  silenceLogger();

  let clock: SinonFakeTimers;
  const sandbox = sinon.createSandbox();

  beforeEach(() => {
    clock = sinon.useFakeTimers();
    sandbox.spy(logger);
  });

  afterEach(() => {
    clock.restore();
    sandbox.restore();
  });

  describe('hasResourceTimedOut', () => {
    
    it('will correctly return false for objects that are not in creating state', () => {
      // Prepare
      const kObj:KubernetesObject & Record<string,any> = getTenantInK8s({
        status: { state: 'active' }
      });

      // Execute
      const result = hasResourceTimedOut(kObj, 20);

      // Assert
      expect(result).to.be.false;
      sinon.assert.notCalled(logger.debug as any);

    });

    it('will correctly return false for objects that are missing a status', () => {
      // Prepare
      const kObj:KubernetesObject & Record<string,any> = getTenantInK8s({
        status: undefined
      });

      // Execute
      const result = hasResourceTimedOut(kObj, 20);

      // Assert
      expect(result).to.be.false;
      sinon.assert.notCalled(logger.debug as any);
      
    });

    it('will correctly return false if no creation timestamp is present', () => {
      // Prepare
      const kObj:KubernetesObject & Record<string,any> = getTenantInK8s({
        status: { state: 'creating' },
        metadata: {
          creationTimestamp: undefined
        }
      });

      // Execute
      const result = hasResourceTimedOut(kObj, 20);

      // Assert
      expect(result).to.be.false;
      sinon.assert.calledOnce(logger.debug as any);
    });

    it('will correctly return false time difference between creation and now has not surpassed timeout', () => {
      // Prepare
      clock.setSystemTime(new Date('2000-01-01T00:00:00.000Z'));
      const kObj:KubernetesObject & Record<string,any> = getTenantInK8s({
        status: { state: 'creating' },
        metadata: {
          creationTimestamp: new Date()
        }
      });
      clock.tick(19000);

      // Execute
      const result = hasResourceTimedOut(kObj, 20);

      // Assert
      expect(result).to.be.false;
      sinon.assert.notCalled(logger.debug as any);
    });

    it('will correctly return true time difference between creation and now has surpassed timeout', () => {
      // Prepare
      clock.setSystemTime(new Date('2000-01-01T00:00:00.000Z'));
      const kObj:KubernetesObject & Record<string,any> = getTenantInK8s({
        status: { state: 'creating' },
        metadata: {
          creationTimestamp: new Date()
        }
      });
      clock.tick(21000);

      // Execute
      const result = hasResourceTimedOut(kObj, 20);

      // Assert
      expect(result).to.be.true;
      sinon.assert.notCalled(logger.debug as any);
    });
  });

  describe('mainService', () => {
    it('will correctly return true if only namespace timed out and item is a namespace', () => {
      // Prepare
      const configNamepaceMin:MinConfig = {
        main: { kind: 'PulsarNamespace' },
        statusMonitor: {
          creationTimeoutInSeconds: 130
        }
      };
      
      clock.setSystemTime(new Date('2000-01-01T00:00:00.000Z'));
      const obj = {
        kind: 'PulsarNamespace',
        metadata: {
          creationTimestamp: new Date()
        },
        status: { state: 'creating' }
      } as MonitoredItemMin;
      clock.tick(131000);

      // Execute
      const result = resourceTimedOut(obj, configNamepaceMin);

      // Assert
      expect(result).to.be.true;
      sinon.assert.notCalled((logger as any).warn);
    });
  });
});