import { getNamespace } from '@/services/shared/service.shared.get_namespace';
import { ResourceMeta } from '@dot-i/k8s-operator';
import { expect } from 'chai';

describe('service.get_namespace', () => {
  it('should throw if no namespace is present in meta', () => {
    // Prepare
    const meta: ResourceMeta = {
      apiVersion: 'my-api.io',
      kind: 'MyRresource',
      id: '1234',
      name: 'my-name',
      resourceVersion: '1234',
      // namespace: not-present
    };
    const func = () => getNamespace(meta);

    // Execute

    // Assert

    expect(func).to.throw();
  });
});