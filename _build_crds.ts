import moduleAlias from 'module-alias';

moduleAlias.addAliases({
  '@': `${__dirname}/src`,
  '@test': `${__dirname}/test`,
  '@root': `${__dirname}`
});


import logger from '@/log/logger.default';
import refParser from '@apidevtools/json-schema-ref-parser';
import { readFileSync, writeFileSync } from 'fs';
import { parse, stringify } from 'yaml';

async function run(){
  await create_pulsar_tenant();
  await create_pulsar_namespace();
  await create_pulsar_topic();
}


type ModBeforeDerefFunc = (inp: Record<string,any>) => Promise<Record<string,any>>; 

async function create_crd(schemaLocation: string, outputLocation: string, modFuncs:Record<string, ModBeforeDerefFunc>){
  logger.info(`Reading schema from ${schemaLocation}`);

  const crd = parse(readFileSync(schemaLocation).toString());
  
  const operations = (crd.spec.versions as Array<Record<string,any>>).map(async (version, index) => {
    let schema = version.schema.openAPIV3Schema;
    logger.info(`Dereferencing Version: ${version.name}`);


    if (modFuncs[version.name])
      schema = await modFuncs[version.name](schema);

    const dereferenced:any = await refParser.dereference(schema);
    delete dereferenced.$defs;

    version.schema.openAPIV3Schema = dereferenced;
  });

  await Promise.all(operations);
  logger.info(`Saving crd to ${outputLocation}`);
  writeFileSync(outputLocation, stringify(crd, {
    aliasDuplicateObjects: false
  }));
  logger.info('Done saving');
}

async function create_pulsar_tenant(){
  await create_crd('schemas/schema.crd.pulsar_tenant.yaml', '.ci/helm/.crds/crd.pulsar_tenant.yaml', {

    'v1alpha1': async (schema) => {
      
      delete schema.additionalProperties;

      delete schema.properties.spec.required;
      delete schema.properties.spec.additionalProperties;
      
      delete schema.properties.spec.properties.config.additionalProperties;
      delete schema.properties.spec.properties.config.required;

      delete schema.properties.status.required;
      delete schema.properties.status.additionalProperties;

      delete schema.required;
      
      return schema;
    }

  });
}

async function create_pulsar_namespace(){
  await create_crd('schemas/schema.crd.pulsar_namespace.yaml', '.ci/helm/.crds/crd.pulsar_namespace.yaml', {
    'v1alpha1': async (schema) => {

      delete schema.required;
      delete schema.additionalProperties;
      
      delete schema.properties.spec.properties.config.additionalProperties;

      delete schema.properties.status.required;
      delete schema.properties.status.additionalProperties;

      // delete schema.properties.spec.properties.config.required;
      
      return schema;
    }
  });
}

async function create_pulsar_topic(){
  await create_crd('schemas/schema.crd.pulsar_topic.yaml', '.ci/helm/.crds/crd.pulsar_topic.yaml',{
    'v1alpha1': async (schema) => {
      delete schema.required;
      delete schema.additionalProperties;

      delete schema.properties.status.required;
      delete schema.properties.status.additionalProperties;

      delete schema.properties.spec.required;
      delete schema.properties.spec.additionalProperties;

      delete schema.properties.status.properties.partitions.properties.items.items.required;
      delete schema.properties.status.properties.partitions.properties.items.items.additionalProperties;

      delete schema.properties.status.properties.partitions.required;
      delete schema.properties.status.properties.partitions.additionalProperties;

      delete schema.properties.status.properties.subscriptions.properties.items.items.required;
      delete schema.properties.status.properties.subscriptions.properties.items.items.additionalProperties;

      delete schema.properties.status.properties.subscriptions.required;
      delete schema.properties.status.properties.subscriptions.additionalProperties;

      delete schema.properties.status.properties.producers.required;
      delete schema.properties.status.properties.producers.additionalProperties;

      return schema;

    }
  });
}


run();
